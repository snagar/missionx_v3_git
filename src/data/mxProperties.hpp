#ifndef BASE_DATA_CLASS_H_
#define BASE_DATA_CLASS_H_
#pragma once

#include <map>
#include "../core/Utils.h"
#include "../core/mxproperty.hpp"

using namespace missionx;
using namespace mxconst;

/*
mxProperties is mainly use with the embedded script class.
It holds properties from different classes, like tasks, triggers, goal etc and inject it into MY-BASIC when needed.
*/
namespace missionx
{


class mxProperties : public missionx::mxProperty
{

public:
  std::map<std::string, missionx::mxProperty> mapProperties;
  mxProperties()
  {
    mapProperties.clear();
  }

  // -----------------------------------

  mxProperties(mxProperties const&) = default;


  // -----------------------------------
  void clone(mxProperties& inProperties)
  {
    // we do not copy the XML Node: node. Need better investigation regarding implications.
    mapProperties.clear();
#ifdef APL
    this->mapProperties.clear();
    for (auto p : inProperties.mapProperties)
      Utils::addElementToMap(this->mapProperties, p.first, p.second);
#else
    this->mapProperties = inProperties.mapProperties;
#endif
  }

  // OPERATOR
  void operator=(mxProperties& inProp) { this->clone(inProp); }

  // -----------------------------------
  // -----------------------------------
  // -----------------------------------
  void clear()
  {
    if (mapProperties.empty())
      return;

    mapProperties.clear();
  }

  // -----------------------------------
  template<typename T>
  mxProperty setProperty(const std::string& inPropertyName, const T inElement)
  {

    auto p = mapProperties.find(inPropertyName);
    if (p != mapProperties.end())
    {
      auto* ptrProperty = &p->second; // mxPropertyWithString
      ptrProperty->setValue(inElement);
      return (*ptrProperty);
    }
    else
    {
      mxProperty new_property;
      new_property.setValue(inElement);
      this->mapProperties.insert(make_pair(inPropertyName, new_property));
      return new_property;
    }

    return mxProperty();
  }


  // -----------------------------------

  /* modify MX_STRING type property */
  mxProperty setStringProperty(std::string inKey, std::string inNewValue)
  {
    if (trim(inKey).empty())
    {
      Log::logMsgErr("[setProperty]Found EMPTY property key!!! Notify Developer !!!");
      assert(!trim(inKey).empty() && "EMPTY string key in mxProperties");
    }
    else
    {
      return setProperty<std::string>(inKey, inNewValue);
    }

    return mxProperty();
  }

  // -----------------------------------

  mxProperty setBoolProperty(std::string inPropertyName, bool inValue)
  {

    auto p = mapProperties.find(inPropertyName);
    if (p != mapProperties.end())
    {
      auto* ptrProperty = &p->second; // mxProperty
      ptrProperty->setValue(inValue);
      return (*ptrProperty);
    }
    else
    {
      mxProperty new_property;
      new_property.setValue(inValue);
      this->mapProperties.insert(make_pair(inPropertyName, new_property));
      return new_property;
    }

    return mxProperty();
  }

  // -----------------------------------

  mxProperty setBoolProperty(const std::string& inPropertyName, std::string inValue)
  {
    bool b = false;

    if (inValue.empty())
    {
      Log::logMsgErr("[add bool property] Value is empty.");
      return mxProperty(); // skip action with no error
    }
    else if (inPropertyName.empty())
    {
      Log::logMsgErr("[Error add bool property] Property name is empty.");
      return mxProperty(); // skip action with no error
    }

    // convert to lower case
    inValue = stringToLower(trim(inValue));

    // try to guess value and convert to int/bool
    // check length = 1
    // check if "true"/"false"/"yes"/"no"
    bool result = isStringBool(inValue, b);

    if (result)
      return setBoolProperty(inPropertyName, b);

    return mxProperty();
  }

  // -------------------------------------------

  mxProperty setIntProperty(const std::string& inPropertyName, int inValue, const bool failToDeductValue = false)
  {
    auto p = mapProperties.find(inPropertyName);
    if (p != mapProperties.end())
    {
      if (!failToDeductValue)
      {
        auto* ptrProperty = &p->second; // mxPropertyWithString
        ptrProperty->setValue(inValue);
        return (*ptrProperty);
      }
      else
        return mxProperty();
    }
    else
    {
      mxProperty new_property;
      new_property.setValue(inValue);
      this->mapProperties.insert(make_pair(inPropertyName, new_property));

      return new_property;
    }
  }
  // -------------------------------------------

  mxProperty setNumberProperty(const std::string& inPropertyName, double inValue, bool failToDeductValue = false)
  {

    auto p = mapProperties.find(inPropertyName);
    if (p != mapProperties.end())
    {
      if (!failToDeductValue)
      {
        auto* ptrProperty = &p->second; // mxPropertyWithString
        ptrProperty->setValue(inValue);

        return (*ptrProperty);
      }
    }
    else
    {
      mxProperty new_property;
      new_property.setValue(inValue);
      this->mapProperties.insert(make_pair(inPropertyName, new_property));

      return new_property;
    }

    return mxProperty();
  }

  // -----------------------------------

  bool hasProperty(const std::string& inPropName) { return isElementExists(this->mapProperties, inPropName); }


  // -----------------------------------
  void removeProperty(const std::string& inPropName)
  {
    if (hasProperty(inPropName))
      this->mapProperties.erase(inPropName);

    std::set<std::string> attributesToDelete = { inPropName }; // v3.0.303.6
    //Utils::xml_delete_attribute(this->node, attributesToDelete); // v3.0.303.6
  }


  // -----------------------------------

  std::string getPropertyValue(const std::string& key, std::string& outError)
  {
    mxProperty p; // property
    outError.clear();

    auto iter = mapProperties.find(key);
    if (iter != mapProperties.end())
    {
      p = iter->second;
      if (p.type_enum == missionx::mx_property_type::MX_STRING)
      {
        return p.getValue(); // string
      }
      outError = "Property: [" + key + "] is not STRING. Fix Name or Notify Developer!!!";
    }

    // return a value that is "arithmetic based" or "string based" and represent EMPTY.
    return "";
  }

  // -----------------------------------
  // -----------------------------------


};


}

#endif
