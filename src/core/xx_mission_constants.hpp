#ifndef XX_MY_CONSTANTS_HPP
#define XX_MY_CONSTANTS_HPP
#pragma once

#include <cfloat>
#include <iostream>
#include <sstream>
#include <string>
#include <deque>
#include <list>
#include <map>
#include <vector>
#include <chrono>

#ifndef IBM
#include <cfloat>
#include <limits>
#endif

//#include "XPLMDisplay.h" // needed for mouse data structs
//#include "XPLMUtilities.h" // needed for mouse data structs
//#include "vr/mxvr.h"



namespace missionx
{

//#define TIMER_FUNC // v3.305.2 add timing for functions to understand were it consumes more time.
//#define USE_TRIGGER_OPTIMIZATION // v3.305.2

#define ENABLE_GATHER_RANDOM_AIRPORTS_FROM_MAIN_THREAD_CALL 0

constexpr static const int MX_FEATURES_VERSION = 20241212; //20230917; // 330491; //302564;  // added SETUP_LOCK_OVERPASS_URL_TO_USER_PICK

#define PLUGIN_VER_MAJOR "25" // year
#define PLUGIN_VER_MINOR "03" // month
#define PLUGIN_REVISION_S "1"
constexpr auto PLUGIN_REVISION = "1";

#ifdef RELEASE
#define PLUGIN_DEV_BUILD ""
#else
#define PLUGIN_DEV_BUILD "D" // The char "D", won't interfere with plugin version number when we write it to the preference file.
#endif

constexpr int XP12_VERSION_NO = 120000;
constexpr int XP11_COMPATIBILITY = 11;
constexpr int XP12_COMPATIBILITY = 12;

constexpr static auto PLUGIN_VERSION_S = PLUGIN_VER_MAJOR PLUGIN_VER_MINOR PLUGIN_REVISION_S;

#ifndef RELEASE
constexpr auto FULL_VERSION = PLUGIN_VER_MAJOR "." PLUGIN_VER_MINOR PLUGIN_DEV_BUILD "." PLUGIN_REVISION_S " " __DATE__ " " __TIME__;
#else
  constexpr auto FULL_VERSION = PLUGIN_VER_MAJOR "." PLUGIN_VER_MINOR "." PLUGIN_REVISION_S " " __DATE__;
#endif


inline const static std::string APP_NAME = "Mission-X Plugin";

inline const static std::string PLUGIN_DIR_NAME     = "missionx";
inline const static std::string PLUGIN_FILE_VER     = "312"; // v24.12.2
inline const static std::string PLUGIN_FILE_VER_XP11= "301";
inline const static std::string RANDOM_TEMPLATE_VER = "301";

const static std::list<int> lsSupportedMissionFileVersions = { 301,312 }; // v24.12.2 used when reading missions files.

inline const static std::string TEXTURE_DIR_NAME            = "bitmap";
inline const static std::string XPLANE_SCENERY_INI_FILENAME = "scenery_packs.ini";
inline const static std::string SCENERY_PACK_               = "SCENERY_PACK ";
inline const static std::string CARGO_DATA_FILE             = "cargo_data.xml";

#define MAX_COORDS (360 * 180)

// GLOBAL CONSTANTS
inline const static double PI  = 3.1415926535897932384626433832795; // from windows calculator
inline const static double PI2 = 6.283185307179586476925286766559;  // FOR DROPPING calculation

// static double Gravity=-9.81;

inline const static float EARTH_RADIUS_M                  = 6378145.0f;
inline const static double LOWEST_GROUND_ELEV_FOR_TRIGGERS = -3000.0; // v3.0.253.5 used in cases we need to configure triggers with "--" or "---" signs


inline const static float RadToDeg             = (float)(180.0f / PI); // 57.295779513082320876798154814105
inline const static float DegToRad             = 1.0f / RadToDeg;      // 0.01745329251994329576923690768489
inline const static float feet2meter           = 0.3048f;              // 1.0f / 3.28083f;
inline const static float meter2feet           = 3.28083f;
inline const static float meter2nm             = 0.000539957f;
inline const static float nm2meter             = 1852.0f;
inline const static float nm2km                = 1.852f;
inline const static float km2nm                = 0.539957f;
inline const static float OneNmh2meterInSecond = 0.5144444f; //

inline const static float seconds2minutes_f = 0.0166f; // v3.305.3

inline const static float fpm2ms  = 0.005f; // fpm * feetPerMin2MeterSec = meters per seconds

inline const static float kmh2fts = 0.911344f; // v3.0.202
inline const static float fts2kmh = 1.09728f;  // v3.0.202



inline const static int OUT_OF_BOUNDING_ALERT_TIMER_SEC = 30; // 30 sec. alert will broadcast every 30 seconds
inline const static float MISSIONX_DOUBLE_CLICK           = 0.9f;

inline const static std::string EMPTY_STRING = "";
//const static std::string condStr[6]     = { "lt;", "lte;", "=", "!=", "gt;", "gte;" };
//const static std::string MSG_SEP        = ";----------------;;";
inline const static std::string PREF_FILE_NAME = "missionx_pref.xml";

inline const static int BRIEFER_DESC_CHAR_WIDTH = 70;

// Global Char Buffer for Login
inline const static int LOG_BUFF_SIZE = 2048;
[[maybe_unused]]
inline static char      LOG_BUFF[LOG_BUFF_SIZE];


// 21638.7 Nautical Miles; 40075.0 km  24901.5 miles
inline const static float EQUATER_LEN_NM      = 21638.7f;
inline const static float EARTH_AVG_RADIUS_NM = 3440.07019148103f;


inline const static float    DEGREESE_IN_CIRCLE    = 360.0f;
inline const static intptr_t DAYS_IN_YEAR_365      = 365;
inline const static intptr_t SECONDS_IN_1HOUR_3600 = 3600;
inline const static intptr_t SECONDS_IN_1MINUTE    = 60;
inline const static float    SECONDS_IN_1MINUTE_F  = 60.0f;
inline const static intptr_t SECONDS_IN_1DAY       = 86400;
inline const static intptr_t HOURS_IN_A_DAY_24     = 24;

inline const static double NEARLY_ZERO = 0.0000000001; // dataref_manager

#ifdef DEBUG_WRONG_TERRAIN_ELEV
inline const static unsigned int NUM_CIRCLE_POINTS = 25; // v3.0.253.1
#else
inline const static unsigned int NUM_CIRCLE_POINTS        = 25; // v3.0.253.7 decreased from 359; // v3.0.202a
inline const static unsigned int NUM_CIRCLE_POINTS_3D_OBJ = 8;  // v3.0.253.7 decreased from 359; // v3.0.202a

inline static const size_t MAX_MX_PAD_MESSAGES = 20; // v3.0.110

#endif
////// ENUMS & STRUCTS //////
namespace enums
{
// // v24.12.2
// typedef enum class _from_where_mission_was_loaded
//   : uint8_t
// {
//   loaded_from_mission_file = 0,
//   loaded_from_savepoint = 1,
//   loaded_from_random = 2
// } mx_from_where_mission_was_loaded;


typedef enum class _mx_timer_type
  : uint8_t
{
  xp = 0,
  os = 1
} mx_timer_type;

// v24.03.1
typedef enum class _mx_treeNodeState
  : uint8_t
{
  closed = 0,
  opened = 1,
  was_opened = 2 // distinguish when we at least dealt with the "open" state. Used for one time scroll logic.
} mx_treeNodeState;

// v24.03.1
typedef enum class _mxNoteField
  : uint8_t
{
  fromICAO = 0,
  fromParkLoc,
  fromRunway,
  fromSID,
  fromTrans,
  toTrans,
  toSTAR,
  toRunway,
  toParkLoc,
  toICAO,
  _COUNT, // ILLIGAL 
  begin = 0,
  end = _COUNT
} mx_note_shortField_enum;

typedef enum class _mxNoteLargeFields
  : uint8_t
{
  waypoints = 0,
  taxi,
  takeoff_notes,
  cruise_notes,
  descent_notes,
  _COUNT, // ILLIGAL
  begin  = 0,
  end    = _COUNT
} mx_note_longField_enum;

typedef enum class _mx_action_from_trigger_enum
  : uint8_t
{
  set_success = 0,
  reset = 1
} mx_action_from_trigger_enum;



} // namespace enums

namespace enums_translation
{
static const std::map <missionx::enums::mx_note_shortField_enum, const char*> trnsEnumNoteShort =
  {
    {missionx::enums::mx_note_shortField_enum::fromICAO, "fromICAO"},
    {missionx::enums::mx_note_shortField_enum::fromParkLoc, "fromParkLoc"},
    {missionx::enums::mx_note_shortField_enum::fromRunway, "fromRunway"},
    {missionx::enums::mx_note_shortField_enum::fromSID, "fromSID"},
    {missionx::enums::mx_note_shortField_enum::fromTrans, "fromTrans"},
    {missionx::enums::mx_note_shortField_enum::toTrans, "toTrans"},
    {missionx::enums::mx_note_shortField_enum::toSTAR, "toSTAR"},
    {missionx::enums::mx_note_shortField_enum::toRunway, "toRunway"},
    {missionx::enums::mx_note_shortField_enum::toParkLoc, "toParkLoc"},
    {missionx::enums::mx_note_shortField_enum::toICAO, "toICAO"}
  };
static const std::map <missionx::enums::mx_note_longField_enum, const char*> trnsEnumNoteLong =
  {
    {missionx::enums::mx_note_longField_enum::waypoints, "waypoints"},
    {missionx::enums::mx_note_longField_enum::taxi, "taxi"},
    {missionx::enums::mx_note_longField_enum::takeoff_notes, "takeoff_notes"},
    {missionx::enums::mx_note_longField_enum::cruise_notes, "cruise_notes"},
    {missionx::enums::mx_note_longField_enum::descent_notes, "descent_notes"}
  };
}

typedef struct _mxVec3
{
  float x, y, z;
  _mxVec3() { x = y = z = 0.0f; };
  _mxVec3(float _x, float _y, float _z)
  {
    x = _x;
    y = _y;
    z = _z;
  }
} mxVec3;


typedef struct _mxVec4
{
  float x, y, z, w;
  _mxVec4() { x = y = z = w = 0.0f; };
  _mxVec4(float _x, float _y, float _z, float _w)
  {
    x = _x;
    y = _y;
    z = _z;
    w = _w;
  }
} mxVec4;


typedef struct _mxRGB // v3.305.1
{
  float r {255.0f};
  float g {255.0f};
  float b {255.0f};

  _mxRGB()
  {
    r = g = b =255.0f;
  }

  void setRGB(float inR, float inG, float inB)
  {
    r = inR;
    g = inG;
    b = inB;
  }

} mxRGB;


typedef struct _wp_guess_result
{
  int nav_ref{ -1 };
  int nav_type{ 0 };
#ifdef IBM
  double distance_d{ DBL_MAX };
#else
  double distance_d{ std::numeric_limits<double>::max() };
#endif
  std::string name{ "" };
} mx_wp_guess_result;

#ifdef LIN
	typedef enum class _mx_linux_distro
	  : uint8_t
	{
	  debian_ubuntu_distro          = 0,
	  arch_manjaro_garudo_endeavour = 1,
	  others_unknown                = 2
	} mx_linux_distro;
#endif

typedef struct _mx_score_strct
{
  float min{ 0.0f }, max{ 0.0f };
  float score{ 0.5f };  
} mx_score_strct;


typedef enum class _mx_btn_colors
  : uint8_t
{
  white,
  yellow,
  red,
  green,
  blue,
  purple,
  orange,
  black
} mx_btn_colors;

typedef enum class _random_thread_wait_state
  : uint8_t
{
  not_waiting = 0, // we need this if designer used wrong string for channel
  waiting_for_plugin_callback_job,
  finished_plugin_callback_job
} mx_random_thread_wait_state_enum;


typedef enum class _cue_actions
  : uint8_t
{
  cue_action_none,
  cue_action_first_time,
  cue_action_refresh
} cue_actions_enum;

typedef enum class _mx_msg_mode : int8_t
{
  mode_default = 0,
  mode_story   = 1
} mx_msg_mode;

typedef enum class _mx_msg_line_type : int8_t
{
  line_type_text = 0,
  line_type_action = 1
} mx_msg_line_type;

typedef struct _mx_fetch_info
{
  size_t      lastPos;
  std::string token;

  _mx_fetch_info()
  {
    lastPos = 0;
    token.clear();
  }
} mx_fetch_info;


typedef enum class _message_channel_type
  : uint8_t
{
  comm       = 0, // comm is also mxpad channel
  background = 1,
  no_type    = 3 // we need this if designer used wrong string for channel
  // pad = 3
} mx_message_channel_type_enum;


// describe message state and progress.
// Some of the stages are internal and very short.
typedef enum class _missionx_message_state
  : uint8_t
{
  msg_undefined,
  msg_abort,           // v3.0.223.7 allow the QMM to skip post message actions
  msg_not_broadcasted, // loaded but not dispatched into message pool yet
  msg_in_pool,
  msg_text_is_ready,
  msg_prepare_channels,
  msg_channels_need_to_finish_loading,
  msg_is_ready_to_be_played,
  msg_broadcast_once,
  msg_broadcast_few_times,
  msg_is_ready_for_post_message_actions, // v3.305.1
} mx_message_state_enum;


typedef enum class _mxWindowActions
  : uint8_t
{
  ACTION_NONE,                                                // initialize action
  ACTION_TOGGLE_WINDOW,                                       // only change window state (show / hide ) should not modify layer.
  ACTION_TOGGLE_BRIEFER,                                      // v3.0.201 when a command is called then it means we want to see LEG info and such. Layer should be flight_leg_info for running. We also ad some logic to this action
  ACTION_HIDE_WINDOW,                                         // v3.0.160
  ACTION_SHOW_WINDOW,                                         // v3.0.201
  ACTION_CANCEL,                                              // v3.0.141
  ACTION_CLOSE_WINDOW,                                        // v3.0.145
  ACTION_OPEN_NAV_LAYER,                                      // v24025
  ACTION_QUIT_MISSION,                                        // v3.0.145
  ACTION_QUIT_MISSION_AND_SAVE,                               // v3.0.251.1 b2
  ACTION_LOAD_MISSION,                                        // v3.0.141
  ACTION_CREATE_SAVEPOINT,                                    // v3.0.151
  ACTION_LOAD_SAVEPOINT,                                      // v3.0.151
  ACTION_START_MISSION,                                       // v3.0.141
  ACTION_START_RANDOM_MISSION,                                // v3.0.219.1
  ACTION_TOGGLE_MAP,                                          // v3.0.201
  ACTION_TOGGLE_INVENTORY,                                    // v3.0.213.2
  ACTION_COMMIT_TRANSFER,                                     // v3.0.213.2
  ACTION_TOGGLE_MISSION_GENERATOR,                            // v3.0.217.1
  ACTION_TOGGLE_CHOICE_WINDOW,                                // v3.0.231.1
  ACTION_GUESS_WAYPOINTS,                                     // v3.0.355.2
  ACTION_GENERATE_RANDOM_MISSION,                             // v3.0.217.2
  ACTION_SET_LEG_INFO,                                        // v3.0.221.15rc5 support leg // v3.0.221.6 in VR mode we won't close Mission-X when closing inventory or map, instead we will set goal info layer
  ACTION_SET_MISSION_LIST,                                    // v3.0.221.6 in VR mode we won't close Mission-X when closing Mission Generator.
  ACTION_TOGGLE_BRIEFER_MXPAD,                                // v3.0.221.7 in VR mode
  ACTION_TOGGLE_PLUGIN_SETUP,                                 // v3.0.241.7 plugin setup screen
  ACTION_TOGGLE_UI_USER_MISSION_GENERATOR,                    // v3.0.241.9 display user mission creation from UI and not template
  POST_TEMPLATE_LOAD_DISPLAY_IMGUI_GENERATE_TEMPLATES_IMAGES, // v3.0.251.1 display the list of custom template images
  ACTION_SHOW_END_SUMMARY_LAYER,                              // v3.0.251.1 display the IMGUI summary end window
  ACTION_FETCH_FPLN_FROM_EXTERNAL_SITE,                       // v3.0.253.1 start async fetch process
  ACTION_FETCH_ILS_AIRPORTS,                                  // v3.0.253.6
  ACTION_FETCH_NAV_INFORMATION,                               // v24025
  ACTION_FETCH_MISSION_STATS,                                 // v3.0.255.1
  ACTION_ABORT_RANDOM_ENGINE_RUN,                             // v3.0.253.6
  ACTION_SAVE_USER_SETUP_OPTIONS,                             // v3.0.255.4.2
  ACTION_GENERATE_MISSION_FROM_LNM_FPLN,                      // v3.0.301
  ACTION_GENERATE_RANDOM_DATE_TIME,                           // v3.303.10
  ACTION_OPEN_STORY_LAYOUT,                                   // v3.305.1 open story layout
  ACTION_RESET_BRIEFER_POSITION                               // v3.305.1 reset Briefer window position to center of window
} mx_window_actions;                                          // for all windows. some actions are specific and some shared

// types of cue triggers, this will help with GL color selecting. I hope to also have combinations of colors
typedef enum class _cue_types
  : uint8_t
{
  cue_none           = 0,
  cue_trigger        = 1,  // yellow
  cue_task           = 5,  // green
  cue_mandatory_task = 7,  // magenta
  cue_sling_task     = 8,  // 
  cue_message        = 10, // black
  cue_inventory      = 12, // purple
  cue_script         = 15, // orange
  cue_obj            = 20  // orange ? v3.0.303.6
} mx_cue_types;

typedef enum class _mx_property_type
  : uint8_t
{
  MX_UNKNOWN = 0,
  MX_BOOL    = 1,
  MX_CHAR    = 2,
  MX_INT     = 3,
  MX_FLOAT   = 4,
  MX_DOUBLE  = 5,
  MX_STRING  = 6
} mx_property_type; // v3.0.217.2 moved from mxProperty

// v3.305.1 TODO: should we deprecate this type ?
typedef struct _mx_property_type_as_string_code
{
  const std::string BOOL{ "1" };
  const std::string CHAR{ "2" };
  const std::string INT{ "3" };
  const std::string FLOAT{ "4" };
  const std::string DOUBLE{ "5" };
  const std::string STRING{ "6" };
} mx_property_type_as_string_code;


typedef enum class _enum_layer_state
  : uint8_t // v3.0.253.6
{
  not_initialized                              = 0,
  validating_data                              = 5,
  failed_data_is_not_present                   = 10,
  fatal_database_is_not_initializing_correctly = 15,
  success_can_draw                             = 20
} mx_layer_state_enum;

typedef enum class _enum_ils_types
  : uint8_t // v3.0.253.6
{
  LOC = 0,
  ILS_cat_I,
  ILS_cat_II,
  ILS_cat_III,
  IGS,
  LDA,
  SDF,
  GLS,
  LP,
  LPV
} mx_ils_type_enum;

// The order of _enum_trig_type is crucial since we use it in the conversion UI screen, so camera must be 3 and rad 0
typedef enum class _enum_trig_type
  : uint8_t // v3.0.301
{
  rad    = 0,
  script = 1,
  poly   = 2,
  camera = 3, // v3.0.303.7
  slope = 4
} mx_trig_type_enum;




// v3.0.253.6
static std::map<mx_ils_type_enum, std::string> mapILS_types = { { mx_ils_type_enum::LOC, "LOC" },
                                                                { mx_ils_type_enum::ILS_cat_I, "ILS-cat-I" },
                                                                { mx_ils_type_enum::ILS_cat_II, "ILS-cat-II" },
                                                                { mx_ils_type_enum::ILS_cat_III, "ILS-cat-III" },
                                                                { mx_ils_type_enum::IGS, "IGS" },
                                                                { mx_ils_type_enum::LDA, "LDA" },
                                                                { mx_ils_type_enum::SDF, "SDF" },
                                                                { mx_ils_type_enum::GLS, "GLS" }, // v3.305.4
                                                                { mx_ils_type_enum::LP, "LP" }, // v3.305.4
                                                                { mx_ils_type_enum::LPV, "LPV" } // v3.305.4
                                                              };


typedef struct _messageLine_struct
{
  missionx::mx_msg_line_type type; // if mode is story_mode then "text_line" could be an action and not just a text message
  std::string label;
  std::string label_position;
  std::string label_color;

  std::string message_text; // original message without splitting
                            // std::vector<std::string> textLines; // split message
  std::deque<std::string> textLines; // if line type is "action", then only the first value in the "deque" is relevant.

  missionx::mxVec4 imvec4Color;
  //ImVec4           imvec4Color;

  _messageLine_struct() { init(); }

  _messageLine_struct(const std::string& inLabel, const std::string& inMessageText, const mxVec4& inColor)
    : _messageLine_struct() // v3.305.3
  { 
    this->label        = inLabel;
    this->imvec4Color  = inColor;
    this->message_text = inMessageText;
  }

  void init()
  {
    type = missionx::mx_msg_line_type::line_type_text;
    label.clear();
    label_position.clear();
    label_color.clear();
    message_text.clear();
    textLines.clear();
    imvec4Color.x = imvec4Color.y = imvec4Color.z = imvec4Color.w = 1.0f; // white
  }

} messageLine_strct;
// end struct


typedef struct _mx_location_3d_objects // v3.0.213.7 moved from read_mission_file class, to use in Point class too.
{
  std::string distance_to_display_nm, keep_until_leg, cond_script;
  std::string lat, lon, elev, elev_above_ground;
  std::string heading, pitch, roll, speed; // v3.0.202
  std::string adjust_heading;              // v3.0.207.5

  _mx_location_3d_objects()
  {
    lat = lon = elev = elev_above_ground = "";
    distance_to_display_nm = keep_until_leg = cond_script = "";
    heading = pitch = roll = speed = "";
    adjust_heading.clear(); // v3.0.207.5
  }
} mx_location_3d_objects;


// v24.12.2 - requested by @RandomUser in x-plane.org
typedef struct _mx_option_info
{
  int         longest_text_length_i{ 0 };
  int         user_pick_from_replaceOptions_combo_i{ 0 }; // ui
  float       combo_width_f; // ui
  std::string combo_label_s; // ui

  std::string              longestTextInVector_s;  // Longest text from the option names
  std::string              name;                   // holds the <option name="" > attribute value. Will be displayed above the option list.
  std::vector<std::string> vecReplaceOptions_s;    // holds all the <opt> name attribute values.
  std::vector<const char*> vecReplaceOptions_char; // ui, holds all the <opt> name attribute values from the string vector.

  
  std::vector<const char*> refresh_vecReplaceOptions_char()
  {
    vecReplaceOptions_char.clear();
    for (const auto& opt_name_ptr : vecReplaceOptions_s)
      vecReplaceOptions_char.push_back(opt_name_ptr.c_str());

    return vecReplaceOptions_char;
  }
} mx_option_info;


// v25.02.1 - success task timer information
typedef struct _mx_success_timer_info
{

  float       prevRemainingTime_f{ 9999999.0f };
  std::string triggerNameWithShortestSuccessTimer;
  std::vector<std::string> vecTriggersWithActiveTimers;

  void reset()
  {
    prevRemainingTime_f = 9999999.0f;
    triggerNameWithShortestSuccessTimer.clear();
    vecTriggersWithActiveTimers.clear();
  }

} mx_success_timer_info;



//// a struct to hold UI <option> information
//typedef struct _mx_xy_f
//{
//  float x{ 0.0f };
//  float y{ 0.0f };
//
//  _mx_xy_f() = default;
//  _mx_xy_f(const float& inX, const float& inY)
//  { 
//    x = inX;
//    y = inY;
//  }
//
//} mx_xy_f;

// v24.12.2



// v3.0.213.5
const int BRIEFER_WINDOWS_WIDTH  = 1200;
const int BRIEFER_WINDOWS_HEIGHT = 600;

// v3.0.223.1 VR related constants
const int MAX_CHARS_IN_RANDOM_LINE_2D = 95;
const int MAX_CHARS_IN_RANDOM_LINE_3D = 80;
const int MAX_LINES_IN_RANDOM_DESC_2D = 24;
const int MAX_LINES_IN_RANDOM_DESC_3D = (int)(MAX_LINES_IN_RANDOM_DESC_2D ); //* missionx::mxvr::VR_RATIO);

const int MAX_CHARS_IN_BRIEFER_LINE_2D = 160;
const int MAX_CHARS_IN_BRIEFER_LINE_3D = 145;
const int MAX_LINES_IN_BRIEFER_DESC_2D = 15;
const int MAX_LINES_IN_BRIEFER_DESC_3D = (int)(MAX_LINES_IN_BRIEFER_DESC_2D); // * missionx::mxvr::VR_RATIO);



class MxKeyValuePropertiesTypes // Used in ext_script.cpp to hold updatable element properties to modify or to fetch.
{
public:
  MxKeyValuePropertiesTypes()
  {
    key_s.clear();
    key_type = missionx::mx_property_type::MX_UNKNOWN;
  }

  MxKeyValuePropertiesTypes(std::string inKey, missionx::mx_property_type inValType)
  {
    key_s    = inKey;
    key_type = inValType;
  }

  ~MxKeyValuePropertiesTypes() {}

  std::string                key_s;
  missionx::mx_property_type key_type; // represent property type: MX_STRING, MX_DOUBLE, MX_FLOAT, MX_INT, MX_BOOL
};


typedef enum class _mx_plane_type
  : uint8_t
{
  plane_type_any = 0,
  plane_type_helos,
  plane_type_props,
  plane_type_prop_floats, // custom
  plane_type_ga_floats,   // custom
  plane_type_ga,          // custom
  plane_type_turboprops,
  plane_type_jets,
  plane_type_heavy,
  plane_type_fighter
} mx_plane_types;


// v3.0.241.10 sqlite data types
typedef enum class _mx_datatypes
  : int8_t
{
  skip_field = 0,
  int_typ    = 1,
  real_typ   = 2, // real stands for float + double
  text_typ   = 3,
  date_typ   = 4,
  null_typ   = 5,
  float_typ  = 6, // just in case
  double_typ = 7,  // just in case
  zero_type  =10
} db_types;


typedef enum class _uiLayer
  : uint8_t
{
  // welcome_layer = 0, //v3.0.225.2
  // briefer_detail,
  // mxpad_layer, // v3.0.221.7
  // user_driven_mission_layer, // v3.0.241.9
  // v3.0.251.1 IMGUI layers
  imgui_home_layer = 0, // contains all options layers
  option_user_generates_a_mission_layer,
  option_setup_layer, // v3.0.241.7
  option_mission_list,
  option_generate_mission_from_a_template_layer, // v3.0.217.1
  option_external_fpln_layer,                    // v3.0.253.1
  option_ils_layer,                              // v3.0.253.6 ILS search layer
  option_conv_fpln_to_mission,                   // v3.0.301 convert FPLN to mission
  flight_leg_info,
  flight_leg_info_map2d, // v3.0.200a2
  flight_leg_info_end_summary,
  flight_leg_info_inventory, // v3.0.213.1
  flight_leg_info_story_mode, // v3.305.1
  about_layer,               // v3.0.253.2
  //conv_pick_fpln, // v3.0.301 not sure we need to use this
  //conv_design_fpln, // v3.0.301 not sure we need to use this
  uiLayerUnset,              // v3.0.160
} uiLayer_enum;


typedef enum class _unitsOfMeasure
  : uint8_t
{
  unit_unsupports,
  ft, // feet/foot
  meter,
  km, // kilometers
  nm  // nautical miles
} mx_units_of_measure;

typedef enum class _mission_type
  : int8_t
{
  not_defined = -1,
  medevac = 0,
  cargo   = 1,
  oil_rig = 2
} mx_mission_type;

// v3.303.14
typedef enum class _mission_subcategory_type
  : int8_t
{
  not_defined      = -1,
  med_any_location = 0,
  med_accident     = 1,
  med_outdoors     = 2,
  oilrig_cargo     = 10,
  oilrig_med       = 11,
  oilrig_personnel = 12,
  cargo_ga         = 20,
  cargo_farming    = 21,
  cargo_isolated   = 22,
  cargo_heavy
} mx_mission_subcategory_type;

typedef enum class _ui_random_date_time_type // v3.303.10
  : uint8_t
{
  current_day_and_time = 0, // use current time
  os_day_and_time = 1, // use OS day of year and time (hour)
  any_day_time   = 2, // any day and time, fully random
  exact_day_and_time = 3,
  pick_months_and_part_of_preferred_day = 4
} mx_ui_random_date_time_type;

typedef enum class _ui_random_weather_options // v3.303.12
  : uint8_t
{
  use_xplane_weather = 0, // use x-plane settings = do nothing from plugin side
  pick_pre_defined   = 1,  // Pick one of the 9 pre-defined sets
  use_xplane_weather_and_store = 2 // v3.303.14 deprecated, convert to checkbox   // Let the user define bottom/top layers, wind speed, persipitation and more
} mx_ui_random_weather_options;

typedef enum class _mxFetchState
  : uint8_t
{
  fetch_not_started    = 0,
  fetch_in_process     = 5,
  fetch_ended          = 10,
  fetch_guess_wp       = 15, // guess the waypoint in main plugin call back
  fetch_guess_wp_ended = 16, // guess the waypoint in main plugin call back
  fetch_error          = 20
} mxFetchState_enum;

typedef enum class _mx_filter_ramp_type
  : uint8_t
{
  any_ramp_location     = 0,
  start_ramp            = 1, // example: briefer starting location
  exact_plane_ramp_type = 2,
  airport_ramp          = 5,
  end_ramp              = 10
} mxFilterRampType;

typedef struct _sound_directive_struct
{
  bool bCommandIsActive{ false };         // will be true on first time when it is being handled
  bool bCommandWasEnded{ false };         // will be true when SoundFragment::flc() finish the channel volume change
  bool bCalledRepeatAtLeastOnce{ false }; // store progress of repeat calls. If it is true than we called the repeat code at least once so we should not call it again

  char command{ '\0' };

  int   new_volume{ -1 };
  float startingVolume_f{ 0.0f }; // holds the volume at first time

  float seconds_to_start_f{ -1.0f };
  float transition_time_f{ 0.0f }; // >=0 => immediate
  int   step_i         = { 0 };    // if change volume through time then which step are we from the whole ?
  float increment_by_f = { 0.0f };

  int loopFor_i = { 0 }; // How many times to loop
  int currentLoop_i = { 0 }; // How many loops we had

} mx_track_instructions_strct;

typedef struct _latLon
{
  float lat{ 0.0f };
  float lon{ 0.0f };

  _latLon() { 
    lat = 0.0f;
    lon = 0.0f;
  }
  _latLon(float inLat, float inLon)
  {
    lat = inLat;
    lon = inLon;
  }
} mxVec2f;

typedef struct _latLonDouble
{
  _latLonDouble(){};
  _latLonDouble(const double inLat, const double inLon)
  {
    lat = inLat;
    lon = inLon;
  }
  double lat{ 0.0f };
  double lon{ 0.0f };
} mxVec2d;



  typedef struct _stats_tmp_data
{
  bool        bInitialized{ false };
  int         line_id{ -1 };
  int         icao_id{ -1 };
  double      time_passed{ 0.0 };
  double      distance_from_center_of_rw_d{ 0.0 };
  double      score_centerLine{ 0.0 }; // how good between 0.0 and 1.0

  mxVec2d     plane;               // position of plane

  std::string activity_s{ "" }; // takeoff landing
  std::string icao{ "N/A" };
  std::string runway_s{ "N/A" };

} mx_stats_data;


typedef struct _ext_internet_fpln_strct
{
  int         internal_id{ -1 };
  int         fpln_unique_id{ -1 }; // holds plan unique id from site
  std::string fromICAO_s{ "" };
  std::string toICAO_s{ "" };
  std::string fromName_s{ "" };
  std::string toName_s{ "" };
  std::string flightNumber_s{ "" };
  double      distnace_d{ 0.0 };

  int         maxAltitude_i{ 0 };
  std::string notes_s{ "" };

  std::string popularity_s{ "" }; // v3.0.253.3
  int         popularity_i{ 0 };  // v3.0.253.3

  int                                     waypoints_i{ 0 };
  std::string                             encode_polyline_s;
  std::list<missionx::mxVec2f>            listNavPoints;
  std::list<missionx::mx_wp_guess_result> listNavPointsGuessedName; // v3.0.255.2

  std::string formated_nav_points_s{ "" };                    // holds the list of nav points but in formated string
  std::string formated_nav_points_with_guessed_names_s{ "" }; // v3.0.255.2 holds the list of nav points but in formated string and guessed wp names, not just coordinates


} mx_ext_internet_fpln_strct;



const static struct _fplndb_json_keys_struct
{
  const std::string Z_KEY_id{ "id" };
  const std::string Z_KEY_popularity{ "popularity" }; // v3.0.253.3
  const std::string Z_KEY_distance{ "distance" };
  const std::string Z_KEY_encodedPolyline{ "encodedPolyline" };
  const std::string Z_KEY_flightNumber{ "flightNumber" };
  const std::string Z_KEY_fromICAO{ "fromICAO" };
  const std::string Z_KEY_fromName{ "fromName" };
  const std::string Z_KEY_maxAltitude{ "maxAltitude" };
  const std::string Z_KEY_notes{ "notes" };
  const std::string Z_KEY_toICAO{ "toICAO" };
  const std::string Z_KEY_toName{ "toName" };
  const std::string Z_KEY_waypoints{ "waypoints" };
} mx_fplndb_json_keys;

// v3.0.253.6 holds the query result from ILS query
typedef struct _ils_airport_row_strct
{
  int    seq{ -1 }; // used by the plugin to distinguish between the rows
  int    ap_elev_ft_i{ 0 };
  int    loc_bearing_i{ 0 };
  int    rw_length_mt_i{ 0 };
  int    loc_frq_mhz{ 0 };
  double distnace_d{ 0.0 };
  double rw_width_d{ 0.0 };
  double bearing_from_to_icao_d{ 0.0 }; // bearing relative to start icao v3.0.253.13


  std::string toICAO_s{ "" };   // target airport name
  std::string toName_s{ "" };   // target airport name
  std::string loc_rw_s{ "" };   // localaizer runway
  std::string locType_s{ "" };  // localaizer type
  std::string surfType_s{ "" }; // Surface type v3.0.253.13

} mx_ils_airport_row_strct;


typedef struct _leg_enrout_stats_strct
{
  std::string sLegName{ "" };
  float       fDistanceFlew{ 0.0f };
  float       fCumulativeDistanceFlew_beforeCurrentLeg{ 0.0f }; // holds the distance flew before the start of the current leg.
  float       fStartingPayload{ 0.0f };
  float       fTouchDownWeight{ 0.0f }; // payload + fuel
  float       fTouchDown_acf_m_max{ 0.0f }; // Max weight allowed for plane at the touch down point. This to make sure user won't change plane after touch down and we will have wrong info.
  float       fEndPayload{ 0.0f };
  float       fStartingFuel{ 0.0f };
  float       fEndFuel{ 0.0f };

  float fMaxG{ 0.0f };
  float fMinG{ 0.0f };
  float fMaxPitch{ 0.0f };
  float fMinPitch{ 0.0f };
  float fMaxRoll{ 0.0f };
  float fMinRoll{ 0.0f };

  float fTouchDown_landing_rate_vh_ind_fpm{ 0.0f };
  float fTouchDown_gforce_normal{ 1.0f };
  float fLanding_vh_ind_fpm_avg_15_meters{ 0.0f };
  float fLanding_gforce_normal_avg_15_meters{ 0.0f };
  std::vector<float> vecFpm15Meters{};
  std::vector<float> vecgForce15Meters{};

  void init() { sLegName.clear();
    fDistanceFlew = fCumulativeDistanceFlew_beforeCurrentLeg = 0.0f;
    fStartingPayload = fEndPayload = 0.0f;
    fStartingFuel = fEndFuel = 0.0f;
    fTouchDownWeight = 0.0f;
    fTouchDown_acf_m_max     = 0.0f;
    fMaxG = fMinG = 0.0f;
    fMaxPitch = fMinPitch = 0.0f;
    fMaxRoll = fMinRoll = 0.0f;
    fTouchDown_landing_rate_vh_ind_fpm = fLanding_vh_ind_fpm_avg_15_meters = 0.0f;
    fTouchDown_gforce_normal = fLanding_gforce_normal_avg_15_meters = 0.0f;


    vecFpm15Meters.clear();
    vecgForce15Meters.clear();
  }

  void reset_fpm_and_gForce_data()
  { 
    fLanding_vh_ind_fpm_avg_15_meters = 0.0f;
    vecFpm15Meters.clear();

    fLanding_gforce_normal_avg_15_meters = 0.0f;
    vecgForce15Meters.clear();
  }

} mx_enrout_stats_strct;


typedef struct _mission_stats_strct // v3.0.255.1
{
  std::vector<double>      vecSeq_d; // line_id used by the plugin to distinguish between the rows
  std::vector<double>      vecElev_mt_d;
  std::vector<double>      vecElev_ft_d;
  std::vector<double>      vecFlaps_d;
  std::vector<double>      vecDayInYear_d;     // day in year
  std::vector<double>      vecLocalTime_sec_d; // local time from midnight
  std::vector<double>      vecAirSpeed_d;      //
  std::vector<double>      vecGroundSpeed_d;   //
  std::vector<double>      vecFaxil_d;         //
  std::vector<double>      vecRoll_d;          //
  std::vector<std::string> vecActivity_s;      // takeoff and landing, in most cases the value is empty

  double      min_elev_ft{ 0.0 }, max_elev_ft{ 0.0 }, min_airspeed{ 0.0 }, max_airspeed{ 0.0 };
  double      time_flew_sec_d{ 0.0 };

  // v3.303.8.3 MIN/MAX stats_vu
  double minPitch, maxPitch;
  double minRoll, maxRoll;
  double minGforce, maxGforce;
  double minFaxilG, maxFaxilG;
  double minAirspeed, maxAirspeed;
  double minGroundSpeed, maxGroundspeed;

  //std::string landing_stats_s{ "" }; // v3.303.8.3

  std::string time_flew_format_s{ "" };

  std::vector<missionx::mx_stats_data> vecLandingStatsResults;  // v3.303.8.3
  float                                pitchScore{ 0.0f }; // v3.303.8.3
  float                                rollScore{ 0.0f };  // v3.303.8.3
  float                                gForceScore{ 0.0f }; // v3.303.8.3

  _mission_stats_strct() { reset(); }

  void reset()
  {
    vecSeq_d.clear();
    vecElev_mt_d.clear();
    vecElev_ft_d.clear();
    vecFlaps_d.clear();
    vecDayInYear_d.clear();
    vecLocalTime_sec_d.clear();
    vecLocalTime_sec_d.clear();
    vecAirSpeed_d.clear();
    vecFaxil_d.clear();
    vecRoll_d.clear();
    vecActivity_s.clear();

    min_elev_ft = max_elev_ft = -4000.0;
    min_airspeed = max_airspeed = 0.0;

    time_flew_sec_d = 0.0;
    time_flew_format_s.clear();

    vecLandingStatsResults.clear();

    pitchScore = rollScore = gForceScore = 0.0f;
  }

} mx_mission_stats_strct;

typedef struct _mx_clock_time
{
  static constexpr int CHRONO_START_YEAR = 1900;

  int year{ CHRONO_START_YEAR };
  int month{ 1 };
  int dayInMonth{ 0 };
  int dayInYear{ 0 }; // starts in 0..355
  int hour{ 0 };
  int seconds_in_day{ 0 };

} mx_clock_time_strct;

typedef struct _mx_aptdat_cached_info
{
  bool                   isCustom{ false };
  std::string            boundary{ "" }; // v3.303.8.3 each node(x,y) will be terminated with "|"
  std::list<std::string> listNavInfo;

  void clear()
  {
    isCustom = false;
    boundary.clear();
    listNavInfo.clear();
  }

} mx_aptdat_cached_info;


//// https://github.com/jpreiss/array2d


  namespace dref
  {
    const static std::string DREF_SHARED_DRAW_ROTATION = "missionx/obj3d/rotation_per_frame_1_min";

  }


} // end missionx namespace




////////////////////////////////////////////////////
////////////////////////////////////////////////////
/////////////// MXCONST MXCONST MXCONST MXCONST 
////////////////////////////////////////////////////
////////////////////////////////////////////////////


namespace mxconst
{
namespace conv
{
constexpr int DEFAULT_RADIUS_LENGTH_FOR_GROUND_WP = 60;
constexpr int DEFAULT_RADIUS_LENGTH_FOR_AIRBORNE_WP = 2000;

};


inline static const std::vector<const char*> vecMarkerTypeOptions = { "Default (point down)", "Point up 15m", "Point down 15m", "Tall point down 15x60", "Point down 60m", "Point up 60m", "Static Mission-X Text (40m)", "Rotate Mission-X Text (40m)", "Smooth Rotate Mission-X Text (40m)", "Smooth Rotate Mission-X Text (250m)", "Smooth Rotate X (40m)", "Smooth Rotate X (128m)" };
inline static const std::vector<std::string> vecMarkerTypeOptions_markers = { "marker01", "marker02", "marker03", "marker04", "marker05", "marker06", "marker07", "marker08", "marker09", "marker10", "marker11", "marker12" };

inline const static std::string DEFAULT_CYCLE         = "1802";
inline const static std::string DEFAULT_FONT_LOCATION_0 = "./Resources/plugins/missionx/libs/fonts/DejaVuSansMono.ttf";
inline const static std::string DEFAULT_FONT_LOCATION_1 = "./Resources/plugins/missionx/libs/fonts/Inconsolata.ttf";
inline const static std::string DEFAULT_FONT_LOCATION_2 = "./Resources/plugins/missionx/libs/fonts/Roboto-Bold.ttf"; 
inline const static std::string DEFAULT_FONT_LOCATION_3 = "./Resources/plugins/missionx/libs/fonts/EBGaramond-Bold.ttf";

//inline constexpr const static int TEXT_ICON_POS  = 0;
//inline constexpr const static int TEXT_TITLE_POS = 1;
//inline constexpr const static int TEXT_OPT_POS   = 2;
//inline constexpr const static int TEXT_DEF_POS   = 3;

inline const static std::string TEXT_TYPE_DEFAULT        = "default_font";
inline const static std::string TEXT_TYPE_DEFAULT_PLUS_1 = "default_font+1";
inline const static std::string TEXT_TYPE_TITLE_SMALL    = "title_small";     // title small 14px
inline const static std::string TEXT_TYPE_TITLE_SMALLEST = "title_smallest";     // title smallest = 13px
inline const static std::string TEXT_TYPE_TITLE_REG      = "title_reg";     // title regular
inline const static std::string TEXT_TYPE_TITLE_MED      = "title_med";     // title medium
inline const static std::string TEXT_TYPE_TITLE_BIG      = "title_big";     // title big
inline const static std::string TEXT_TYPE_TEXT_REG       = "text_reg";      // text_reg
inline const static std::string TEXT_TYPE_TEXT_MED       = "text_med";      // text medium
inline const static std::string TEXT_TYPE_TEXT_SMALL     = "text_small";    // text small
inline const static std::string TEXT_TYPE_MSG_BOTTOM     = "msg_bottom";    // msg_bottom
inline const static std::string TEXT_TYPE_MSG_POPUP      = "msg_popup";     // msg popup
inline const static std::string TEXT_TYPE_TITLE_TOOLBAR  = "title_toolbar"; // title toolbar


inline const static float FONT_PIXEL_13 = 13.0f; // v3.0.251.1 holds preferred font pixel size
inline const static float FONT_PIXEL_14 = 14.0f; // v3.303.14 holds preferred font pixel size for bigger characters
inline const static float FONT_PIXEL_15 = 15.0f; // v3.303.14 holds preferred font pixel size for bigger characters
inline const static float FONT_PIXEL_16 = 16.0f; // v3.303.14 holds preferred font pixel size for bigger characters
inline const static float FONT_PIXEL_18 = 18.0f; // v3.303.14 holds preferred font pixel size for bigger characters
inline const static float FONT_PIXEL_20 = 20.0f; // v3.303.14 holds preferred font pixel size for bigger characters
inline const static float FONT_PIXEL_22 = 22.0f; // v3.303.14 holds preferred font pixel size for bigger characters
inline const static float FONT_PIXEL_24 = 24.0f; // v3.303.14 holds preferred font pixel size for bigger characters
inline const static float FONT_PIXEL_26 = 26.0f; // v3.303.14 holds preferred font pixel size for bigger characters
inline const static float FONT_PIXEL_32 = 32.0f; // v3.303.14 holds preferred font pixel size for bigger characters


// static float FONT_SIZE{ 13.0f };
constexpr float DEFAULT_MIN_FONT_PIXEL_SIZE = 12.0f;
constexpr float DEFAULT_MAX_FONT_PIXEL_SIZE = 16.0f;

constexpr float DEFAULT_MIN_FONT_SCALE  = 0.8f;
constexpr float DEFAULT_MAX_FONT_SCALE  = 1.4f;
constexpr float DEFAULT_BASE_FONT_SCALE = 1.0f;

// FILE NAMES
inline static const std::string CUSTOM_APT_DAT_FILE = "customAptDat.txt"; // v3.303.12_r2 custom apt dat file that we want to deprecate in near future

// General
inline const static std::string UNIX_EOL                      = "\n";           // used in Unix like OSes for end of line
inline const static std::string WIN_EOL                       = "\r\n";         // Windows end of line
inline const static std::string QM                            = "\"";           // Quotation Mark
inline const static std::string SPACE                         = " ";            // SPACE
inline const static std::string FOLDER_SEPARATOR              = "/";            // folder separator
inline const static std::string BRIEFER_FOLDER                = "briefer";      // v24.12.1 refactor // v3.0.x name of the mandatory folder that the mission file/s will reside
inline const static std::string TEMPLATE_FILE_NAME            = "template.xml"; // v3.0.241.10 b2 define a template file in a custom mission folder
inline const static std::string FOLDER_RANDOM_MISSION_NAME    = "random";       // v3.0.217.3 name of the random folder
inline const static std::string RANDOM_MISSION_DATA_FILE_NAME = "random.xml";   // v3.0.217.3 name of the random folder
inline const static std::string COLON                         = ":";            // v3.0.223.6
inline const static std::string COMMA_DELIMITER               = ",";
inline const static std::string PIPE_DELIMITER                = "|";


inline const double TWNENTY_METERS_D                              = 20.0; // v3.0.241.10 b3 - used in position plane. This is the threshold distance in meters that will be converted to NM
inline const double MIN_ACCEPTABLE_DISTANCE_TO_GUESS_WP_TYPE_NM_D = 1.0;  // v3.0.255.2 - used in guessing waypoint type.


inline const static std::string CONVERTER_FILE = "converter.sav";

//////// FROM Mission-X V2 ///////////////////////////
/////////////////////////////////////////////////////


//// Regular Expression constants
// const static std::string MX_REGEX_ARRAY_AT_THE_END_OF_STRING   = "\\[\\d+\\]$"; // a string that has array like ending
// const static std::string MX_REGEX_INTEGER = "\\d+"; // number in a string
// const static std::string MX_ERR_WRONG_ARRAY_NUMBER = "Wrong array number, check parameter settings!"; // ERROR when dataref array represent wrong array size


// V3.0.0 /// V3.0.0 /// V3.0.0 /// V3.0.0 /// V3.0.0 ///// V3.0.0 /// V3.0.0 /// V3.0.0 /// V3.0.0 /// V3.0.0 ///
// V3.0.0 /// V3.0.0 /// V3.0.0 /// V3.0.0 /// V3.0.0 ///// V3.0.0 /// V3.0.0 /// V3.0.0 /// V3.0.0 /// V3.0.0 ///
// V3.0.0 /// V3.0.0 /// V3.0.0 /// V3.0.0 /// V3.0.0 ///// V3.0.0 /// V3.0.0 /// V3.0.0 /// V3.0.0 /// V3.0.0 ///

// custom dataref
inline const static std::string FILE_CUSTOM_ACF_CACHED_DATAREFS_NAME = "missionx_cached_datarefs.txt"; // v3.303.9.1


// Common Attributes
inline const static std::string ATTRIB_MXFEATURE                = "mxfeature";      // v3.0.255.3
inline const static std::string ATTRIB_XP_VERSION               = "xp_version";     // v3.303.12
inline const static std::string ATTRIB_PLUGIN_VERSION           = "plugin_version"; // v24.02.5
inline const static std::string ELEMENT_SETUP                   = "setup";          // v3.0.255.3 // v3.0.255.4.2 to lower case
inline const static std::string ELEMENT_NODE                    = "node";           // v3.303.11 part of data_manager::mx_base_node class and should use instead of mxProprties class when only dealing with XML
inline const static std::string ELEMENT_OPTIONS_CAPITAL_LETTERS = "OPTIONS";        // v3.0.255.4.2 - will replace <options>
inline const static std::string ELEMENT_TEMPLATE_REPLACE_OPTIONS = "REPLACE_OPTIONS"; // v3.0.255.4.1 used in template injection XML
inline const static std::string ELEMENT_OPT                      = "opt";             // v3.0.255.4
inline const static std::string ELEMENT_FIND_REPLACE             = "find_replace";    // v3.0.255.4.1 used in Random::inject_file_info()
inline const static std::string ELEMENT_INFO                     = "info";            // v3.0.255.4.1 <info> is a sub element of <opt> element. All its attributes in <info> will be copied over mission_info
inline const static std::string ELEMENT_ERROR                    = "error";           // v3.305.3 use with log formatting
inline const static std::string ELEMENT_WARNING                  = "warning";         // v3.305.3 use with log formatting
inline const static std::string ATTRIB_ID                        = "id";
inline const static std::string ATTRIB_TITLE                     = "title";
inline const static std::string ATTRIB_OPTIONAL                  = "optional";

inline const static std::string ATTRIB_STARTING_ICAO = "starting_icao";



inline const static int MAX_ITEM_IN_INVENTORY = 15; // v3.0.213.4 // used in em_reset_inventory_item_table() function.
inline const static float AGL_TO_GATHER_FPM_DATA = 15.0f; // v3.0.213.4 // used in em_reset_inventory_item_table() function.

inline const static int INT_UNDEFINED = -1; //
inline const static int INT_FIRST_0   = 0;  //

inline const static std::string ZERO  = "0";  //
inline const static std::string ONE   = "1";  //
inline const static std::string TWO   = "2";  //
inline const static std::string THREE = "3";  //
inline const static std::string FOUR  = "4";  //
inline const static std::string FIVE  = "5";  //
inline const static std::string SIX   = "6";  //
inline const static std::string SEVEN = "7";  //
inline const static std::string EIGHT = "8";  //
inline const static std::string NINE  = "9";  //
inline const static std::string TEN   = "10"; //
inline const static std::string TWELVE    = "12"; //
inline const static std::string FORTEEN   = "14"; //
inline const static std::string FIFTEEN   = "15"; //
inline const static std::string SIXTEEN   = "16"; //

// Mission Data - XML

// missionx configuration file
inline const static std::string MISSIONX_CONF_FILE = "missionx.cfg"; // v3.0.255.4.1
inline const static std::string MISSIONX_ROOT_DOC  = "MISSIONX";     // v3.0.255.4.1
inline const static std::string ELEMENT_OVERPASS   = "overpass";     // v3.0.255.4.1
inline const static std::string ELEMENT_URL        = "url";          // v3.0.255.4.1

// const static std::string_view OVERPASS_XML_URLS = R"(<OVERPASS>
//  <url>https://lz4.overpass-api.de/api/interpreter</url>
//  <url>https://z.overpass-api.de/api/interpreter</url>
//  <url>https://overpass.openstreetmap.ru/api/interpreter</url>
//  <url>https://overpass.openstreetmap.fr/api/interpreter</url>
//  <url>https://overpass.kumi.systems/api/interpreter</url>
//</OVERPASS>)";


inline const static std::string OVERPASS_XML_URLS = "<" + ELEMENT_OVERPASS + ">" + "<url>https://lz4.overpass-api.de/api/interpreter</url>       " +
                                                    "<url>https://z.overpass-api.de/api/interpreter</url>         " + 
                                                    "<url>https://overpass.openstreetmap.ru/api/interpreter</url> " + 
                                                    "<url>https://overpass.openstreetmap.fr/api/interpreter</url> " +
                                                    "<url>https://overpass.kumi.systems/api/interpreter</url>     " +
                                                    "</" + ELEMENT_OVERPASS + ">";


inline const static std::string MISSION_ELEMENT              = "MISSION";
inline const static std::string ELEMENT_METADATA             = "metadata"; // v24.12.1 holds user generated type mission and subcategory.
inline const static std::string ATTRIB_CATEGORY              = "category"; // v24.12.1 holds user generated type mission and subcategory.
inline const static std::string ATTRIB_UI_LAYER              = "ui_layer"; // v24.12.1 from which layer the mission was generated ?
inline const static std::string CONVERSION_ROOT_DOC          = "CONVERSION";
inline const static std::string DUMMY_ROOT_DOC               = "DUMMY";
inline const static std::string TEMPLATE_ROOT_DOC            = "TEMPLATE";
inline const static std::string MAPPING_ROOT_DOC             = "MAPPING";
inline const static std::string MISSION_ROOT_SAVE_DOC        = "SAVE";
inline const static std::string ATTRIB_VERSION               = "version";        // MISSION
inline const static std::string ATTRIB_MISSION_FILE_FORMAT   = "mission_file_format"; // v25.03.1 used with template. Will hold the target mission file format (301 or 312 as an example)
inline const static std::string ATTRIB_MISSION_DESIGNER_MODE = "designer_mode";  // MISSION
inline const static std::string MX_FILE_SAVE_EXTENTION       = ".savepoint.sav"; // MISSION //
inline const static std::string MX_FILE_SAVE_DREF_EXTENTION  = ".dataref.txt";   // MISSION //
inline const static std::string ATTRIB_TYPE                  = "type";           // trigger // refuel_zone
inline const static std::string ATTRIB_ELEV_MIN_FT           = "elev_min_ft";    // v3.0.303.4 trigger elevation
inline const static std::string ATTRIB_ELEV_MAX_FT           = "elev_max_ft";    // trigger elevation
inline const static std::string ATTRIB_RE_ARM                = "rearm";          // v3.0.219.1 used in trigger. Will be set when leaving trigger
inline const static std::string XML_EXTENTION                = ".xml";           // v3.0.241.10 b3 refine random osm

inline const static std::string ELEMENT_DESIGNER       = "designer";
inline const static std::string ATTRIB_FORCE_LEG_NAME  = "force_leg_name";  // v3.0.221.15rc5 add LEG support. global settings - designer
// inline const static std::string ATTRIB_FORCE_GOAL_NAME = "force_goal_name"; // global settings - designer
inline const static std::string ELEMENT_DATAREF        = "dataref";         //
inline const static std::string GLOBAL_SETTINGS        = "global_settings";
inline const static std::string ATTRIB_METAR_FILE_NAME = "metar_file_name"; //

inline const static std::string ELEMENT_COMPATIBILITY    = "compatibility";  // v24.12.2 a global_setting sub element, used to set compatibility flags for older missions.
inline const static std::string ATTRIB_INVENTORY_LAYOUT  = "inventory_layout"; // v24.12.2 force mission inventory layout. Values "[11|12]". USed with <compatibility> element.


// Time
inline const static std::string ATTRIB_TIME_HOURS       = "hours";       // global settings
inline const static std::string ATTRIB_TIME_MIN         = "min";         // global settings
inline const static std::string ELEMENT_START_TIME      = "start_time";
inline const static std::string ATTRIB_TIME_DAY_IN_YEAR = "day_in_year"; // global settings
inline const static std::string ATTRIB_LOCAL_TIME_SEC   = "local_time_sec"; // global settings // v3.303.8.3

// Mission Description
inline const static std::string ELEMENT_MISSION_INFO            = "mission_info";             // Holds information for mission briefing
inline const static std::string ATTRIB_MISSION_IMAGE_FILE_NAME  = "mission_image_file_name";  // mission briefing
inline const static std::string ATTRIB_TEMPLATE_IMAGE_FILE_NAME = "template_image_file_name"; // template image
inline const static std::string ATTRIB_PLANE_DESC               = "plane_desc";               // mission briefing
inline const static std::string ATTRIB_ESTIMATE_TIME            = "estimate_time";            // mission briefing
inline const static std::string ATTRIB_DIFFICULTY               = "difficulty";               // mission briefing
inline const static std::string ATTRIB_WEATHER_SETTINGS         = "weather_settings";         // weather briefing
inline const static std::string ATTRIB_SCENERY_SETTINGS         = "scenery_settings";         // scenery briefing
inline const static std::string ATTRIB_WRITTEN_BY               = "written_by";               // scenery briefing
inline const static std::string ATTRIB_OTHER_SETTINGS           = "other_settings";           // weather briefing
inline const static std::string ATTRIB_SHORT_DESC               = "short_desc";               // short description

// Save checkpoint
inline const static std::string ELEMENT_PROPERTIES             = "properties";                // properties, for checkpoint
inline const static std::string ELEMENT_SAVE                   = MISSION_ROOT_SAVE_DOC;       // for checkpoint
inline const static std::string PROP_MISSION_PROPERTIES        = "mission_properties";        // for checkpoint
inline const static std::string PROP_MISSION_FOLDER_PROPERTIES = "mission_folder_properties"; // for checkpoint

// FOLDER PROPERTIES
inline const static std::string ELEMENT_FOLDERS                    = "folders";                     //
inline const static std::string ATTRIB_MISSION_PACKAGE_FOLDER_PATH = "mission_package_folder_path"; // v3.0.213.7 renamed  from "missions_root_folder_name" to "MISSION_PACKAGE_FOLDER_PATH" for better readability
inline const static std::string ATTRIB_SOUND_FOLDER_NAME           = "sound_folder_name";  
inline const static std::string ATTRIB_OBJ3D_FOLDER_NAME           = "obj3d_folder_name";  
inline const static std::string ATTRIB_METAR_FOLDER_NAME           = "metar_folder_name";  
inline const static std::string ATTRIB_SCRIPT_FOLDER_NAME          = "script_folder_name"; 


inline const static std::string PROP_XPLANE_VERSION            = "xplane_version"; // v3.303.8
inline const static std::string PROP_XPLANE_INSTALL_PATH       = "xplane_install_path";
inline const static std::string PROP_XPLANE_PLUGINS_PATH       = "xplane_plugins_path";
inline const static std::string PROP_MISSIONX_PATH             = "missionx_path";
inline const static std::string PROP_MISSIONX_BITMAP_PATH      = "missionx_bitmap_path"; // v3.0.118
inline const static std::string FLD_MISSIONX_SAVE_PATH         = "missionx_save_path";
inline const static std::string FLD_MISSIONX_LOG_PATH          = "missionx_log_path";
inline const static std::string FLD_MISSIONX_SAVEPOINT_PATH    = "missionx_savepoint_path";
inline const static std::string FLD_MISSIONX_SAVEPOINT_DREF    = "missionx_savepoint_dref";
inline const static std::string FLD_MISSIONS_ROOT_PATH         = "missions_root_folder";                  // currently "custom scenery/missionx"
inline const static std::string FLD_RANDOM_TEMPLATES_PATH      = "missionx_random_templates_folder_path"; // v3.0.217.1 currently "Resources/plugin/missionx/templates"
inline const static std::string FLD_RANDOM_MISSION_PATH        = "missionx_random_mission_folder_path";   // v3.0.217.1 currently "custom scenery/missionx/random"
inline const static std::string FLD_CUSTOM_SCENERY_FOLDER_PATH = "xplane_custom_scenery_folder_path";     // v3.0.219.9 currently "custom scenery"
inline const static std::string FLD_DEFAULT_APTDATA_PATH       = "xplane_default_aptdata_folder_path";    // v3.0.219.9 currently "xp11/Resources/default scenery/default apt dat/"

inline const static std::string ATTRIB_NAME                = "name";
inline const static std::string ATTRIB_UNIQUE_ELEMENT_NAME = "unique_element_name"; // v3.0.217.4
inline const static std::string ELEMENT_DESC               = "desc";
inline const static std::string ATTRIB_IS_TARGET_POI       = "is_target_poi"; // v3.0.303 boolean attribute to flag a task as the target position. Used to write the position to xpshared/target_lat|target_lon
inline const static std::string ATTRIB_TARGET_LAT          = "target_lat"; // v3.0.303 Store task lat in <leg >
inline const static std::string ATTRIB_TARGET_LON          = "target_lon"; // v3.0.303 Store task lon in <leg >
inline const static std::string ATTRIB_IS_DUMMY             = "is_dummy";   // v3.0.303_12 used with <leg> element. It will ignore all objective/tasks and triggers tests and will only execute: "pre/post scripts", "pre/post commands" and "<display_object{_xxx}>" elements consider to also evaluate linked triggers at <leg> level. Will always flagged as success after one cycle.
inline const static std::string ATTRIB_DUMMY_LEG_ITERATIONS = "dummy_leg_iterations"; // v3.0.303_12 holds a counter, how many times the dummy leg was iterated. If larger than 2 then we can flag it is successfull and continue to next leg


// METAR
inline const static std::string ELEMENT_METAR                  = "metar";                   //
inline const static std::string ATTRIB_INJECT_METAR_FILE       = "inject_metar_file";       // v3.0.224.1 used in message element and will be called once text message has been broadcasted.
inline const static std::string XPLANE_METAR_FILENAME          = "METAR.rwx";               //
inline const static std::string ATTRIB_FORCE_CUSTOM_METAR_FILE = "force_custom_metar_file"; // bool value "0 = no/false; 1 = yes/true


// Map 2D
inline const static std::string ELEMENT_MAP = "map"; //



inline const static std::string ATTRIB_MANDATORY = "mandatory";
inline const static std::string PROP_TASK_STATE  = "task_state";

inline const static std::string ATTRIB_MAP_WIDTH     = "map_width";
inline const static std::string ATTRIB_MAP_HEIGHT    = "map_height";
inline const static std::string ATTRIB_MAP_FILE_NAME = "map_file_name";

inline const static std::string ELEMENT_EMBEDED_SCRIPTS  = "embedded_scripts";
inline const static std::string ELEMENT_FILE             = "file";
inline const static std::string ELEMENT_INCLUDE_FILE     = "include_file";
inline const static std::string ATTRIB_INCLUDE_FILE      = ELEMENT_INCLUDE_FILE; // for the sake of readability
inline const static std::string ELEMENT_SCRIPTLET        = "scriptlet";
inline const static std::string ELEMENT_SHARED_VARIABLES = "shared_variables"; // v3.0.202a // root of all shared variables
inline const static std::string ELEMENT_VAR              = "var";              // v3.0.202a // represent a shared variable element
inline const static std::string ATTRIB_INIT_VAL          = "init_val";         // v3.0.202a // represent a shared variable element

inline const static std::string ELEMENT_SCRIPT_GLOBAL_STRING_PARAMS = "global_string_params";
inline const static std::string ELEMENT_SCRIPT_GLOBAL_NUMBER_PARAMS = "global_number_params";
inline const static std::string ELEMENT_SCRIPT_GLOBAL_BOOL_PARAMS   = "global_bool_params";


inline const static std::string ELEMENT_LOGIC  = "logic";
inline const static std::string ELEMENT_INTERPOLATION  = "interpolation"; // v3.305.3 used with save/load checkpoint
inline const static std::string ELEMENT_XPDATA = "xpdata";
inline const static std::string ELEMENT_TASK = "task";

inline const static std::string ATTRIB_BASE_ON_TRIGGER        = "base_on_trigger";        // trigger name that will evaluate the task from physical perspectives (can have logical aspects too).
inline const static std::string ATTRIB_BASE_ON_SCRIPT         = "base_on_script";         // script will evaluate validity of task or other action. Not meant for physical location.
inline const static std::string ATTRIB_BASE_ON_COMMAND        = "base_on_command";        // v3.0.221.10 Holds command path
inline const static std::string ATTRIB_BASE_ON_SLING_LOAD     = "base_on_sling_load";     // v3.0.303 sling load
inline const static std::string ATTRIB_IS_PLACEHOLDER         = "is_placeholder";         // v3.0.303.7 placeholder task. Meaning its state will be changed through indirect code, like a script from other trigger or as an outcome of a message or choice
inline const static std::string ATTRIB_EVAL_SUCCESS_FOR_N_SEC = "eval_success_for_n_sec"; // Flag task as success only if its conditions are met for at least N seconds
inline const static std::string ATTRIB_FORCE_EVALUATION = "force_evaluation"; // renamed in v3.0.205.3 from continues_evaluation // "always_evaluate"; // Precede "eval_success_for_n_sec". Even it task was flaged as success, it will continue to
                                                                       // evaluate its conditions until Objective will be flagged as success.
inline const static std::string ATTRIB_CUMULATIVE_TIMER_FLAG = "cumulative_timer_flag";   // v3.0.221.11 boolean - flag if timer should be cumulative or not. Example: when evaluating seconds for success, you are not allowed to leave the area of
                                                                                 // effect. It will reset the timer. Cumulative will "just" pause and then un-pause the timer.


inline const static std::string ELEMENT_OBJECTIVES     = "objectives";
inline const static std::string ELEMENT_OBJECTIVE      = "objective";
inline const static std::string ATTRIB_TASK_NAME       = "task_name";
//const static std::string ATTRIB_ALIAS_NAME      = "alias_name"; // v3.0.303.7 deprecated
inline const static std::string ATTRIB_DEPENDS_ON_TASK = "depends_on_task"; // depends on a task in same objective


inline const static std::string ELEMENT_MX_CHOICES      = "mx_choices";       // v3.0.231.1 plugin root element for all loaded choices. Used internally by the plugin and not the user. also used in save checkpoint.
inline const static std::string ELEMENT_CHOICES         = "choices";          // v3.0.231.1 The container for choices
inline const static std::string ELEMENT_CHOICE          = "choice";           // v3.0.231.1
inline const static std::string ELEMENT_OPTION          = "option";           // v3.0.231.1
inline const static std::string ELEMENT_OPTION_GROUP    = "option_group";     // v24.12.2
inline const static std::string ATTRIB_TEXT             = "text";             // v3.0.231.1
inline const static std::string ATTRIB_ONETIME_OPTION_B = "onetime_option_b"; // v3.0.231.1
inline const static std::string ATTRIB_NEXT_CHOICE      = "next_choice";      // v3.0.231.1 - follow up choice to display
inline const static std::string ATTRIB_ACTIVE_CHOICE    = "active_choice";    // v3.0.231.1


inline const static std::string ELEMENT_FMS                     = "fms";                  // v3.0.231.1
inline const static std::string ELEMENT_GPS                     = "gps";                  // v3.0.217.4 holds GPS points for generated mission and maybe to regular missions too
inline const static std::string ATTRIB_GPS_DISPLAY_FULL_ROUTE_B = "display_full_route_b"; // v3.0.253.7
inline const static std::string ATTRIB_DESTINATION_ENTRY        = "destination_entry";    // v3.0.219.7 add GPS/FMS current destination flying to.
inline const static std::string ELEMENT_FMS_ENTRY               = "fms_entry";            // v3.0.219.7 add GPS/FMS current destination flying to.

inline const static std::string ELEMENT_GOALS                   = "goals";
inline const static std::string ELEMENT_GOAL                    = "goal";
inline const static std::string ELEMENT_FLIGHT_PLAN             = "flight_plan";             // v3.0.221.15rc4 will replace <goals>
inline const static std::string ELEMENT_LEG                     = "leg";                     // v3.0.221.15rc4 will replace <goal>. represent a leg in a flight plan
inline const static std::string ELEMENT_WEATHER                 = "weather";                 // v3.0.303.12 sub element <weather> to use in <leg> parent element
inline const static std::string ELEMENT_SAVED_WEATHER           = "saved_weather";           // v3.0.303.13 sub element <saved_weather> saved <global_settings" to apply the weather during mission start and only if was loaded from save point.
inline const static std::string ELEMENT_SPECIAL_LEG_DIRECTIVES  = "special_leg_directives";  // v3.0.221.15rc5 replaces ELEMENT_SPECIAL_GOAL_DIRECTIVES
inline const static std::string ELEMENT_SPECIAL_GOAL_DIRECTIVES = "special_goal_directives"; // v3.0.221.8
inline const static std::string ELEMENT_LINK_TO_OBJECTIVE       = "link_to_objective";
inline const static std::string ELEMENT_LINK_TO_TRIGGER         = "link_to_trigger";
inline const static std::string ELEMENT_START_LEG_MESSAGE       = "start_leg_message";
inline const static std::string ELEMENT_START_GOAL_MESSAGE      = "start_goal_message";
inline const static std::string PROP_START_LEG_MESSAGE_FIRED    = "start_leg_message_fired"; // v3.0.221.15rc5 replaced PROP_START_GOAL_MESSAGE_FIRED // v3.0.213.7 add to goal after message was broadcasted, so won't fire after load checkpoint
inline const static std::string ELEMENT_END_LEG_MESSAGE         = "end_leg_message";         // v3.0.221.15rc5 replaced  ELEMENT_END_GOAL_MESSAGE
inline const static std::string ELEMENT_END_GOAL_MESSAGE        = "end_goal_message";        // compatibility
inline const static std::string ELEMENT_PRE_LEG_SCRIPT          = "pre_leg_script";
inline const static std::string PROP_PRE_LEG_SCRIPT_FIRED       = "pre_leg_script_fired"; // v3.0.221.15rc5 replaced PROP_PRE_GOAL_SCRIPT_FIRED // v3.0.213.7 add to goal after script was fired, so won't fire after load checkpoint
inline const static std::string ELEMENT_POST_LEG_SCRIPT         = "post_leg_script";
inline const static std::string ATTRIB_DISPLAY_AT_POST_LEG_B    = "display_at_post_leg_b"; // v3.0.303.11 Used to display 3D Objects only when a flight leg was ended and after "post_leg_script"
inline const static std::string ATTRIB_NEXT_LEG                 = "next_leg";
inline const static std::string ATTRIB_NEXT_GOAL                = "next_goal";

inline const static std::string ELEMENT_DRAW_SCRIPT = "draw_script"; // v3.0.224.2 <draw_script name="" /> allow embeded scripts to be executed in the draw callback.
// Leg Messages
inline const static std::string DYNAMIC_MESSAGE              = "dynamic_message"; // v3.0.224.4
inline const static std::string PREFIX_DYN_MESSAGE_NAME      = "msg_4_dyn_message_"; // v3.0.224.4
inline const static std::string PREFIX_TRIG_DYN_MESSAGE_NAME = "trig_dyn_message_"; // v3.0.224.4
inline const static std::string ATTRIB_MESSAGE_NAME_TO_CALL  = "message_name_to_call"; // v3.0.224.3
inline const static std::string ATTRIB_RELATIVE_TO_TASK      = "relative_to_task"; // v3.0.224.3
inline const static std::string ATTRIB_RELATIVE_TO_TRIGGER   = "relative_to_trigger"; // v3.0.224.3
inline const static std::string ATTRIB_OVERRIDE_TASK_NAME    = "override_task_name"; // v3.0.224.3 used only in random templates. Will replace the generated task name with the overrided one. in <expected_location location_type="xy"
                                                                                  // location_value="tag=loc_acc|nm_between=2-30" override_task_name="task_name1" override_trigger_name="trig_name1" />
inline const static std::string ATTRIB_OVERRIDE_TRIGGER_NAME = "override_trigger_name"; // v3.0.224.3 only in random templates. Will replace the generated trigger name with the overrided one
// v3.0.223.4 will be used in random mission template in <leg>. This should disable all distance messages that being injected in RandomEngine::injectMessagesWhileFlyingToDestination().
inline const static std::string ATTRIB_DISABLE_AUTO_MESSAGE_B       = "disable_auto_message_b";
inline const static std::string ATTRIB_SUPPRESS_DISTANCE_MESSAGES_B = "suppress_distance_messages_b"; // v25.02.1
// End Leg Messages


inline const static std::string ELEMENT_BRIEFER                    = "briefer";
inline const static std::string ATTRIB_STARTING_LEG                = "starting_leg";  // v3.0.221.15rc5 replaced ATTRIB_STARTING_GOAL
inline const static std::string ATTRIB_STARTING_GOAL               = "starting_goal"; // compatibility
inline const static std::string ATTRIB_POSITION_PREF               = "position_pref"; // v3.0.301 B4. Valid options: xp10/xp11
inline const static std::string ELEMENT_LOCATION_ADJUST            = "location_adjust";
inline const static std::string ATTRIB_LAT                         = "lat";
inline const static std::string ATTRIB_LONG                        = "long";
inline const static std::string ATTRIB_LAT_OSM                     = "lat";                   // v3.0.253.4 osm latitude
inline const static std::string ATTRIB_LONG_OSM                    = "lon";                   // v3.0.253.4 osm longitude
inline const static std::string ATTRIB_ELEV_FT                     = "elev_ft";               // under trigger
inline const static std::string ATTRIB_HEADING_PSI                 = "heading_psi";           // psi - The true heading of the aircraft in degrees from the Z axis - OpenGL coordinates
inline const static std::string ATTRIB_FORCE_HEADING_B             = "force_heading_b";       // v3.0.301 B4, force heading of plane even if plane is in 20m radius from starting location. Mainly for designers and not the simmers
inline const static std::string ATTRIB_FORCE_POSITION_B            = "force_position_b";      // v3.303.12_r2
inline const static std::string ATTRIB_ADJUST_HEADING              = "adjust_heading";        // v3.0.207.5 +/- values to adjust heading of moving object
inline const static std::string ATTRIB_PITCH                       = "pitch";                 //
inline const static std::string ATTRIB_ROLL                        = "roll";                  //
inline const static std::string ATTRIB_STARTING_SPEED_MT_SEC       = "starting_speed_mt_sec"; // starting speed in meters per seconds
inline const static std::string ATTRIB_PAUSE_AFTER_LOCATION_ADJUST = "pause_after_location_adjust";
inline const static std::string PROP_CURRENT_LOCATION              = "current_location";
inline const static std::string PROP_POINT_DATA                    = "point_data";

inline const static std::string ATTRIB_RAMP_INFO = "ramp_info"; // v3.0.253.1 ramp info is used in RandomEngine::prepare_mission_based_on_external_fpln() for briefer debug info

// Sling Load - v3.0.303
inline const static std::string ATTRIB_START_LAT = "start_lat";
inline const static std::string ATTRIB_START_LON = "start_lon";
inline const static std::string ATTRIB_END_LAT     = "end_lat";
inline const static std::string ATTRIB_END_LON     = "end_lon";
inline const static std::string ATTRIB_INIT_SCRIPT = "init_script";


// 3D Objects // v3.0.200
inline const static std::string PROP_OBJECTS_ROOT      = "objects_root";      // savepoint
inline const static std::string PROP_OBJECTS_INSTANCES = "objects_instances"; // savepoint

inline const static std::string ELEMENT_OBJECT_TEMPLATES                 = "object_templates";
inline const static std::string ELEMENT_OBJ3D                            = "obj3d";
inline const static std::string ELEMENT_PATH                             = "path";                                // v3.0.207.1 renamed to "path". Holds points of movement path
inline const static std::string ATTRIB_CYCLE                             = "cycle";                               // v3.0.207.1 cycle path ?
inline const static std::string PROP_POINT_FROM                          = "point_from";                          // v3.0.213.7  obj3d.pointFrom
inline const static std::string PROP_POINT_TO                            = "point_to";                            // v3.0.213.7  obj3d.pointTo
inline const static std::string PROP_CURRENT_POINT_NO                    = "current_point_no";                    // v3.0.213.7 obj3d.currentPointNo
inline const static std::string PROP_INSTANCE_DATA_ELEMENT               = "instance_data";                       // v3.0.213.7 obj3d instance element for checkpoint
inline const static std::string PROP_INSTANCE_SPECIAL_PROPERTIES         = "instance_special_properties";         // v3.0.213.7 obj3d instance element for checkpoint
inline const static std::string PROP_GOAL_INSTANCES_SPECIAL_DATA_ELEMENT = "goal_instances_special_data_element"; // v3.0.213.7 Will use after load checkpoint to apply on loaded instances (if any)
inline const static std::string PROP_LOADED_FROM_CHECKPOINT = "loaded_from_checkpoint"; // v3.0.213.7 we will add this to Goal elements, so we will know that instance objects should be read from the instance data in the checkpoint file.
inline const static std::string PROP_GOAL_MAP2D             = "goals_maps";             // v3.0.213.7 we will add this to Goal elements, so we will know that instance objects should be read from the instance data in the checkpoint file.


inline const static std::string ELEMENT_LOCATION       = "location";       // lat long elev_ft
inline const static std::string ELEMENT_TILT           = "tilt";           // heading pitch roll
inline const static std::string ELEMENT_DISPLAY_OBJECT = "display_object"; // "flight_leg" element. Inform plugin to add new instance for specific obj3d template when goal became active
// v3.0.219.12+ points to an element name and its sub element tag name <display_object_set name="<main tag name>" template="<sub tag name>". Holds 3D Objects to display in target location
inline const static std::string ELEMENT_DISPLAY_OBJECT_SET        = "display_object_set";
inline const static std::string ELEMENT_DISPLAY_OBJECT_NEAR_PLANE = "display_object_near_plane"; // v3.303.12 "flight_leg" element. Inform plugin to add new instance relative to plane position

inline const static std::string ATTRIB_REPLACE_LAT                              = "replace_lat";                  // v3.0.217.6
inline const static std::string ATTRIB_REPLACE_LONG                             = "replace_long";                 // v3.0.217.6
inline const static std::string ATTRIB_REPLACE_ELEV_FT                          = "replace_elev_ft";              //  v3.0.217.6
inline const static std::string ATTRIB_REPLACE_ELEV_ABOVE_GROUND_FT             = "replace_elev_above_ground_ft"; //  v3.0.217.6
inline const static std::string ATTRIB_REPLACE_DISTANCE_TO_DISPLAY_NM           = "replace_distance_to_display_nm"; //  v3.0.303.3
inline const static std::string ATTRIB_INSTANCE_NAME                            = "instance_name";                // unique name for 3D object template
inline const static std::string ATTRIB_TARGET_MARKER_B                          = "target_marker_b";              // v3.0.241.7 mark an instance as a target marker. Can be helful to show hide these specific markers in random MED missions
inline const static std::string ATTRIB_OBJ3D_TYPE                               = "obj_type";
inline const static std::string ATTRIB_IS_VIRTUAL_B                             = "is_virtual_b"; // v3.0.255.4 - if yes then treat the file name as a virtual library object to load and do not use the mission folder "obj" folder.
inline const static std::string ATTRIB_ALTERNATE_OBJ_FILE                       = "alternate_obj_file";
inline const static std::string ATTRIB_DISTANCE_TO_DISPLAY_NM                   = "distance_to_display_nm"; // v3.0.200
inline const static std::string ATTRIB_KEEP_UNTIL_GOAL                          = "keep_until_goal";        // v3.0.200 ***** converted to ATTRIB_KEEP_UNTIL_LEG *****
inline const static std::string ATTRIB_KEEP_UNTIL_LEG                           = "keep_until_leg";         // v3.0.221.15rc5 add LEG support
inline const static std::string ATTRIB_HIDE                                     = "hide";                   // v3.0.207.4 // boolean value that will flag a 3D Instance to hide
inline const static std::string PROP_DISPLAY_DEFAULT_OBJECT_FILE_OVER_ALTERNATE = "display_active_object";  // v3.0.200 // boolean. Which 3D Object to display the default or: "alternate_obj_file" attribute one.
inline const static std::string PROP_CAN_BE_DISPLAYED                           = "can_be_displayed";       // v3.0.200 // boolean. can an instance be drawn in X-Plane world ?
inline const static std::string ATTRIB_ELEV_ABOVE_GROUND_FT                     = "elev_above_ground_ft";   // v3.0.200 // boolean. can an instance be drawn in X-Plane world ?
inline const static std::string PREFIX_REPLACE_ = "replace_"; // v3.0.200 // all attributes we want to replace, should start with replace_xxx, example: replace_lat, replace_lon. The work after "replace_" must be the same as the original attribute,
                                                       // so we will be able to find it in the property.
// v3.0.200 // used with "display_object" element when we want to add task state to the "show/hide" conditions with "cond_script, keep_until.., display_". Value format: "objective.task"
inline const static std::string ATTRIB_LINK_TASK         = "link_task";
inline const static std::string PROP_LINK_OBJECTIVE_NAME = "link_objective_name"; // v3.0.200 // attrib_task is split to task and objects then this property holds the object name task is in.
inline const static std::string ATTRIB_SPEED_KMH         = "speed_kmh";           // km per hour
inline const static std::string ATTRIB_WAIT_SEC          = "wait_sec";


// Triggers
inline const static std::string ELEMENT_TRIGGERS       = "triggers";
inline const static std::string ELEMENT_TRIGGER        = "trigger";
inline const static std::string ELEMENT_SCRIPTS        = "scripts";
inline const static std::string ELEMENT_SET_DATAREFS         = "set_datarefs"; // v3.303.12 used in "on_enter" trigger
inline const static std::string ELEMENT_SET_DATAREFS_ON_EXIT = "set_datarefs_on_exit"; // v3.303.12 "on_exit" trigger
inline const static std::string ELEMENT_CONDITIONS     = "conditions";
inline const static std::string ATTRIB_PLANE_ON_GROUND = "plane_on_ground"; // bool. does trigger fires when plane is on ground ?
inline const static std::string ATTRIB_COND_SCRIPT     = "cond_script";     // script to evaluate trigger as part of the trigger firing conditions

inline const static std::string ATTRIB_POST_SCRIPT            = "post_script";
inline const static std::string ATTRIB_SCRIPT_NAME_WHEN_FIRED = "script_name_when_fired"; // v3.0.213.4 script_name_when_enter was renamed to script_name_when_fired
inline const static std::string ATTRIB_SCRIPT_NAME_WHEN_LEFT  = "script_name_when_left";  // when plane leaves trigger area
inline const static std::string ELEMENT_LOC_AND_ELEV_DATA     = "loc_and_elev_data";      //
inline const static std::string ELEMENT_POINT                 = "point";                  //
inline const static std::string ELEMENT_REFERENCE_POINT       = "reference_point";        // center of polygonal area or a reference point for triggers based script.
inline const static std::string ELEMENT_RADIUS                = "radius";                 //
 
inline const static std::string ELEMENT_RECTANGLE       = "rectangle";  // v3.0.253.5 used in poly triggers and auto rectangular trigger creation, just like the radius and radius length.
inline const static std::string ATTRIB_DIMENSIONS       = "dimensions"; // v3.0.253.5 used in poly triggers and auto rectangular trigger creation. This will determined the bearing to calculate the rectangular.
inline const static std::string DEFAULT_RECT_DIMENTIONS = "100|50";     // v3.0.253.5 used in poly triggers and auto rectangular trigger creation. This will determined the bearing to calculate the rectangular.
inline const static std::string ATTRIB_FIRST_POINT_IS_CENTER_B = "first_point_is_center"; // v3.0.301 B4 used with flight plan conversion screen, when creating centered box triggers
inline const static std::string ATTRIB_VECTOR_BT_LENGTH_MT = "vector_bt_length_mt"; // v3.0.301 B4 bottom to top length in meters (height)
inline const static std::string ATTRIB_VECTOR_LR_LENGTH_MT = "vector_lr_length_mt"; // v3.0.301 B4 left to right length in meters (width)
 
inline const static std::string ELEMENT_ELEVATION_VOLUME   = "elevation_volume";    //
inline const static std::string ATTRIB_ELEV_MIN_MAX_FT     = "elev_min_max_ft";     // volume of trigger [ --N|++N] or [min|max]
inline const static std::string ATTRIB_ELEV_LOWER_UPPER_FT = "elev_lower_upper_ft"; // volume of trigger [ --N|++N] or [lower|upper] in feet // v3.0.205.3
inline const static std::string ATTRIB_LENGTH_MT           = "length_mt";           // radius length in meter
inline const static std::string ATTRIB_ENABLED             = "enabled";             // trigger


// Trigger types
inline const static std::string TRIG_TYPE_RAD            = "rad";               // radius
inline const static std::string TRIG_TYPE_POLY           = "poly";              // polygon zone
inline const static std::string TRIG_TYPE_SLOPE          = "slope";             // slope
inline const static std::string TRIG_TYPE_SCRIPT         = "script";            //
inline const static std::string TRIG_TYPE_CAMERA         = "camera";            // v3.0.223.7 we can add camera_rad or camera_poly if designers will really really want this. Default is "rad"
inline const static std::string ATTRIB_ANGLE             = "angle";             // slope related
inline const static std::string ELEMENT_CALC_SLOPE       = "calc_slope";        // slope related // calculate 3 point slope from user data and one origin point
inline const static std::string ATTRIB_BEARING_2D        = "bearing_2d";        // slope related // calculate 3 point slope from user data and one origin point
inline const static std::string ATTRIB_LENGTH_WITH_UNITS = "length_with_units"; // slope related // calculate 3 point slope from user data and one origin point
inline const static std::string ATTRIB_SLOPE_ANGLE_3D    = "slope_angle_3d";    // slope related // calculate 3 point slope from user data and one origin point
inline const static std::string PROP_HAS_CALC_SLOPE      = "has_calc_slope";    // slope related // special boolean property to flag that we read <calc_slope> and it is valid

//inline const static std::string ATTRIB_LENGTH_NM         = "length_nm";      // length_nm, can be used anywhere
//inline const static std::string ATTRIB_TRIGGER_LENGTH_NM = ATTRIB_LENGTH_NM; // used in trigger


/// INVENTORIES // v3.0.213.1
inline const static std::string ELEMENT_CARGO_CATEGORIES = "cargo_categories"; // v24.05.1
inline const static std::string ELEMENT_INVENTORIES      = "inventories";
inline const static std::string ELEMENT_INVENTORY        = "inventory";
inline const static std::string ELEMENT_PLANE            = "plane";
inline const static std::string ELEMENT_ITEM_BLUEPRINTS  = "item_blueprints";
inline const static std::string ELEMENT_ITEM             = "item"; // inventory item
inline const static std::string ATTRIB_WEIGHT_KG         = "weight_kg";
inline const static std::string ATTRIB_QUANTITY          = "quantity";
inline const static std::string ATTRIB_BARCODE           = "barcode";

constexpr static float MINIMUM_EXPECTED_PILOT_WEIGHT_IN_STATION = 40.0f; // v24.12.2 used when evaluating if to add pilot weight to the plane.



// MESSAGES
inline const static std::string ELEMENT_MESSAGE_TEMPLATES      = "message_templates";
inline const static std::string ELEMENT_MESSAGES               = "messages";
inline const static std::string ELEMENT_MESSAGE                = "message";
inline const static std::string ATTRIB_MESSAGE_NAME_WHEN_FIRED = "message_name_when_fired"; // trigger message //
inline const static std::string ATTRIB_MESSAGE_NAME_WHEN_LEFT  = "message_name_when_left";  // trigger message
inline const static std::string ATTRIB_ORIGINATE_MESSAGE_NAME  = "originate_message_name";  // v3.0.223.7 will be set in SF once it is created. Will be used with background SF only (for now)
inline const static std::string ATTRIB_MESSAGE_NAME_WHEN_ENTERING_PHYSICAL_AREA = "message_name_when_entering_physical_area"; // v3.305.1b useful in triggers when we want to send a message even before "all conditions are met".

// trigger other outcomes
inline const static std::string ELEMENT_OUTCOME                      = "outcome"; // v3.0.221.11+ trigger commands
inline const static std::string ATTRIB_COMMANDS_TO_EXEC_WHEN_FIRED   = "commands_to_exec_when_fired"; // v3.0.221.11+ trigger commands
inline const static std::string ATTRIB_COMMANDS_TO_EXEC_WHEN_LEFT    = "commands_to_exec_when_left"; // v3.0.221.11+ trigger commands
inline const static std::string ATTRIB_DATAREF_TO_EXEC_WHEN_FIRED    = "dataref_to_modify_when_fired"; // v3.0.221.11+ trigger commands
inline const static std::string ATTRIB_DATAREF_TO_EXEC_WHEN_LEFT     = "dataref_to_modify_when_left"; // v3.0.221.11+ trigger commands
inline const static std::string ATTRIB_SET_OTHER_TASKS_AS_SUCCESS    = "set_other_tasks_as_success"; // v25.02.1
inline const static std::string ATTRIB_RESET_OTHER_TASKS_STATE       = "reset_other_tasks_state"; // v25.02.1
inline const static std::string ATTRIB_SET_OTHER_TRIGGERS_AS_SUCCESS = "set_other_triggers_as_success"; // v25.02.1


inline const static std::string ELEMENT_MIX                   = "mix"; // hold message channels information
inline const static std::string ATTRIB_MESSAGE                = ELEMENT_MESSAGE;
inline const static std::string ATTRIB_MESSAGE_TYPE           = "message_type";   // message and queues // comm, back(ground), pad message.
inline const static std::string PROP_IS_MXPAD_MESSAGE         = "is_mxpad";       // Internal flag. Define message as PAD message or not.
inline const static std::string PROP_MESSAGE_HAS_TEXT_TRACK   = "has_text_track"; // created during class init() and needs to be removed once we read "text" channel
inline const static std::string ATTRIB_MESSAGE_MIX_TRACK_TYPE = "track_type";     // message and queues // comm, back(ground), pad message.
inline const static std::string ATTRIB_PARENT_MESSAGE         = "parent_message"; // SoundFragment holds the message name it was defined in so we can stop specific communication channel or background sound.
// Message/seconds to play/display message. Will override the queMessage display timer rules. Can be use for sound file too but need implementation.
inline const static std::string ATTRIB_MESSAGE_OVERRIDE_SECONDS_TO_PLAY = "override_seconds_to_play";
// Message/seconds to play/display message. Will override the queMessage display timer rules. Can be use for sound file too but need implementation.
inline const static std::string ATTRIB_MESSAGE_OVERRIDE_SECONDS_TO_DISPLAY_TEXT = "override_seconds_to_display_text";
// used when plugin calculate per line /seconds to play/display message. Will override the queMessage display timer rules. Can be use for sound file too but need implementation.
inline const static std::string ATTRIB_MESSAGE_OVERRIDE_SECONDS_CALC_PER_LINE = "override_seconds_calc_per_line";
inline const static std::string ATTRIB_MESSAGE_MUTE_XPLANE_NARRATOR = "mute_xplane_narrator"; // if mute then we won't use "speak string"
inline const static std::string ATTRIB_MESSAGE_HIDE_TEXT            = "hide_text";            // Message/Desc used under
inline const static std::string ATTRIB_SOUND_FILE                   = "sound_file";           // Message/Desc used under
inline const static std::string ATTRIB_SOUND_VOL                    = "sound_vol";            // Message/Desc used under
inline const static std::string ATTRIB_ORIGINAL_SOUND_VOL           = "original_sound_vol";   // v3.306.1b used with <mix background> when we have repeat command we should consider using this attribute
inline const static std::string ATTRIB_TRACK_INSTRUCTIONS           = "track_instructions";   // v3.0.303.6 sound directive, special string command to tell the mixture how to configure the sound volume during playtime
inline const static std::string ATTRIB_TIMER_TYPE                   = "timer_type";   // v3.306.1 timr type, used with background mixture
inline const static std::string PROP_TIMER_TYPE_XP                   = "xp";   // v3.306.1 timer type "xp" default
inline const static std::string PROP_TIMER_TYPE_OS                   = "os";   // v3.306.1 timer type os.

inline const static std::string ATTRIB_CHARACTERS = "characters"; // v3.305.1 used in <mix>, format "code|label|hex color", example "j|Jeff|#B17DCB" 

inline const static std::string PROP_IMAGE_FILE_NAME          = "image_file_name";       // mxpad image file name
inline const static std::string PROP_TEXT_RGB_COLOR           = "text_rgb_color";        // mxpad text color. there should be translation
inline const static std::string ATTRIB_LABEL_PLACEMENT        = "label_placement";       // v3.0.223.4 L or R. Also replaces "mxpad_label_placement"
inline const static std::string ATTRIB_LABEL                  = "label";                 // text label // v3.0.223.4
inline const static std::string ATTRIB_LABEL_COLOR            = "label_color";           // label color // v3.0.223.4
inline const static std::string ELEMENT_MXPAD_ACTIVE_MESSAGES = "mxpad_active_messages"; // for savepoint, represent the current messages in MxPad window
inline const static std::string ELEMENT_MXPAD_ACTION_REQUEST  = "mxpad_action_request";  // for savepoint, represent the current messages in MxPad window
inline const static std::string ELEMENT_MXPAD_DATA            = "mxpad_data";            // for savepoint, represent the root MXPAD related elements
inline const static std::string PROP_CURRENT_MX_PAD_RUNNING   = "current_mxpad_running"; // savepoint. stores current running mxpad script name. We need to init after load

inline const static std::string PROP_NEXT_MSG                   = "next_msg";                 // v3.0.223.1 automatic find and send next message
inline const static std::string ATTRIB_MODE                     = "mode";                     // v3.305.1 Used with message, empty or story message mode.
inline const static std::string ATTRIB_FADE_BG_CHANNEL          = "fade_bg_channel";          // v3.305.3 seconds to auto mute and stop the background mix channel. Format: "message name where bg started,seconds to fade"
inline const static std::string ATTRIB_ADD_MINUTES              = "add_minutes";              // v3.0.223.1 add minutes as timelapse. We should configure
inline const static std::string ATTRIB_TIMELAPSE_TO_LOCAL_HOURS = "timelapse_to_local_hours"; // v3.0.223.1 timelapse to next local hour in format "24H:MI"
inline const static std::string ATTRIB_SET_DAY_HOURS            = "set_day_hours";            // v3.0.223.1 Immediate set time and day in format "day:24H:MI"
inline const static int         MAX_LAPS_I                      = 24;                         // v3.0.223.1
inline const static std::string MESSAGE_MODE_STORY              = "story";                    // v3.305.3



inline const static int MXPAD_LINE_WIDTH = 50;


inline const static std::string CHANNEL_TYPE_TEXT       = "text"; // comm / back / text
inline const static std::string CHANNEL_TYPE_COMM       = "comm"; // comm / back / text
inline const static std::string CHANNEL_TYPE_BACKGROUND = "back"; // comm / back / text
//const static std::string CHANNEL_TYPE_PAD        = "pad";  // comm / back / pad / text

// MESSAGES
inline const static std::string ELEMENT_END_MISSION         = "end_mission";
inline const static std::string ELEMENT_END_MISSION_SUCCESS = "end_mission_success";
inline const static std::string ELEMENT_END_MISSION_FAIL    = "end_mission_fail";
inline const static std::string ELEMENT_END_SUCCESS_IMAGE   = "success_image";
inline const static std::string ELEMENT_END_SUCCESS_MSG     = "success_msg";
inline const static std::string ELEMENT_END_SUCCESS_SOUND   = "success_sound";
inline const static std::string ELEMENT_END_FAIL_IMAGE      = "fail_image";
inline const static std::string ELEMENT_END_FAIL_MSG        = "fail_msg";
inline const static std::string ELEMENT_END_FAIL_SOUND      = "fail_sound";
inline const static std::string ATTRIB_OPEN_CHOICE          = "open_choice"; // v3.0.303.7

// MAP
inline const static std::string ATTRIB_FILE_NAME      = "file_name";
inline const static std::string ATTRIB_FILE_PATH      = "file_path";
inline const static std::string ATTRIB_FULL_FILE_PATH = "full_file_path"; // v3.0.217.2
inline const static std::string ATTRIB_REAL_W         = "realW";          // real width
inline const static std::string ATTRIB_REAL_H         = "realH";          // real height


// MXPAD
inline const static std::string ELEMENT_MXPAD                 = "mxpad";                    // mxpad
inline const static std::string ATTRIB_MANAGE_SCRIPT          = "manage_script";            // script that manages the mxpad progress
inline const static std::string ATTRIB_STARTING_FUNCTION      = "starting_function";        // When calling manage_script for first time, which function should be first to be evaluated.
inline const static std::string PROP_NEXT_RUNNING_FUNCTION    = "next_running_function";    // script that manages the mxpad progress
inline const static std::string PROP_CURRENT_RUNNING_FUNCTION = "current_running_function"; // script that manages the mxpad progress
inline const static std::string PROP_PREV_RUNNING_FUNCTION    = "prev_running_function";    // script that manages the mxpad progress


// PROPERTIES representing core attributes
inline const static std::string PROP_IS_LINKED             = "is_linked";               // boolean property to flag a trigger/task as linked. Add it during Objective validation and use it to store "checkpoint" and load/set mission.
inline const static std::string PROP_LINKED_TO             = "linked_to";               // boolean property to flag a trigger as linked to a task. checkpoint save/load
inline const static std::string PROP_All_COND_MET_B        = "all_conditions_met_b";    // boolean property to flag a trigger as success
inline const static std::string PROP_PLANE_IN_PHYSICAL_AREA= "plane_in_physical_area_b";// boolean property to flag plane in trigger area // v3.303.14
inline const static std::string PROP_PLANE_IN_ELEV_VOLUME  = "plane_in_elev_volume_b";  // boolean property to flag plane in trigger volume elevation // v3.303.14
inline const static std::string PROP_PLANE_ON_GROUND       = "plane_on_ground_b";       // boolean property to flag plane on ground v3.305.1c
inline const static std::string PROP_SCRIPT_COND_MET_B     = "script_conditions_met_b"; // boolean property to flag a condition script as success
inline const static std::string PROP_STATE_ENUM            = "enum_state";              // enum property describes state of Trigger or Flight Leg
inline const static std::string PROP_TRIG_ELEV_TYPE        = "trig_elev_type";          // enum property to describe trigger elevation volume
inline const static std::string PROP_IS_VALID              = "is_valid";                // boolean property to flag a goal/objective state
inline const static std::string PROP_HAS_MANDATORY         = "has_mandatory";           // boolean property to flag a goal/objective with Mandatory Objective
inline const static std::string PROP_IS_COMPLETE           = "is_complete";             // boolean property to flag a task is completed or not (achieved or not)
inline const static std::string PROP_IS_FIRST_TIME         = "is_first_time";         // v3.0.241.1 boolean property to flag a flight leg as first time or not. Relevant after loading checkpoint, to not execute all first time messages and settings.
inline const static std::string PROP_COMPLETE_DESC         = "complete_desc";         // boolean property to flag a task is completed successfully or not.
inline const static std::string PROP_ERROR_REASON          = "error_reason";          // holds task error explanation
inline const static std::string PROP_COUNTER               = "counter";               // message counter
inline const static std::string PROP_MESSAGE_BROADCAST_FOR = "broadcast_for";         // message, to whom was broadcast
inline const static std::string PROP_MESSAGE_TRACK_NAME    = "track_name";            // message track name for Queue Message Manager
inline const static std::string PROP_POINTS                = "points";                //
inline const static std::string PROP_HAS_ALWAYS_TASK       = "has_always_task";       // objective attribute
inline const static std::string PROP_MISSION_FILE_LOCATION = "mission_file_location"; // v3.0.255.3

// Datarefs
inline const static std::string ATTRIB_DREF_KEY = "key";

// Mission properties
inline const static std::string PROP_MISSION_CURRENT_LEG  = "mission_current_leg";  // savepoint // v3.0.221.15rc5 renamed from PROP_MISSION_CURRENT_GOAL
inline const static std::string PROP_MISSION_STATE        = "mission_state";        // MISSION // savepoint
inline const static std::string PROP_MISSION_ABORT_REASON = "mission_abort_reason"; // MISSION // savepoint
// Mission Savepoint plane location
inline const static std::string SAVEPOINT_PLANE_LATITUDE  = "savepoint_latitude";  // MISSION // savepoint
inline const static std::string SAVEPOINT_PLANE_LONGITUDE = "savepoint_longitude"; // MISSION // savepoint
inline const static std::string SAVEPOINT_PLANE_ELEVATION = "savepoint_elevation"; // MISSION // savepoint
inline const static std::string SAVEPOINT_PLANE_SPEED     = "savepoint_speed";     // MISSION // savepoint // groundspeed
inline const static std::string SAVEPOINT_PLANE_HEADING   = "savepoint_heading";   // MISSION // savepoint // psi


// embedded script - seeded attributes
inline const static std::string MX_                       = "mx_";
inline const static std::string EXT_MX_FUNC_CALL          = "mxFuncCall";
inline const static std::string EXT_MX_CURRENT_LEG        = "mxCurrentLeg"; // v3.0.221.15rc5 replaces mxCurrentGoal
inline const static std::string EXT_MX_CURRENT_OBJ        = "mxCurrentObjective";
inline const static std::string EXT_MX_CURRENT_TASK       = "mxCurrentTask";
inline const static std::string EXT_MX_CURRENT_TRIGGER    = "mxCurrentTrigger";
inline const static std::string EXT_MX_CURRENT_3DOBJECT   = "mxCurrent3dObject";   // v3.0.200
inline const static std::string EXT_MX_CURRENT_3DINSTANCE = "mxCurrent3dInstance"; // v3.0.200
inline const static std::string EXT_MX_QM_MESSAGE         = "mxQmMessage";         // v3.0.223.1 holds the message name
// Task get info
inline const static std::string EXT_mxState                = "state";                // string "success/was_success/need_evaluation"
inline const static std::string EXT_mxType                 = ATTRIB_TYPE;            // "mxType"; // string trigger/script/undefined
inline const static std::string EXT_mxTaskActionName       = "taskActionName";       // string // action_code_name
inline const static std::string EXT_mxTaskHasBeenEvaluated = "taskHasBeenEvaluated"; // bool

// Navigation seed attributes
inline const static std::string EXT_mxNavType   = "mxNavType";
inline const static std::string EXT_mxNavLat    = "mxNavLat";
inline const static std::string EXT_mxNavLon    = "mxNavLon";
inline const static std::string EXT_mxNavHeight = "mxNavHeight";
inline const static std::string EXT_mxNavFreq   = "mxNavFreq";
inline const static std::string EXT_mxNavHead   = "mxNavHeading";
inline const static std::string EXT_mxNavID     = "mxNavID";
inline const static std::string EXT_mxNavName   = "mxNavName";
inline const static std::string EXT_mxNavRegion = "mxNavRegion";

// Cargo Information for SlingLoad scripts // v3.0.303 Sling Load
inline const static std::string EXT_mxCargoPosLat = "mxCargoPosLat";
inline const static std::string EXT_mxCargoPosLon = "mxCargoPosLon";



// Queue Message Manager - QMM
inline const static std::string QMM_DUMMY_MSG_NAME_PREFIX = "msg_"; // used to construct new messages from code

// calculate weight related
inline const static float DEFAULT_PILOT_WEIGHT  = 85.0f; // v3.0.213.3  85kg
inline const static float DEFAULT_STORED_WEIGHT = 5.0f;  // v3.0.213.3  5kg

// Base weights by designer
inline const static std::string ELEMENT_BASE_WEIGHTS_KG    = "base_weights_kg";     // options // mute any sound, include narrator and fmod sounds
inline const static std::string ATTRIB_PILOT               = "pilot";               // v3.0.213.4 pilot weight in kg
inline const static std::string ATTRIB_PASSENGERS          = "passengers";          // v3.0.213.4 passengers weight in kg
inline const static std::string ATTRIB_STORAGE             = "storage";             // v3.0.213.4 storage weight in kg
inline const static std::string ELEMENT_POSITION           = "position";            // v3.0.223.1 Hidden parameter for now. Will skip plane repositioning at random start.
inline const static std::string ATTRIB_AUTO_POSITION_PLANE = "auto_position_plane"; // v3.0.223.1 Hidden attribute. Boolean type. true=position plane (default), false=skip position plane.



// OPTIONS
inline const static std::string OPT_MUTE_MX_SOUNDS                           = "mute_missionx_sound";                     // options // mute any sound, include narrator and fmod sounds
inline const static std::string OPT_ABORT_MISSION_ON_CRASH                   = "abort_mission_on_crash";                  // options // bool // abort mission on crash
inline const static std::string OPT_ENABLE_DESIGNER_MODE                     = "enable_designer_mode";                    // options // bool //
inline const static std::string OPT_DISPLAY_VISUAL_CUES                      = "enable_visual_cues";                      // options // bool //
inline const static std::string OPT_AUTO_HIDE_SHOW_MXPAD                     = "auto_hide_mxpad";                         // v3.0.215.1 auto hide mxpad options
inline const static std::string OPT_AUTO_PAUSE_IN_2D                         = "auto_pause_in_2D";                        // v3.0.253.9.1
inline const static std::string OPT_AUTO_PAUSE_IN_VR                         = "auto_pause_in_vr";                        // v3.0.221.6
inline const static std::string OPT_CYCLE_LOG_FILES                          = "cycle_log_files";                         // v25.03.1
inline const static std::string OPT_DISPLAY_MISSIONX_IN_VR                   = "display_missionx_in_vr";                  // v3.0.221.7
inline const static std::string OPT_WRITE_CONVERTED_FPLN_TO_XPLANE_FOLDER    = "write_convert_fms_to_xplane_folder";      // v3.0.241.2
inline const static std::string OPT_DISABLE_PLUGIN_COLD_AND_DARK_WORKAROUND  = "disable_plugin_cold_and_dark_workaround"; // v3.0.221.10
inline const static std::string OPT_OVERPASS_FILTER                          = "overpass_filter";                         // v3.0.253.4
inline const static std::string OPT_OVERPASS_URL                             = "overpass_url";                            // v3.0.253.6 store overpass url
inline const static std::string OPT_GPS_IMMEDIATE_EXPOSURE                   = "gps_immediate_exposure";                  // v3.0.253.7 Display all points of global GPS in FMS or one by one
inline const static std::string OPT_FLIGHT_LEG_PROGRESS_COUNTER              = "flight_leg_progress_counter";             // v3.0.253.7 flight_leg_progress_counter_i
inline const static bool        DEFAULT_AUTO_PAUSE_IN_VR                     = false;                                     // v3.0.255.4.2
inline const static bool        DEFAULT_AUTO_PAUSE_IN_2D                     = true;                                      // v3.0.255.4.2
inline const static bool        DEFAULT_DISPLAY_MISSIONX_IN_VR               = true;                                      // v3.0.255.4.2
inline const static bool        DEFAULT_WRITE_CONVERTED_FMS_TO_XPLANE_FOLDER = false;                                     // v3.0.255.4.2
inline const static bool        DEFAULT_DISABLE_PLUGIN_COLD_AND_DARK         = false;                                     // v3.0.255.4.2


// the following optional attributes are read from General_Properties of the mission, they are not stored in option file.
// In future build, we might add base weights options to the "Option" screen.
inline const static std::string OPT_PILOT_BASE_WEIGHT      = "pilot_base_weight";      // v3.0.213.3 assist in calculating weight of plane = empty plane + pilot + passengers + lauggage
inline const static std::string OPT_STORAGE_BASE_WEIGHT    = "storage_base_weight";    // v3.0.213.3 other payload
inline const static std::string OPT_PASSENGERS_BASE_WEIGHT = "passengers_base_weight"; // v3.0.213.3 passengers
inline const static std::string OPT_PILOT                  = "pilot";      // v24.03.2 Adding deprecated attribute
inline const static std::string OPT_STORAGE                = "storage";    // v24.03.2 Adding deprecated attribute
inline const static std::string OPT_PASSENGERS             = "passengers"; // v24.03.2 Adding deprecated attribute

// TTF FONTS
//inline const static std::string FONT_TTF = "Adamina-Regular.ttf"; // "Ubuntu-R.ttf"; // true type fonts


// PLUGIN MESSAGES
inline const static std::string MESSAGE_NEED_TO_RESTART_XPLANE = "Please restart X-Plane so changes will take effect!";

//// COMMANDS related elements // v3.0.221.9
inline const static std::string ELEMENT_XP_COMMANDS                 = "xp_commands";
inline const static std::string ATTRIB_COMMANDS                     = "commands"; // use as an attribute at goal level. example: <fire_commands_at_goal_begin commands="bell/412/push,bell/412/" /> we should execute each command at its own flb
inline const static std::string ELEMENT_FIRE_COMMANDS_AT_LEG_START  = "fire_commands_at_leg_start";
inline const static std::string ELEMENT_FIRE_COMMANDS_AT_GOAL_START = "fire_commands_at_goal_start";
inline const static std::string ELEMENT_FIRE_COMMANDS_AT_LEG_END    = "fire_commands_at_leg_end";
inline const static std::string ELEMENT_FIRE_COMMANDS_AT_GOAL_END   = "fire_commands_at_goal_end";

/// TBD not yet implemented
inline const static std::string ATTRIB_FIRE_COMMANDS_ON_SUCCESS  = "fire_commands_on_success";  // can be used with triggers
inline const static std::string ATTRIB_FIRE_COMMANDS_ON_FAILURE  = "fire_commands_on_failure";  // can be used with triggers
inline const static std::string ATTRIB_FIRE_COMMANDS_AT_TASK_END = "fire_commands_at_task_end"; // TODO: not sure if we want this
/// END TBD not yet implemented

inline const static std::string ATTRIB_START_COLD_AND_DARK           = "start_cold_and_dark";          // v3.0.221.10 briefers additional_location attribute
inline const static std::string ELEMENT_DATAREFS_START_COLD_AND_DARK = "datarefs_start_cold_and_dark"; //  v3.0.221.10 will be stored at briefer level

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
inline const static std::string MX_TRUE  = "true";  // used when reading XML attributes
inline const static std::string MX_FALSE = "false"; // used when reading XML attributes
inline const static std::string MX_YES   = "yes";   // used when reading XML attributes
inline const static std::string MX_NO    = "no";    // used when reading XML attributes

//////////////////////////////  BITMAP BITMAP BITMAP  //////////////////////////////
// original textures
inline const static std::string BITMAP_INVENTORY_MXPAD              = "inventory_mxpad.png";
inline const static std::string BITMAP_MAP_MXPAD                    = "map_mxpad.png";
inline const static std::string BITMAP_AUTO_HIDE_EYE_FOCUS          = "eye_focus_16x16.png";          // used in MX-Pad to show if auto hide is active.
inline const static std::string BITMAP_AUTO_HIDE_EYE_FOCUS_DISABLED = "eye_focus_disabled_16x16.png"; // used in MX-Pad to show if auto hide is active.

// new textures
inline const static std::string BITMAP_LOAD_MISSION      = "load_missions.png";                       // used in welcome layer. // v24025 renamed file

// IMGUI Related
inline const static std::string BITMAP_HOME                      = "btn_home_64x64.png";              // v3.0.251.1 home like icon in blue
inline const static std::string BITMAP_TARGET_MARKER_ICON        = "btn_target_icon_64x64.png";       // v3.0.253.9.1 icon for target marker show/hide
inline const static std::string BITMAP_BTN_LAB_24X18             = "btn_lab_240x180.png";             // v3.0.251.1
inline const static std::string BITMAP_BTN_WORLD_PATH_24X18      = "btn_world_paths_240x180.png";     // v3.0.251.1
inline const static std::string BITMAP_BTN_PREPARE_MISSION_24X18 = "btn_prepare_mission_240x180.png"; // v3.0.251.1
inline const static std::string BITMAP_BTN_FLY_MISSION_24X18     = "btn_fly_mission_240x180.png";     // v3.0.251.1
inline const static std::string BITMAP_BTN_SETUP_24X18           = "btn_setup_240x180.png";           // v3.0.251.1
inline const static std::string BITMAP_BTN_CONVERT_FPLN_TO_MISSION_24X18   = "btn_fpln_convert_to_mission_240x180.png";           // v3.0.301

inline const static std::string BITMAP_BTN_TOOLBAR_SETUP_64x64 = "btn_setup_icon_64x64.png"; // v3.0.251.1
inline const static std::string BITMAP_BTN_QUIT_48x48  = "btn_quit_48x48.png";       // v3.0.251.1
inline const static std::string BITMAP_BTN_SAVE_48x48  = "btn_save_48x48.png";       // v3.0.251.1
 
inline const static std::string BITMAP_BTN_ABOUT_64x64 = "btn_about_64x64.png"; // v3.0.253.2
inline const static std::string BITMAP_BTN_NAV_24x18   = "btn_nav_24x18.png";   // v3.0.253.6 // v24025 renamed

inline const static std::string BITMAP_FMOD_LOGO = "fmod_logo.jpg"; // v3.0.253.6 fmod logo

inline const static std::string BITMAP_BTN_WARN_SMALL_32x28 = "btn_warn_small_32x28.png"; // v3.0.255.3 warning sign
inline const static std::string BITMAP_BTN_NAVINFO          = "btn_navinfo.png";          // v24025



inline const static std::string WHITE         = "white";
inline const static std::string RED           = "red";
inline const static std::string YELLOW        = "yellow";
inline const static std::string GREEN         = "green";
inline const static std::string ORANGE        = "orange";
inline const static std::string PURPLE        = "purple";
inline const static std::string BLUE          = "blue";
inline const static std::string BLACK         = "black";


inline const static float MANDATORY_R = 0.99f;
inline const static float MANDATORY_G = 0.001f;
inline const static float MANDATORY_B = 0.001f;
 
inline const static float DEFAULT_R = 0.9f;
inline const static float DEFAULT_G = 0.65f;
inline const static float DEFAULT_B = 0.0f;

inline const static int FONT_HEIGHT = 8; // 14; // x-plane default font height
// ####### MXPAD SELECTION ####
inline const static int  MAX_MXPAD_SELECTIONS      = 6;
inline const static std::string MXPAD_SELECTION_DELIMITER = "#SEL#";

// MOVING OBJECT Constants
inline const static float DEFUALT_3D_OBJECT_SPEED_KMH = 10.0f;

// RANDOM ENGINE: Generates Mission elements and attributes
inline const static std::string ATTRIB_USE_TRIGGER_NAME_FROM_TEMPLATE = "use_trigger_from_template"; // v3.0.221.10 will search for element <trigger name="" > in the template and will use it instead of the default map trigger template. This will allow for special triggers that are not just based on radius but
                                                                                                     // maybe also need script This is an extension to the original trigger implementation.
inline const static std::string ATTRIB_ADD_TASKS_FROM_TEMPLATE    = "add_tasks_from_template";    // v3.0.221.10 will search for element <tasks name="" > and will append all tasks in it to the current objective.
inline const static std::string ATTRIB_ADD_MESSAGES_FROM_TEMPLATE = "add_messages_from_template"; // v3.0.221.10 will search for element with tag <{add_messages_from_template}> and will append all messages in it to xMessages
inline const static std::string ATTRIB_ADD_TRIGGERS_FROM_TEMPLATE = "add_triggers_from_template"; // v3.0.221.10 will search for element with tag <{add_triggers_from_template}> and will append all triggers elements to xTriggers
inline const static std::string ATTRIB_ADD_SCRIPTS_FROM_TEMPLATE  = "add_scripts_from_template";  // v3.0.221.10 will search for element with tag <{add_scripts_from_template}> and will append all tasks in it to xScripts.

inline const static std::string ATTRIB_LOCATION_TYPE    = "location_type";
inline const static std::string ATTRIB_LOCATION_VALUE   = "location_value";
inline const static std::string ATTRIB_EXPECTED_LAND_ON = "expected_land_on";

inline const static std::string ELEMENT_TEMPLATE                   = "TEMPLATE";
inline const static std::string ATTRIB_TEMPLATE                    = "template";
inline const static std::string ELEMENT_BRIEFER_AND_START_LOCATION = "briefer_and_start_location";
inline const static std::string ELEMENT_EXPECTED_LOCATION          = "expected_location";

inline const static std::string ATTRIB_PLANE_TYPE     = "plane_type"; // v3.0219.6 used in template so we know which type of plane: [heli|ga|big] [helos|jets|turboprops|props|heavy]
inline const static std::string PLANE_TYPE_HELOS      = "helos";      // v3.0219.9 Helicopter
inline const static std::string PLANE_TYPE_JETS       = "jets";       // v3.0219.9 Jet planes
inline const static std::string PLANE_TYPE_PROPS      = "props";      // v3.0219.9 Propellor
inline const static std::string PLANE_TYPE_TURBOPROPS = "turboprops"; // v3.0219.9 Turbo Propeller, general heavier and larger types
inline const static std::string PLANE_TYPE_HEAVY      = "heavy";      // v3.0219.9 The big daddies :-)

inline const static std::string ELEMENT_CONTENT = "content"; // v3.0.219.14 Used in random templates only
inline const static std::string ATTRIB_LIST     = "list";    // v3.0.219.14 Used in random templates only


inline const static std::string LOCATION_TYPE_SAME_AS      = "same_as"; // v3.0.219.1 "generate template" used in "goal template type" (implemented)
inline const static std::string FL_TEMPLATE_VAL_START      = "start"; // v3.0.219.3 "generate template" used in "goal template type" (implemented)
inline const static std::string FL_TEMPLATE_VAL_LAND       = "land"; // v3.0.219.1 "generate template" used in "goal template type" (implemented)
inline const static std::string FL_TEMPLATE_VAL_HOVER      = "hover"; // v3.0.219.3 "generate template" used in "goal template type" ()
inline const static std::string FL_TEMPLATE_VAL_LAND_HOVER = "land_hover"; // v25.02.1 "generate template" - should be the default for "medevac" and "helos"
inline const static std::string FL_TEMPLATE_VAL_DELIVER    = "deliver"; // v3.0.219.6 "generate template" used in "goal template type" ()

inline const static std::string EXPECTED_LOCATION_TYPE_ICAO   = "icao";   // v3.0.221.4
inline const static std::string EXPECTED_LOCATION_TYPE_XY     = "xy";     // xy, icao,goal,start  (xy can have string based random or number random that represents random lat/long
inline const static std::string EXPECTED_LOCATION_TYPE_OILRIG = "oilrig"; // v3.303.14 oilrig = lat/long should be embedded in the <leg> element, no search is needed.
inline const static std::string EXPECTED_LOCATION_TYPE_OSM    = "osm";    // v3.0.241.10 b2 osm
inline const static std::string EXPECTED_LOCATION_TYPE_WEBOSM = "webosm"; // v3.0.253.4 webosm used with custom templates where a designer asks from plugin to fetch osm data from web (overpass in this implementation).
inline const static std::string EXPECTED_LOCATION_TYPE_NEAR   = "near";   // "near" usually with land type, searches for nearest ICAO relative to last goal position

inline const static std::string ELEMENT_ICAO          = "icao";                // v3.0.221.5 used in RandomEngine when we try convert any icao tag to point
inline const static std::string ATTRIB_ICAO_ID        = "icao_id";             // v3.303.8.3
inline const static std::string ATTRIB_AP_NAME        = "ap_name";             // v3.303.14 airport name, used when creating OIL RIG mission
inline const static std::string ATTRIB_NAVREF         = "navref";              // v3.0.231.1 FMS data
inline const static std::string ATTRIB_NAV_TYPE       = "nav_type";            // v3.0.255.4
inline const static std::string ATTRIB_DISP_FMS_ENTRY = "displayed_fms_entry"; // v3.0.231.1 FMS data

inline const static std::string GENERATE_TYPE_MEDEVAC     = "medevac";
inline const static std::string GENERATE_TYPE_DELIVERY    = "delivery";   // v3.0.219.6
inline const static std::string GENERATE_TYPE_CARGO       = "cargo";   // v3.303.14
inline const static std::string GENERATE_TYPE_OILRIG_MED   = "oilrig_med"; // v3.303.14
inline const static std::string GENERATE_TYPE_OILRIG_CARGO = "oilrig_cargo"; // v3.303.14
inline const static std::string ELEMENT_CARGO              = "cargo";        // v24.05.1 used when reading external cargo xml data file

inline const static std::string DEFAULT_RANDOM_IMAGE_FILE = "random.png"; // v3.0.217.6

inline const static std::string TERRAIN_TYPE_MEDEVAC_SLOPE = "medevac_slope"; // v3.0.219.12+ will be used when using <display_object_set> if the template name is "medevac" and slope is larger than plugin define max terrain slope then replace with "medevac_slope" object sets.

inline const static std::string ATTRIB_RADIUS_MT                            = "radius_mt";                            // v3.0.217.6 used in point template
inline const static std::string ATTRIB_LOC_DESC                             = "loc_desc";                             // v3.0.217.6 used in point template
inline const static std::string ATTRIB_RANDOM_TAG                           = "random_tag";                           // v3.0.219.1 the name of the element tag to search for random data. Can be random 3D Object or <point> etc.
inline const static std::string ATTRIB_SET_NAME                             = "set_name";                             // v3.0.219.14 the name of a sub element that we need to random pick from.
inline const static std::string ATTRIB_RANDOM_WATER_TAG                     = "random_water_tag";                     // v3.0.219.10 the name of the element tag to search for random water 3D Objects
inline const static std::string ATTRIB_RELATIVE_POS_BEARING_DEG_DISTANCE_MT = "relative_pos_bearing_deg_distance_mt"; // v3.0.219.1 position 3D Object providing: "bearing|distance"
inline const static std::string ATTRIB_DEBUG_RELATIVE_POS                   = "debug_relative_pos"; // v3.303.14 used with RandomEngine. After calculating ATTRIB_RELATIVE_POS_BEARING_DEG_DISTANCE_MT we need to reset its value so it won't interfer with positioning of <display_object>. The "ATTRIB_RELATIVE_POS_BEARING_DEG_DISTANCE_MT" has two uses, calculate at the start of the mission or calculate relative to target/plane during Random mission creation.
inline const static std::string ATTRIB_EXCLUDE_OBJ                          = "exclude_obj";                          // v3.0.219.5 used in <point> hint Engine which objects to remove from generated mission if exists.
inline const static std::string ATTRIB_INCLUDE_OBJ                          = "include_obj";                          // v3.0.219.5 used in <point> hint Engine which objects to include in generated mission. Include can only pick one: "hiker,man,woman" will pick one and add it. overlaping location restriction.
inline const static std::string PROP_IS_WET                   = "is_wet";                 // v3.0.219.10 holds point data if water or terrain (land)
inline const static std::string PROP_FOUND_ICAO               = "found_icao";             // v3.0.219.12+ flags NavAidInfo as found in optimized aptData information
inline const static std::string ATTRIB_LIMIT_TO_TERRAIN_SLOPE = "limit_to_terrain_slope"; // v3.0.219.12+ used in <display_object> element where attribute "limit_to_terrain_slope" will assist if to display the object on certain terrain
inline const static std::string ATTRIB_IS_SET_OF_FLIGHT_LEGS   = "is_set_of_flight_legs";  // v3.0.221.7 renamed to is_set_of_goals //v3.0.219.12+ used in <display_object> element where attribute "limit_to_terrain_slope" will assist if to display the object on certain terrain
inline const static std::string ATTRIB_COPY_LEG_AS_IS_B        = "copy_leg_as_is_b";     // v3.0.303 used in combination of <content> element. when picking a set of legs we can flag them as final without the need to generate them from scratch, meaning we have all we need
inline const static std::string ATTRIB_IS_RANDOM_COORDINATES   = "is_random_coordinates";     // v3.0.219.12+ used in <display_object> element where attribute "limit_to_terrain_slope" will assist if to display the object on certain terrain
inline const static std::string ATTRIB_POI_TAG                 = "poi_tag";                   // v3.0.219.14+ point of interest tag a <point> attribute. We will add to the GPS not as a target but as a point you can navigate too.
inline const static std::string ATTRIB_BASE_ON_EXTERNAL_PLUGIN = "base_on_external_plugin";   // v3.0.221.9 values: "true" or "false", flag that tells plugin to use external dataref to flag success of task. You still need to define a trigger since
                                                                                       // we are not sure that there is any plugin listening and working with these datarefs
inline const static std::string ATTRIB_HOVER_TIME_SEC_RANDOM       = "hover_time_sec_random"; // v3.0.241.8 former: ATTRIB_CUSTOM_HOVER_TIME // v3.0.221.11 random hover time in seconds. Can be in format "10" or "10-40"
inline const static std::string ATTRIB_DEFAULT_RANDOM_HOVER_TIME   = "10-30";                 // v3.0.241.8 default hover time
inline const static std::string ATTRIB_CUSTOM_FLIGHT_LEG_DESC_FLAG = "custom_flight_leg_desc_flag"; // v3.0.221.15rc5 // v3.0.221.11 does goal has custom description
inline const static std::string ATTRIB_FORCE_TEMPLATE_DISTANCES_B     = "force_template_distances_b";     // v3.0.241.8 If the user picks expected distance in the setup screen, the designer can still decide if to allow this or not per <expected_location> element, not globally.
inline const static std::string ATTRIB_IS_SKEWED_POSITION_B = "is_skewed_position_b"; // v3.0.241.8 flag a point element as skewed position. Can be used in Random message handling when picking a point and it has this attribute.
inline const static std::string ATTRIB_IS_BRIEFER_OR_START_LOCATION_B = "is_briefer_or_start_location_b"; // v3.0.303.10
inline const static std::string ATTRIB_SKEWED_NAME          = "skewed_name";          // v3.0.241.8 For templates markers. Replace <display_object> name with the "skewed_name"
inline const static std::string ATTRIB_REAL_POSITION        = "real_position";        // v3.0.241.8
inline const static std::string ATTRIB_IS_TARGET_POINT_B    = "is_target_point_b";    // v3.0.241.8 a flag on the <point> that flag it as a target point. It can still be skewed value.
inline const static double      MAX_AWAY_SKEWED_DISTANCE_NM = 0.5;                    // v3.0.253.12 added for general use in all classes and not just RandomMission:get_skewed_target_position() function


inline const static std::string ATTRIB_PICK_LOCATION_BASED_ON_SAME_TEMPLATE_B = "pick_location_based_on_same_template_b"; // v3.0.221.15rc5 renamed from pick_location_based_on_same_goal_template_b //  v3.0.221.15 rc3 an attribute that holds flag if random picked <point> should be same as the flight leg template type.
inline const static std::string ATTRIB_MIN_VALID_FLIGHT_LEGS     = "min_valid_flight_legs";     // v3.0.221.15 rc3.2 minimum number of goals so mission would be valid.
inline const static std::string ATTRIB_SKIP_AUTO_TASK_CREATION_B = "skip_auto_task_creation_b"; // v3.0.221.15 rc4 skip auto task creation when building a goal. Designer probably will inject his/her tasks in goal_special_directives
inline const static std::string ATTRIB_FORCE_SLOPED_TERRAIN                   = "force_sloped_terrain";      // v3.0.253.6 // <expected_location> Only if leg template=hover, force slope so if getTarget() will pick location with shalower slope then we will fail it and ask for another target.
inline const static std::string ATTRIB_FORCE_LEVELED_TERRAIN = "force_leveled_terrain";   // v3.0.253.9.1 // <expected_location> henry @Daikan asked for this. plugin will pick only "land" area and fail if won't find. Same as
                                                                                    // ATTRIB_FORCE_SLOPED_TERRAIN only for none slopped terrain. Provide values 0..10
inline const static std::string ATTRIB_FORCE_TYPE_OF_TEMPLATE = "force_type_of_template"; // v3.0.253.9.1 // <expected_location> henry @Daikan asked for this. plugin will pick only "land" area and fail if won't find. Same as
                                                                                    // ATTRIB_FORCE_SLOPED_TERRAIN only for none slopped terrain. Provide values 0..10
inline const static std::string ATTRIB_DESIGNER_MAX_SLOPE_TO_LAND           = "max_slope_to_land";          // v3.0.253.9.1 // <expected_location> henry @Daikan asked for this. Designer prefered max_slope_to_land value. default is mxconst::MAX_SLOPE_TO_LAND and should be stored in data_manager::MAX_SLOPE_TO_LAND.
inline const static std::string PROP_NUMBER_OF_LOOPS_TO_FORCE_TYPE_TEMPLATE = "no_of_loops_to_force_slope"; // v3.0.253.6 // internal, used in
inline const static std::string PROP_IS_LAST_FLIGHT_LEG                     = "is_last_flight_leg";         // v3.0.223.1 used during random mission generation
 
inline const static std::string APT_1_LAND_AIRPORT_HEADER_CODE_v11_SPACE   = "1 ";    // v3.0.219.12+ apt dat codes with space. add trim if you don't want space
inline const static std::string APT_16_SEAPLANE_BASE_HEADER_CODE_v11_SPACE = "16 ";   // v3.0.219.12+ apt dat codes with space. add trim if you don't want space
inline const static std::string APT_17_HELIPORT_HEADER_CODE_v11_SPACE      = "17 ";   // v3.0.219.12+ apt dat codes with space. add trim if you don't want space
inline const static std::string APT_1300_RAMP_CODE_v11_SPACE               = "1300 "; // v3.0.219.12+ apt dat codes with space. add trim if you don't want space
inline const static std::string APT_100_LAND_RW_CODE_v11_SPACE             = "100 ";  // v3.0.219.12+ apt dat codes with space. add trim if you don't want space
inline const static std::string APT_101_WATER_RW_CODE_v11_SPACE            = "101 ";  // v3.0.219.12+ apt dat codes with space. add trim if you don't want space
inline const static std::string APT_102_HELIPAD_RW_CODE_v11_SPACE          = "102 ";  // v3.0.219.12+ apt dat codes with space. add trim if you don't want space

inline const static std::string APT_1_LAND_AIRPORT_HEADER_CODE_v11   = "1";    // v3.0.253.7 apt dat codes with space.
inline const static std::string APT_16_SEAPLANE_BASE_HEADER_CODE_v11 = "16";   // v3.0.253.7 apt dat codes with space.
inline const static std::string APT_17_HELIPORT_HEADER_CODE_v11      = "17";   // v3.0.253.7 apt dat codes with space.
inline const static std::string APT_1300_RAMP_CODE_v11               = "1300"; // v3.0.253.7 apt dat codes with space.
inline const static std::string APT_100_LAND_RW_CODE_v11             = "100";  // v3.0.253.7 apt dat codes with space.
inline const static std::string APT_101_WATER_RW_CODE_v11            = "101";  // v3.0.253.7 apt dat codes with space.
inline const static std::string APT_102_HELIPAD_RW_CODE_v11          = "102";  // v3.0.253.7 apt dat codes with space.

inline const static std::string ATTRIB_DISTANCE_NM                = "distance_nm";                // v3.0.219.11 hold distance from last goal to current
inline const static std::string ATTRIB_SHARED_TEMPLATE_TYPE       = "shared_template_type";       // v3.0.221.7 used with shared type datarefs
inline const static std::string ATTRIB_SHARED_FLIGHT_LEG_TEMPLATE = "shared_flight_leg_template"; // v3.0.221.15rc5 support leg/ Replaces the name shared_goal_template  // v3.0.221.7 used with shared type datarefs
inline const static std::string ATTRIB_SHARED_GOAL_TEMPLATE       = "shared_goal_template";       // v3.0.221.7 used with shared type datarefs


// helpers attribute for random creation
inline const static std::string ATTRIB_TARGET_POS = "target_pos";        // v3.0.221.9 target position - format: "{lat}|{lon}|{elev}. Internal use. holds leg main target location. Useful to share lat/lon with shared datarefs. Also we should use it when
                                                           // we determind not random mission, it should be the first task in a goal that is mandatory depends on trigger with location values.
inline const static std::string ATTRIB_TASK_TRIGGER_NAME = "task_trigger_name"; // v3.0.219.2 store the name of the trigger the goals task is dependent upon.
inline const static std::string ATTRIB_HOVER_TIME        = "hover_time";        // v3.0.219.3 store hover time, which is also task success evaluation time
inline const static std::string ATTRIB_DEPENDS_ON        = "depends_on";        // v3.0.219.11 depends on other goal. Used in random mission generator template.
inline const static std::string ATTRIB_TERRAIN_SLOPE     = "terrain_slope";     // v3.0.221.3 holds terrain slope from calculate_slope_for_build_flight_leg_thread ( mission.flcPRE() )

inline const static int         DEFAULT_HOVER_HEIGHT_FT                                = (int)(100 * missionx::meter2feet);
inline const static std::string DEFAULT_TEMPLATE_EXPECTED_LOCATION_LAND_ON             = "any";
inline const static std::string DEFAULT_TEMPLATE_DELIVERY_EXPECTED_LOCATION_TYPE_VALUE = EXPECTED_LOCATION_TYPE_ICAO; //"icao"; // v3.0.219.6 for delivery type template
inline const static std::string EXPECTED_LOCATION_VALUE_DEFAULT_DISTANCE               = "30";                        // 30nm for XY based option
inline const static std::string DEFAULT_INVENTORY_RADIUS_MT                            = "100";                       // v3.0.219.7 used in RandomEngine.injectInventory() function

inline const static float      DEFAULT_RANDOM_POINT_JUMP_NM      = 0.5f; // v3.0.219.5 used in RandomEngine::getRandomAirport(). Represent how much nautical miles to jump each airport search
inline const static int        MAX_DISTANCE_TO_SEARCH_AIRPORT    = 2500; // v3.0.219.5 used in RandomEngine::getRandomAirport(). Represent the maximum distance we allow to search for next airport in nautical miles.
inline const static float      MIN_DISTANCE_TO_SEARCH_AIRPORT    = 1.5f; // v3.0.219.12 used in RandomEngine::getRandomAirport(). Represent the minimum distance to filter out airports.
inline const static int        DEFAULT_RANDOM_DEGREES_TO_EXCLUDE = 20;   // v3.0.219.5 used in RandomEngine::getRandomAirport(). Represent the degrees to include in the "exclude" configuration
inline const static float      DEFAULT_MAX_SLOPE_TO_LAND_ON      = 6.0f; // v3.0.219.11

inline const static std::map<std::string, int> MAP_IGNORE_PROPS_DURING_IO = { { PROP_ERROR_REASON, 0 } };

// Reality XP related
inline const static std::string RXP_FPEI_FLIGHT_PLAN_SEGMENT    = ":F:";              // v3.0.242.2 RealityXP Flight Plan Segment. Flight Plan Element Identifier (FPEI)
inline const static std::string EXTERNAL_FPLN_FOLDERS_FILE_NAME = "fpln_folders.ini"; // v3.0.242.2 Holds the file name in "missionx" plugin that points to all the external utilities FPLN/data folders. Example: RXP
inline const static std::string EXTERNAL_FPLN_TARGET_FILE_NAME  = "missionx_fpln";    // v3.0.242.2 Holds the file name missionx will create. Example: RXP

// ILS LAYER
inline const static double SLIDER_SHORTEST_MIN_ILS_SEARCH_RADIUS = 50;     // v3.0.253.6 holds the minimum search ILS airport
inline const static float  SLIDER_SHORTEST_MAX_ILS_SEARCH_RADIUS = 250.0;  // v3.0.253.6 holds the lowest MAX radius we will search for ILS airports.
inline const static float  SLIDER_ILS_MAX_SEARCH_RADIUS          = 9000.0; // v3.0.253.6 holds the MAX radius we will search for ILS airports.

inline const static int SLIDER_ILS_SHORTEST_RW_LENGTH_MT    = 740;  // v3.0.253.6 holds the shortest slider value allowed for runway length (ILS layer).
inline const static int SLIDER_ILS_LOGEST_RW_LENGTH_MT      = 3740; // v3.0.253.6 holds the longest slider value for runway length (ILS layer)
inline const static int SLIDER_ILS_STARTING_RW_LENGTH_VALUE = 1000; // v3.0.253.6 holds the longest slider value for runway length (ILS layer)

inline const static int SLIDER_ILS_SHORTEST_RW_WIDTH_MT    = 15; // v3.0.253.6 holds the shortest slider value allowed for runway width (ILS layer).
inline const static int SLIDER_ILS_WIDEST_RW_WIDTH_MT      = 70; // v3.0.253.6 holds the longest slider value for runway width (ILS layer)
inline const static int SLIDER_ILS_STARTING_RW_WIDTH_VALUE = 45; // v3.0.253.6 holds the longest slider value for runway width (ILS layer)

inline const static int SLIDER_ILS_LOWEST_AIRPORT_ELEV_FT         = -80;  // v3.0.253.6 holds the lowest slider value allowed for airport elevation (ILS layer).
inline const static int SLIDER_ILS_HIGHEST_AIRPORT_ELEV_FT        = 7000; // v3.0.253.6 holds the highest slider value for airport elevation (ILS layer)
inline const static int SLIDER_ILS_STARTING_AIRPORT_ELEV_VALUE_FT = 0;    // v3.0.253.6 holds the default slider value for airport elevation (ILS layer)

// SETUP Screen
inline const static double      SLIDER_MIN_RND_DIST                    = 5.0;
inline const static double      SLIDER_MAX_RND_DIST                    = 50.0;
inline const static float       MAX_RAD_4_OSM_MAX_DIST                 = 20.0; // v25.12.2

inline const static std::string SETUP_DISPLAY_TARGET_MARKERS                  = "setup_display_target_markers";                   // v3.0.241.7 will assist in show/hide instanced objects that are flaged with attribute: "target_marker_b" attribute
inline const static std::string SETUP_DISPLAY_TARGET_MARKERS_AWAY_FROM_TARGET = "setup_display_target_markers_away_from_targets"; // v3.0.241.7 Random medevac missions will display the target marker ~3nm from target.
inline const static std::string OPT_OVERRIDE_RANDOM_TARGET_MIN_DISTANCE       = "opt_override_target_min_distance";               // v3.0.241.7 Random medevac missions will override target distance. This is a hint and not for all targets type. "allow_override_b" atttribute should be present.
inline const static std::string SETUP_SLIDER_RANDOM_TARGET_MIN_DISTANCE = "setup_slider_med_target_min_distance"; // v3.0.241.7 holds slider value of minimal expected distance
inline const static std::string SETUP_SLIDER_FONT_SCALE_SIZE            = "setup_slider_font_scale_size";         // v3.0.251.1 holds preferred font size by user
inline const static std::string SETUP_LOCK_OVERPASS_URL_TO_USER_PICK    = "setup_lock_overpass_url_to_user_pick"; // v3.0.253.4.2 used with overpass combo
inline const static std::string NO_SETUP_TEXT                           = "N/An/aN/An/a";                         // v3.0.253.4.2 used with overpass combo
inline const static std::string SETUP_NORMALIZED_VOLUME                 = "setup_normalized_volume";              // v3.0.303.6 Used with volume settings. Values between 1..100
inline const static int         DEFAULT_SETUP_MISSION_VOLUME_I          = 30;                                     // v3.0.303.6 holds default volume value
inline const static std::string SETUP_NORMALIZE_VOLUME_B                = "setup_normalize_volume_b";             // v3.0.303.6 If the failure is true: then use the "setup_mission_volume" to override higher mixture values. Example, if the <mix> tag defines a volume of 0.5 and the setup volume is set to 40 (=0.4) then it will force the lower value
inline const static std::string SETUP_AUTHORIZATIOJN_KEY                = "setup_authorization_key_s";            // v3.303.8.3 authorization key from flightplandatabase.com
inline constexpr const static char* SETUP_PILOT_NAME                    = "setup_pilot_name";                     // v3.305.1 pilot nickname used in story mode messages
inline constexpr const static char* DEFAULT_SETUP_PILOT_NAME            = "Pilot";                                // v3.305.1 pilot nickname used in story mode messages
inline constexpr const static char* SETUP_USE_XP11_INV_LAYOUT           = "setup_use_xp11_inv_layout";            // v24.12.2 Toggle option to use the Original inventory ui layout, instead of "station" layout.


// DEPRECATED ELEMENTS turned to constexpr
inline constexpr const static char* SETUP_WRITE_CACHE_TO_DB = "setup_write_cache_to_db"; // v3.0.253.3 Write information to SQLITE DB too.
inline constexpr const static char* SETUP_FONT_PIXEL_SIZE   = "setup_font_pixel_size";   // v3.0.251.1 holds preferred font pixel size
inline constexpr const static char* SETUP_FONT_LOCATION     = "setup_font_location";     // v3.0.251.1 holds preferred font pixel size


#ifdef LIN
inline const static std::string SETUP_LINUX_FLAVOR_CODE_I = "setup_linux_flavor_code_i"; // v3.303.8.1 FMOD seems to crash on System::release() in arch based Linux distro. The workaround is to not release it at all. It is not the best approach but it is a tested workaround none the less.
#endif


// properties name to user UI
inline const static std::string PROP_FPLN_ID_PICKED           = "fpln_id_picked";           // v3.0.253.1 Used with "fetch external flight plan" layer, and holds the FPLN ID picked for Random Engine + missionx::data_manager::tableExternalFPLN_vec
inline const static std::string PROP_MED_CARGO_OR_OILRIG      = "med_cargo_or_oilrig";      // v3.0.241.9 UI info from user defined mission screen
inline const static std::string PROP_MISSION_SUBCATEGORY      = "mission_subcategory";      // v3.303.14 sub category property. Used with user creation screen, stored which subcategory the user picked. Example: OilRig, medevac
inline const static std::string PROP_MISSION_SUBCATEGORY_LBL  = "mission_subcategory_lbl";  // v24.05.1
inline const static std::string PROP_STARTING_DAY             = "starting_day";             // v3.0.303.8 UI info from user defined mission screen
inline const static std::string PROP_STARTING_HOUR            = "starting_hour";            // v3.0.303.8 UI info from user defined mission screen
inline const static std::string PROP_STARTING_MINUTE          = "starting_minutes";         // v3.0.303.8 UI info from user defined mission screen
inline const static std::string PROP_PLANE_TYPE_I             = "plane_type_i";             // v3.0.241.9 UI info from user defined mission screen
inline const static std::string PROP_PLANE_TYPE_S             = "plane_type_s";             // v3.0.241.9 UI info from user defined mission screen
inline const static std::string PROP_NO_OF_LEGS               = "no_of_legs";               // v3.0.241.9 UI info from user defined mission screen
inline const static std::string PROP_MIN_DISTANCE_SLIDER      = "min_distance_slider";      // v3.0.241.9 UI info from user defined mission screen
inline const static std::string PROP_MAX_DISTANCE_SLIDER      = "max_distance_slider";      // v3.0.241.9 UI info from user defined mission screen
inline const static std::string PROP_USE_OSM_CHECKBOX         = "random_use_osm_cb";        // Use OSM databases checkbox.
inline const static std::string PROP_USE_WEB_OSM_CHECKBOX     = "random_use_web_osm_cb";    // Use WEB OSM databases checkbox.
inline const static std::string PROP_NARROW_HELOS_RAMP_SEARCH = "narrow_helos_ramp_search"; // v3.0.253.7
inline const static std::string PROP_ADD_COUNTDOWN            = "add_countdown";            // v3.0.253.9.1 add countdown to most flight legs except the last one
inline const static std::string PROP_START_FROM_PLANE_POSITION      = "start_from_plane_position";      // v3.0.253.11 User prefers not to move the plane when it searches for a starting location. Good for cold and dark cases since after moving the plane using the API, the plane is out of cold and dark state
inline const static std::string PROP_GENERATE_GPS_WAYPOINTS         = "generate_gps_waypoints";         // v3.0.253.12 Should Random Engine generate GPS waypoint or not. Will be used in "User creation", "ILS" and "external FPLN" screens.
inline const static std::string PROP_FILTER_AIRPORTS_BY_RUNWAY_TYPE = "filter_airports_by_runway_type"; // v3.0.255.2 Defined in "create user screen" and used in RandomEngine when needs to filter our airports that has only excluded runways types
inline const static std::string PROP_REMOVE_DUPLICATE_ICAO_ROWS     = "remove_duplicate_icao_rows";     // v3.303.8.3 During data_manager::fetch_fpln_from_external_site() remove duplicate ICAO results. Used in FPLN from External site screen.
inline const static std::string PROP_GROUP_DUPLICATES_BY_WAYPOINTS  = "group_duplicates_by_waypoints";  // v3.303.8.3 During data_manager::fetch_fpln_from_external_site() remove duplicate ICAO & Waypoint numbers. Used in FPLN from External site screen.
inline const static std::string PROP_WEATHER_USER_PICKED            = "weather_user_picked";            // v3.303.12
inline const static std::string PROP_WEATHER_CHANGE_MODE_USER_PICKED = "weather_change_mode_user_picked"; // v3.303.13  // TODO CONSIDER REPRECATING
inline const static std::string VALUE_STORE_CURRENT_WEATHER_DATAREFS = "store_current_weather_datarefs";  // v3.303.13
inline const static std::string PROP_ADD_DEFAULT_WEIGHTS_TO_PLANE    = "add_default_weights_to_plane";  // v3.303.14

// const static std::string PROP_USE_WEB_OSM_FILTER = "random_web_osm_filter"; // v3.0.253.4 WEB OSM filter. ===> use OPT_OVERPASS_FILTER syntax instead
inline const static std::string PROP_FROM_ICAO    = "fromICAO"; // used with external fpln
inline const static std::string PROP_TO_ICAO      = "toICAO";   // v3.0.253.3 used with external fpln // add the option to define destination location
inline const static std::string PROP_LIMIT        = "limit";    // used with external fpln
inline const static std::string PROP_SORT_FPLN_BY = "sort";     // used with external fpln

inline const static std::string RANDOM_TEMPLATE_BLANK_4_UI = "template_blank_4_ui.xml"; // template_blank_4_ui is the only template that is driven by UI so we should not load them.
inline const static std::string COORDINATES_IN_THE_GPS_S   = "Coordinates in GPS";

//// SQLite DB
inline const static std::string ELEMENT_UI_PROPERTIES = "ui_properties";
inline const static std::string DB_AIRPORTS_XP        = "airports_xp.sqlite.db";
inline const static std::string DB_AIRPORTS_XP2       = "airports_xp2.sqlite.db";
inline const static std::string DB_STATS_XP           = "stats_xp.sqlite.db"; // v3.0.255.1 holds running mission stats
inline const static std::string DB_FOLDER_NAME        = "db";                 // sub folder in "missionx" plugin folder.
inline const static std::string DB_FILE_EXTENSION     = ".db";                // sub folder in "missionx" plugin folder.

inline const static std::string STATS_LANDING    = "landing";                                         // "L"; // "Landing" // v3.0.255.1
inline const static std::string STATS_TAKEOFF    = "takeoff";                                         //"T"; // "takeoff"  // v3.0.255.1
inline const static std::string STATS_TRANSITION = "transition";                                      //"T"; // "takeoff"  // v3.0.255.1
                                                                                               // OVERPASS
inline const static std::string DEFAULT_OVERPASS_URL = "https://lz4.overpass-api.de/api/interpreter"; // v3.0.253.6 overpass default overpass URL
inline const static std::string DEFAULT_OVERPASS_WAYS_FILTER =
  "[timeout:25];(way[highway=primary]({{bbox}});way[highway=secondary]({{bbox}});way[highway=tertiary]({{bbox}});way[highway=residential]({{bbox}});way[highway=service]({{bbox}});way[highway=living_street]({{bbox}});way[highway=track]({{"
  "bbox}}););out;"; // v3.0.253.9 added way and ";" since we changed the way designer provide filters. We do not fix their code. // v3.0.253.4 overpass ways filter

inline const static std::string ATTRIB_OSM_KEY   = "k";      // v3.0.253.4 <way k="" v="">
inline const static std::string ATTRIB_OSM_VALUE = "v";      // v3.0.253.4 <way k="" v="">
inline const static std::string ELEMENT_NODE_OSM = "node";   // v3.0.253.4 <node id="" lat=""...">
inline const static std::string ELEMENT_CENTER   = "center"; // v3.0.253.9 <center ><way> sub-element. Will be present if designer added: ";out center;" to the filter text
inline const static std::string ELEMENT_WAY_OSM  = "way";    // v3.0.253.4 <way id="">
inline const static std::string ELEMENT_ND_OSM   = "nd";     // v3.0.253.4 <way id="">
inline const static std::string ELEMENT_REL_OSM  = "rel";    // v3.0.253.9 <rel >
inline const static std::string ATTRIB_REF_OSM   = "ref";    // v3.0.253.4 <way id="">


inline const static std::string ATTRIB_WEBOSM_OPTIMIZE = "webosm_optimize"; // v3.0.253.4     <expected_location location_type="webosm,near,xy"
                                                                     // location_value="nm=25|tag=webosm_helipads|webosm_optimize=n|ramp=H|dbfile=osm-liechtenstein.db,nm=20,tag=start_location" force_template_distances_b="yes" />
inline const static float NEAR_ENOUGH_DISTANCE_FOR_CUSTOM_NAVAID_SCENERY = 20.0f; // v3.0.253.6 used when picking near navaid and we want to also consider custom scenery, so we first pick the closest scenery and then loop over the navaids that have custom scenery and check their distance relative to the one we
         // know is nearest. In ~20nm distance from nearest we should pick the custom scenery navaid over the default one.
inline const int PERCENT_TO_PICK_CUSTOM_SCENERY_OVER_GENERIC = 40;                // v3.0.253.6 used in RandomEngine::getRandomAirport_localThread()


//
inline const std::string ELEMENT_TIMER        = "timer";         // v3.0.253.7 used in flight leg and global settings. We should only have 1 in each root element.
inline const std::string ATTRIB_FAIL_MSG      = "fail_msg";      // v3.0.253.7 used in flight leg and global settings. <timer... fail_msg>
inline const std::string ATTRIB_SUCCESS_MSG   = "success_msg";   // v3.0.253.7 used in flight leg and global settings. <timer... success_msg>
inline const std::string ATTRIB_RUN_UNTIL_LEG = "run_until_leg"; // v3.0.253.7 used in <timer... success_msg>
inline const std::string ATTRIB_FAIL_ON_TIMEOUT_B = "fail_on_timeout_b"; // v3.305.3 used in <timer>, we also support "post_script"
inline const std::string ATTRIB_STOP_ON_LEG_END_B = "stop_on_leg_end_b"; // v3.305.3 used in <timer>
 
inline const std::string ELEMENT_SCORING     = "scoring"; // v3.303.9.1 renamed "rank" element to "scoring"
inline const std::string ELEMENT_PITCH       = "pitch";   // v3.303.8.3 scoring sub element.
inline const std::string ELEMENT_ROLL        = "roll";    // v3.303.8.3 scoring sub element.
inline const std::string ELEMENT_GFORCE      = "gforce";  // v3.303.8.3 scoring sub element.
inline const std::string ELEMENT_CENTER_LINE = "center_line"; // v3.303.8.3 scoring sub element.

// attribs from fragment time
inline const std::string ATTRIB_ZULU_TIME_SEC                          = "begin_zuluTime_sec";                     // v3.0.253.7 zuluTime_sec used in <timer... > save checkpoint
inline const std::string ATTRIB_DAY_IN_YEAR                            = "day_in_year";                            // v3.0.253.7 day_in_year used in <timer... > save checkpoint
inline const std::string ATTRIB_TOTAL_RUNNING_TIME_SEC_SINCE_SIM_START = "total_running_time_sec_since_sim_start"; // v3.0.253.7 total_running_time_sec_since_sim_start used in <timer... > save checkpoint
// attribs from timer
inline const std::string ATTRIB_RUN_CONTINUOUSLY = "run_continuously"; // v3.0.253.7 used in <timer... > save checkpoint
inline const std::string ATTRIB_SECONDS_PASSED   = "seconds_passed";   // v3.0.253.7 used in <timer... > save checkpoint
inline const std::string ATTRIB_BEGIN_TIME       = "begin_time";       // v3.0.253.7 used in <timer... > save checkpoint
inline const std::string ATTRIB_TIMER_STATE      = "timer_state";      // v3.0.253.7 used in <timer... > save checkpoint

// XML replace options // v3.0.255.4
inline const std::string ATTRIB_FIND                 = "find";                 // v3.0.255.4
inline const std::string ATTRIB_REPLACE_WITH         = "replace_with";         // v3.0.255.4
inline const std::string TEMPLATE_INJECTED_FILE_NAME = "mx_tmp3981234.work.t"; // v3.0.255.4 the name of the final template that holds all the injected data into it

inline const static std::string ELEMENT_LNM_LittleNavmap = "LittleNavmap"; // v3.0.301 <LittleNavmap> ROOT
inline const static std::string ELEMENT_LNM_Flightplan   = "Flightplan";   // v3.0.301 <Flightplan>
inline const static std::string ELEMENT_LNM_Waypoints    = "Waypoints";    // v3.0.301 <Waypoints>
inline const static std::string ELEMENT_LNM_Waypoint     = "Waypoint";     // v3.0.301 <Waypoint>
inline const static std::string ELEMENT_LNM_Departure    = "Departure";    // v3.0.301 <Departure>
inline const static std::string ELEMENT_LNM_Pos          = "Pos";          // v3.0.301 <Pos>
inline const static std::string ELEMENT_LNM_Heading      = "Heading";      // v3.0.301 <Heading>
inline const static std::string ELEMENT_LNM_Name         = "Name";         // v3.0.301 <Name>
inline const static std::string ELEMENT_LNM_Ident        = "Ident";        // v3.0.301 <Ident>
inline const static std::string ELEMENT_LNM_Region       = "Region";       // v3.0.301 <Region>
inline const static std::string ELEMENT_LNM_Type         = "Type";         // v3.0.301 <Type>

inline const static std::string ATTRIB_LNM_Lat = "Lat";    // v3.0.301
inline const static std::string ATTRIB_LNM_Lon = "Lon";    // v3.0.301
inline const static std::string ATTRIB_LNM_Alt = "Alt";    // v3.0.301

// Sling Load
// The new datarefs to monitor cargo position
inline static const std::string DREF_TARGET_POS_LAT_D    = "HSL/Cargo/pos_lat_d";
inline static const std::string DREF_TARGET_POS_LON_D    = "HSL/Cargo/pos_lon_d";
inline static const std::string DREF_TARGET_POS_ELEV_M_D = "HSL/Cargo/pos_elev_mt_d";

// These datarefs are dependent on the "Sling Load" plugin. If one of them is changed, then we must do the same to the string values and then recompile Mission-X plugin. No need to change the const string parameter name
inline static const std::string DREF_EXTERNAL_HSL_CARGO_SET_LATITUDE  = "HSL/Cargo/SetLatitude";
inline static const std::string DREF_EXTERNAL_HSL_CARGO_SET_LONGITUDE = "HSL/Cargo/SetLongitude";
inline static const std::string DREF_EXTERNAL_HSL_CARGO_MASS          = "HSL/Cargo/Mass";
inline static const std::string DREF_EXTERNAL_HSL_CARGO_FOLLOW_ONLY   = "HSL/Cargo/FollowOnly"; // integer 1 if the cargo is neither connected nor placed on terrain, 0 otherwise.
inline static const std::string DREF_EXTERNAL_HSL_CARGO_CONNECTED     = "HSL/Cargo/Connected";  // 1 if the cargo is connected to the rope, 0 otherwise.

inline static const std::string CMD_EXTERNAL_HSL_ENABLE_SLING_LOAD         = "HSL/Sling_Enable";        // Enable Sling Load
inline static const std::string CMD_EXTERNAL_HSL_DISABLE_SLING_LOAD        = "HSL/Sling_Disable";       // Disable Sling Load
inline static const std::string CMD_EXTERNAL_HSL_CARGO_LOAD_ON_COORDINATES = "HSL/Load_On_Coordinates"; // Place the load at given coordinates
inline static const std::string CMD_EXTERNAL_HSL_UPDATE_OBJECTS_SLING_LOAD = "HSL/UpdateObjects";       // Update HSL 3D Objects

inline static const double SLING_LOAD_SUCCESS_RADIUS_MT = 10.0; // sling load success radius in meters

// Storing conversion data
inline static const std::string ELEMENT_FPLN    = "FPLN";    // v3.0.303.4
inline static const std::string ELEMENT_BUFFERS = "BUFFERS"; // v3.0.303.4
inline static const std::string ELEMENT_BUFF    = "BUFF";    // v3.0.303.4
inline const static std::string ATTRIB_INDEX    = "index";   // v3.0.303.4

inline const static std::string CONV_ATTRIB_isLeg            = "isLeg";            // v3.0.303.4
inline const static std::string CONV_ATTRIB_isLast           = "isLast";           // v3.0.303.4
inline const static std::string CONV_ATTRIB_ignore_leg       = "ignore_leg";       // v3.0.303.4
inline const static std::string CONV_ATTRIB_convertToBriefer = "convertToBriefer"; // v3.0.303.4
inline const static std::string CONV_ATTRIB_add_marker       = "add_marker";       // v3.0.303.4

inline const static std::string CONV_ATTRIB_markerType               = "markerType";                          // v3.0.303.4
inline const static std::string CONV_ATTRIB_iCurrentBuf              = "iCurrentBuf";                         // v3.0.303.4
inline const static std::string CONV_ATTRIB_radius_to_display_marker = "radius_to_display_3D_marker_in_nm_f"; // v3.0.303.4

inline const static std::string CONV_ATTRIB_on_ground           = "on_ground";           // v3.0.303.4
inline const static std::string CONV_ATTRIB_elev_combo_picked_i = "elev_combo_picked_i"; // v3.0.303.4
inline const static std::string CONV_ATTRIB_slider_elev_value_i = "slider_elev_value_i"; // v3.0.303.4 inLegData.target_trig_strct.slider_elev_value_i
inline const static std::string CONV_ATTRIB_elev_rule_s         = "elev_rule_s";         // v3.0.303.4

// Triggers related
inline const static std::string CONV_ATTRIB_trig_ui_type_combo_indx      = "trig_ui_type_combo_indx";      // v3.0.303.4 radius, script, box
inline const static std::string CONV_ATTRIB_trig_ui_plane_pos_combo_indx = "trig_ui_plane_pos_combo_indx"; // v3.0.303.4 on ground, ignore, airborne
inline const static std::string CONV_ATTRIB_trig_ui_elev_type_combo_indx = "trig_ui_elev_type_combo_indx"; // v3.0.303.4 above than, min/max etc...


// SOUND related
inline const static float MIN_SOUND_VOLUME_F = 0.0f;
inline const static float MAX_SOUND_VOLUME_F = 100.0f;

// gather stats
inline const static float GATHER_STATS_INTERVAL_SEC = 1.0f;

// SQLite sql.xml elements name
inline const static std::string ELEMENT_QUERY       = "query";
inline const static std::string SQLITE_OILRIG_SQLS  = "oilrig_sqls";
inline const static std::string SQLITE_OSM_SQLS     = "osm_sqls";
inline const static std::string SQLITE_NAVDATA_SQLS = "navdata_sqls";
inline const static std::string SQLITE_ILS_SQLS     = "ils_sqls";

// v3.305.1 story mode constants
inline const static std::string STORY_IMAGE_POS               = "imgPos";             // image position. ATTRIB_NAME
inline const static std::string STORY_CHARACTER_CODE          = "storyCharacterCode"; // Story Character Code to find in Message->mapCharacters
inline const static std::string STORY_TEXT                    = "storyText";          // Story Text
inline const static std::string STORY_PAUSE_TIME              = "storyPauseTime";     // How much time to Pause before skipping
inline const static std::string ATTRIB_IGNORE_PUNCTUATIONS_B  = "ignore_punctuations_b"; // v3.305.1 Used in story mode. Do you want the plugin to use same timer for all characters
inline constexpr const static char STORY_ACTION_TEXT          = 't';                  // action t - display text
inline constexpr const static char STORY_ACTION_IMG           = 'i';                  // action i - load image
inline constexpr const static char STORY_ACTION_PAUSE         = 'p';                  // action p - pause
inline constexpr const static char STORY_ACTION_HIDE          = 'h';                  // action h - Hides main window, only as last line
inline constexpr const static char STORY_ACTION_MSGPAD        = 'm';                  // action m - show text in message pad and as a text ?

inline const static std::string STORY_DEFAULT_TITLE_CODE      = "n/a";                // If no character found then use N/A
inline const static std::string STORY_DEFAULT_TITLE_NA        = "n/a";                // If no character found then use N/A

inline static const std::vector<const char*> vecStoryActions = { "[i]", "[p]", "[c]", "[h]", "[m]", "[t]" }; // s=speak title, p=pause, i=image, c=choice, m=text line + display in mxpad, t=text line
inline static const std::vector<const char*> vecStoryPunctuation = { ">", "<" }; // > ignore punctuation, < force punctuation

inline constexpr const static float STORY_DEFAULT_TIME_BETWEEN_CHARS_SEC_F = 0.05f;
inline constexpr const static float STORY_DEFAULT_TIME_AFTER_PERIOD_SEC_F  = 1.25f;
inline constexpr const static float STORY_DEFAULT_TIME_AFTER_COMMA_SEC_F   = 0.60f;
inline constexpr const static float STORY_DEFAULT_PUNCTUATION_EXPONENT_F   = 0.5f;
inline constexpr const static float DEFAULT_SKIP_MESSAGE_TIMER_IN_SEC_F    = 3.0f; // v3.305.3 moved from uiImGuiBriefer->strct_flight_leg_info.strct_story_mode



inline constexpr const static float DEFAULT_SF_FADE_SECONDS_F = 6.0; // v3.306.1b
inline const std::string            ATTRIB_FALLTHROUGH_B      = "fallthrough_b"; // v3.305.1b used in Messages. If message is last message and we want quick transition to next leg, we should set this attribute to true.


// v3.305.3 interpolation properties
inline static constexpr const char* PROP_SECONDS_TO_RUN_I        = "seconds_to_run_i";
inline static constexpr const char* PROP_FOR_HOW_MANY_CYCLES_I   = "for_how_many_cycles_i";
inline static constexpr const char* PROP_CURRENTCYCLECOUNTER_I   = "currentCycleCounter_i";
inline static constexpr const char* PROP_TARGET_VALUE_D          = "target_value_d";

inline static constexpr const char* PROP_DELTA_SCALAR            = "delta_scalar";
inline static constexpr const char* PROP_DELTA_ARRAY_F           = "delta_array_f";
inline static constexpr const char* PROP_LAST_VALUE_ARRAY_D      = "last_value_array_d";

inline static constexpr const char* PROP_STARTING_SCALAR_VALUE_D = "starting_scalar_value_d";
inline static constexpr const char* PROP_LAST_VALUE_D            = "last_value_d";
inline static constexpr const char* PROP_SECONDS_PASSED_F        = "seconds_passed_f";
inline static constexpr const char* PROP_TARGET_VALUES_S         = "target_values_s";
inline static constexpr const char* PROP_START_VALUES_S          = "start_values_s";
inline static constexpr const char* PROP_DELTA_VALUES_S          = "delta_values_s";

inline constexpr const static char* REPLACE_KEYWORD_PILOT_NAME = "%pilot%"; // v3.305.1 replace string used when reading a story mode message line and we want to replace it
inline static constexpr const char* REPLACE_KEYWORD_SELF       = "%self%";  // v3.305.3 can be used with scripts as a parameter sending of a dynamic trigger that we don't know its name, like: "trigger name" from dynamic message.

inline static const std::string ELEMENT_NOTES = "notes"; // v24.03.1


inline static const std::string ELEMENT_STATION = "station"; // v24.12.2 Used with acf cargo information. sim/flightmodel/weight/m_stations []
inline static const std::string ATTRIB_TARGET_INVENTORY = "target_inventory"; // v24.12.2 target inventory



} // end namespace mxconst


#endif // XX_MY_CONSTANTS_HPP
