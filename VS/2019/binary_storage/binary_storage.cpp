#include <iostream>
#include <fstream>
#include <string>
#include <map>
#include "IXMLParser.h"

typedef struct _local_fpln_strct
{
  bool flag_isLeg{ false };
  bool flag_isLast{ false };
  bool flag_ignore_leg{ false };
  bool flag_convertToBriefer{ false };
  bool flag_add_marker{ false }; // v3.0.303.2, will be shown only for checked legs

  int         marker_type_i{ 0 }; // v3.0.303.3 0 = default between 0..3
  int         indx{ -1 };         // holds the line number
  int         iCurrentBuf{ 0 };
  float       radius_to_display_3D_marker_in_nm_f{ 10.0f }; // v3.0.303.3 holds radius in nm
  double      distToPrevWaypoint{ 0.0 }, cumulativeDist{ 0.0 };
  std::string name;
  std::string ident;
  std::string type;
  std::string region;

  std::string attribName;

  ////missionx::Point p; // location
  //IXMLNode        xFlightPlan;
  //IXMLNode        xLeg;
  //IXMLNode        xTriggers;
  //IXMLNode        xMessageTmpl;
  //IXMLNode        xObjectives;
  //char            buff_arr[20][2048]{ '\0' }; // 2048
  //
  //typedef struct trig_ // inner struct
  //{
  //  bool        flag_on_ground{ false };
  //  int         elev_min{ 0 };
  //  int         elev_max{ 0 };
  //  int         elev_combo_picked_i{ -1 };
  //  std::string elev_rule_s{ "" };
  //  std::string elev_lower_upper{ "" };
  //  void        init()
  //  {
  //    flag_on_ground      = false;
  //    elev_min            = 0;
  //    elev_max            = 0;
  //    elev_combo_picked_i = -1;
  //
  //    elev_rule_s.clear();
  //    elev_lower_upper.clear();
  //  }
  //} trig_strct_;
  //trig_strct_ trig_strct;
  //
  //
  //void reset()
  //{
  //  indx                  = -1;
  //  flag_isLast           = false;
  //  flag_isLeg            = false;
  //  flag_ignore_leg       = false;
  //  flag_convertToBriefer = false;
  //  flag_add_marker       = false; // v3.0.303.2
  //
  //  distToPrevWaypoint = 0.0;
  //  cumulativeDist     = 0.0;
  //  name.clear();
  //  type.clear();
  //  region.clear();
  //  attribName.clear();
  //  //p.init();
  //
  //  xFlightPlan = IXMLNode::emptyIXMLNode;
  //  xLeg        = IXMLNode::emptyIXMLNode;
  //  resetBuffPopBrief();
  //}
  //
  //void resetBuffPopBrief()
  //{
  //  for (auto& b : buff_arr)
  //    memset(b, '\0', sizeof(b));
  //}
  //
  //std::string getName() { return (name.empty()) ? ident : name + "(" + ident + ")"; }

} mx_local_fpln_strct;

///  MAIN CLASS /////////////////////////
int main()
{
  IXMLNode            xConvMainNode, xConvDummy;
  mx_local_fpln_strct legData;


  xConvMainNode = IXMLNode::createXMLTopNode("xml", TRUE);
  xConvMainNode.addAttribute("version", "1.0");
  xConvMainNode.addAttribute("encoding", "ASCII"); // "ISO-8859-1");
  xConvMainNode.addClear("\n\tFile has been created by binary-storage code.\n\tAny modification might break or invalidate the file.\n\t", "<!--", "-->");

  IXMLDomParser iDomTemplate;
  IXMLResults   parse_result_strct;

  //  "<DUMMY> </DUMMY>"
  xConvDummy = iDomTemplate.parseString("<DUMMY> </DUMMY>", "DUMMY", &parse_result_strct).deepCopy();
  xConvMainNode.addChild(xConvDummy);

  // prepare out file: customAptDat.txt file
  std::ios_base::sync_with_stdio(false); // v3.0.219.10
  std::cin.tie(nullptr); 

  std::ofstream savefile("converter.dat", std::ios_base::binary);
  if (savefile.good())
  {
    legData.indx = 2;
    legData.flag_add_marker = false;
    legData.flag_isLeg      = true;
    legData.flag_isLast     = false;
    legData.flag_add_marker = true;
    legData.marker_type_i   = 5;
    legData.iCurrentBuf     = 0;
    legData.name            = "WP1";
    legData.attribName      = "leg2";
    

    legData.radius_to_display_3D_marker_in_nm_f = 10.0;


    //legData.xFlightPlan = xConvDummy.addChild("flight_plan");
    //legData.xLeg        = legData.xFlightPlan.addChild("leg");
    //legData.attribName  = "leg2";
    //legData.xLeg.updateAttribute(legData.attribName.c_str(), "name");


    savefile.write(reinterpret_cast<char*>(&legData.indx), sizeof legData.indx);                           savefile.write("\0",sizeof(char));
    savefile.write(reinterpret_cast<char*>(&legData.flag_add_marker), sizeof legData.flag_add_marker);     savefile.write("\0",sizeof(char));
    savefile.write(reinterpret_cast<char*>(&legData.flag_isLeg), sizeof legData.flag_isLeg);               savefile.write("\0",sizeof(char));
    savefile.write(reinterpret_cast<char*>(&legData.flag_isLast), sizeof legData.indx);                    savefile.write("\0",sizeof(char));
    savefile.write(reinterpret_cast<char*>(&legData.flag_add_marker), sizeof legData.flag_add_marker);     savefile.write("\0",sizeof(char));
    savefile.write(reinterpret_cast<char*>(&legData.marker_type_i), sizeof legData.marker_type_i);         savefile.write("\0",sizeof(char));
    savefile.write(reinterpret_cast<char*>(&legData.iCurrentBuf), sizeof legData.iCurrentBuf);             savefile.write("\0",sizeof(char));
    savefile.write(legData.name.c_str(), legData.name.size());
    savefile.write("\0", sizeof(char));
    savefile.write(legData.attribName.c_str(), legData.attribName.size());
    savefile.write("\0", sizeof(char));


    //savefile.write("\0", sizeof(char));                                             // null end string for easier reading
    savefile.close();
  }
  //std::cout << "Save Struct Size = " << sizeof (legData) << "\n";

  std::ifstream loadfile("converter.dat", std::ios_base::binary);
  if (loadfile.good())
  {
    mx_local_fpln_strct legDataRead;
                 // get player name (remember we null ternimated in binary)
    loadfile.read((char*)&legDataRead.indx, sizeof(legDataRead.indx)); // 
    loadfile.read((char*)&legDataRead.flag_add_marker, sizeof(legDataRead.flag_add_marker)); // 
    loadfile.read((char*)&legDataRead.flag_isLeg, sizeof(legDataRead.flag_isLeg));           // 

    loadfile.close();
  }



  return 0;
}