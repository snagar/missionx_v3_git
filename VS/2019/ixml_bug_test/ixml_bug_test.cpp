// ixml_bug_test.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <iostream>
#include "IXMLParser.h"

int main()
{
  static const std::string ROOT_CONFIGURATION = "CONFIGURATION";
  static const std::string ROOT_OPTIONS = "OPTIONS";
  static const std::string ROOT_MISSIONX = "MISSIONX";

  std::string errMsg;
  IXMLDomParser iDomTemplate;
 // ITCXMLNode xTemplateNode = iDomTemplate.openFileHelper("node_data:\\programming\\GIT\\missionx_v3_main\\VS\\2017\\ixml_bug_test\\", "TEMPLATE", &errMsg); // parse xml into ITCXMLNode
  ITCXMLNode xTemplateNode = iDomTemplate.openFileHelper("d:\\xp11clean/Output/preferences/missionx_pref_v3.xml", ROOT_MISSIONX.c_str(), &errMsg); // parse xml into ITCXMLNode
  if (!errMsg.empty()) // check if there is any failure during read
  {
   
    std::cout << errMsg << std::endl;
    return -1;
  }

  // If missionx was not found












  IXMLNode   xRootTemplate = xTemplateNode.deepCopy(); // convert ITCXMLNode to IXMLNode. IXMLNode allow to modify itself

  /// loop over goals and print out the number of <display_object> elements and their name attribute.
  //IXMLNode xGoals = xRootTemplate.getChildNode("goals"); // get <goals> element
  //if (xGoals.isEmpty())
  //{
  //  std::cout << "fail to find <goals> element. Exiting." << std::endl;
  //  return -1;
  //}

  int nChilds = xRootTemplate.nChildNode("goal");
  for (int i1 = 0; i1 < nChilds; ++i1)
  {
    IXMLNode xGoal = xRootTemplate.getChildNode("goal", i1);
    if (xGoal.isEmpty())
    {
      std::cout << "<goal> element: " << i1 << ", seem to be EMPTY. Skipping." << std::endl;
      continue;
    }
    else
    {
      std::cout << "<goal> element: " << i1 << ", seem to be VALID." << std::endl;

      // read sub element <display_object>
      int nInstances = xGoal.nChildNode("display_object");
      for (int i2 = 0; i2 < nInstances; ++i2)
      {
        IXMLNode cNode = xGoal.getChildNode("display_object", i2);
        if (!cNode.isEmpty())
          std::cout << "   <display_object> element: " << i2 << ", is valid. Its name: " + std::string(cNode.getAttribute("name")) << std::endl;
        else
          std::cout << "   <display_object> element: " << i2 << ", is NULL - BUG ???" << std::endl;
      }
    }
  }



    return 0;
}

