//#define CURL_STATICLIB    

#include <iostream>
#include <string>

#include "curl/curl.h"
//#include <nlohmann/json.hpp>
#include "SimplePolyline.h"

#include "../../../src/io/IXMLParser.h"

#define external_fpln
//#define overpass

// for convenience
//using json = nlohmann::json;

static size_t my_write(void* buffer, size_t size, size_t nmemb, void* param)
{
  std::string& text = *static_cast<std::string*>(param);
  size_t totalsize = size * nmemb;
  text.append(static_cast<char*>(buffer), totalsize);
  return totalsize;
}

int main()
{
#ifdef external_fpln
  const std::string url_s = "https://api.flightplandatabase.com:443/search/plans?fromICAO=XBEL&distanceMax=0&limit=20&sort=created";
  const std::string authKey = "vj96ENFziRb0oy7W0VlQ6NJTuXFdJkzIZeREAkJu"; // saar authorization
#else
  //const std::string url_s = "https://overpass-api.de/api/interpreter?data=way\[\"highway\"=\"primary\"\]\[\"highway\"=\"secondary\"\]\[\"highway\"=\"tertiary\"\]\[\"highway\"=\"residential\"\]\[\"highway\"=\"service\"\]\[\"highway\"=\"living_street\"\]\[\"highway\"=\"track\"\](51.379137,-0.135082,51.629866,0.110228);out;";
  //const std::string url_s = "https://overpass-api.de/api/interpreter?data=way\[\"highway\"=\"primary\"\]\[\"highway\"=\"secondary\"\]\[\"highway\"=\"tertiary\"\]\[\"highway\"=\"residential\"\]\[\"highway\"=\"service\"\]\[\"highway\"=\"living_street\"\]\[\"highway\"=\"track\"\](47.832336,-121.644760,47.838752,-121.622610);out;";
  //const std::string url_s = "https://overpass-api.de/api/interpreter?data=way\[\"highway\"=\"primary\"\]\[\"highway\"=\"secondary\"\]\[\"highway\"=\"tertiary\"\]\[\"highway\"=\"residential\"\]\[\"highway\"=\"service\"\]\[\"highway\"=\"living_street\"\]\[\"highway\"=\"track\"\](47.821628,-121.659902,47.842416,-121.609180);out;";
  const std::string url_s = "https://overpass-api.de/api/interpreter?data=way[\"highway\"=\"primary\"][\"highway\"=\"secondary\"][\"highway\"=\"tertiary\"][\"highway\"=\"residential\"][\"highway\"=\"service\"][\"highway\"=\"living_street\"][\"highway\"=\"track\"](47.821628,-121.659902,47.842416,-121.609180);out;";
  //const std::string url_s = "https://overpass-api.de/api/interpreter?data=way[leisure=pitch][sport][!building](47.821628,-121.659902,47.842416,-121.609180);out;";
  //const std::string url_s = "https://overpass-api.de/api/interpreter?data=way[leisure=pitch][sport][!building](27.257681607028,-81.394100189209,27.312298756218,-81.329555511475);(._;>;);out;";
  std::cout << "\nURL:" << url_s << "\n";
#endif
  std::string result;
  CURL* curl;
  CURLcode res;
  curl_global_init(CURL_GLOBAL_DEFAULT);
  curl = curl_easy_init();
  if (curl) {
    //curl_easy_setopt(curl, CURLOPT_URL, "https://tcno.co/hello.txt");
    curl_easy_setopt(curl, CURLOPT_URL, url_s.c_str());
    // set auth
    curl_easy_setopt(curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
    curl_easy_setopt(curl, CURLOPT_USERPWD, authKey.c_str());
    // set client agent
    curl_easy_setopt(curl, CURLOPT_USERAGENT, "Mission-X");
    curl_easy_setopt(curl, CURLOPT_FOLLOWLOCATION, 1L);
    curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, my_write);
    curl_easy_setopt(curl, CURLOPT_WRITEDATA, &result);
    curl_easy_setopt(curl, CURLOPT_VERBOSE, 1L);

    // https://curl.haxx.se/docs/sslcerts.html
    curl_easy_setopt(curl, CURLOPT_SSL_VERIFYPEER, FALSE);
    //curl_easy_setopt(curl, CURLOPT_CAINFO, cacert);

    res = curl_easy_perform(curl);
    curl_easy_cleanup(curl);
    if (CURLE_OK != res) {
      std::cerr << "CURL error: " << res << '\n';
    }
    else
    {

#ifdef USE_JSON_NATIVE
      // add parse statement
      json js = json::parse(result);

      for (auto j : js)
      {
        std::cout << "size: " << j.size() << "\n";
        //for (auto it = j.begin(); it != j.end(); ++it)
        
        //for (auto j1 : j)
        
        //for (json::iterator j1 = j.begin(); j1 != j.end(); ++j1)
        for (auto &j1 : j.items())
        {
          if (j[j1.key()].is_object())
            std::cout << "Object: ";

          std::cout  << " key:" << j1.key() << " value: " << j1.value()  << "\n";


          if (j1.key().compare("encodedPolyline") == 0 )
          {
            int iWaypoints = 0;
            if (j.find("waypoints") != j.end())
              iWaypoints = j["waypoints"].get<int>();

            std::list <mxVec2f> listNavPoints;
            //float decodedCoords[][2];
            SimplePolyline smp;
            auto val = j1.value().get<std::string>();
            int size = smp.decode(val.c_str(), listNavPoints);

            std::cout << "\tDecoded: " << listNavPoints.size() << " waypoints\n";
          }


        }      




      }

      std::cout << js.dump(2) << "\n";
#else 

  #ifdef external_fpln
        // From demo, Example 12
        IXMLReaderStringSZ iReaderSz(result.c_str());;
        IJSONPullParser pp(&iReaderSz);
        IXMLDomParser iDom;
        auto x = iDom.parse(&pp).deepCopy();
        printf("  Write back the JSON file as an XML File.\n");
        IXMLRenderer().writeToFile(x, "json_output.xml");
        //printf("  Search inside the JSON file using XPATH:\n"
        //  "   Content of 'results/[2]/metadata/recent_retweets' is '%S'\n\n",
        //  x.getElementByPath(L"results/[2]/metadata/recent_retweets"));

        int child_a = x.nChildNode("a"); // "a" is the name of the array element, no root
        std::cout << "number of <a>: " << child_a << "\n";

        // try to guess is there is only one
        bool isJust_1_node = false;
        auto testNode = x.getChildNodeByPath("id");
        if (testNode.isEmpty())
          std::cout << "id not found\n";
        else
        {
          isJust_1_node = true;
          child_a = 1;
          std::cout << "text id: " << testNode.getText() << "\n";

        }
  #else if defined (overpass)
        bool isJust_1_node = false;
        IXMLDomParser iDom;
        auto xml = iDom.parseString(result.c_str());
        auto x = xml.deepCopy();
        int child_a = x.nChildNode("way");
        printf("> Write overpass result to XML File.\n");
        IXMLRenderer().writeToFile(x, "overpass_output.xml");
  #endif




        //for (int i1 = 0; i1 < child_a ; ++i1)
        //{

        //  IXMLNode a_node = (isJust_1_node)? x : x.getChildNode(i1);
        //  if (a_node.isEmpty() || std::string(a_node.getName()).compare("a") != 0)
        //  {
        //    continue;
        //  }

        //  std::string distance_s = a_node.getChildNode("distance").getText();
        //  std::string distance_s2 = (a_node.getChildNode("distance2").isEmpty())? "" : a_node.getChildNode("distance2").getText();

        //}


#endif 

        std::cout << "\n\n====== source =====\n" << result << "\n\n";

    }
  }
  curl_global_cleanup();
  //std::cout << result << "\n\n";
}