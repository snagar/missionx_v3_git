
1. Very simple animated image button
https://github.com/ocornut/imgui/issues/1167

So it will display different images: one image when mouse is over it and another image when it isn't. Probably I'd have to code a function for it I guess..

bool ImageAnimButton(ImTextureID user_texture_id, ImTextureID user_texture_id2, const ImVec2& size, const ImVec2& uv0, const ImVec2& uv1, const ImVec2& uv2, const ImVec2& uv3, int frame_padding, const ImVec4& bg_col, const ImVec4& tint_col)
    {
        ImGuiWindow* window = GetCurrentWindow();
        if (window->SkipItems)
            return false;

        ImGuiContext& g = *GImGui;
        const ImGuiStyle& style = g.Style;

        // Default to using texture ID as ID. User can still push string/integer prefixes.
        // We could hash the size/uv to create a unique ID but that would prevent the user from animating UV.
        PushID((void *)user_texture_id);
        const ImGuiID id = window->GetID("#image");
        PopID();

        const ImVec2 padding = (frame_padding >= 0) ? ImVec2((float)frame_padding, (float)frame_padding) : style.FramePadding;
        const ImRect bb(window->DC.CursorPos, window->DC.CursorPos + size + padding*2);
        const ImRect image_bb(window->DC.CursorPos + padding, window->DC.CursorPos + padding + size);
        ItemSize(bb);
        if (!ItemAdd(bb, &id))
            return false;

        bool hovered, held;
        bool pressed = ButtonBehavior(bb, id, &hovered, &held);

        // Render
        const ImU32 col = GetColorU32((hovered && held) ? ImGuiCol_ButtonActive : hovered ? ImGuiCol_ButtonHovered : ImGuiCol_Button);
        RenderFrame(bb.Min, bb.Max, col, true, ImClamp((float)ImMin(padding.x, padding.y), 0.0f, style.FrameRounding));
        if (bg_col.w > 0.0f)
            window->DrawList->AddRectFilled(image_bb.Min, image_bb.Max, GetColorU32(bg_col));
        if(hovered)
        {
            window->DrawList->AddImage(user_texture_id2, image_bb.Min, image_bb.Max, uv2, uv3, GetColorU32(tint_col));
        }
        else
        {
            window->DrawList->AddImage(user_texture_id, image_bb.Min, image_bb.Max, uv0, uv1, GetColorU32(tint_col));
        }

        return pressed;
    }



2. Animated Gifs
https://github.com/ocornut/imgui/issues/2680
For example you can use stb_image.h (https://github.com/nothings/stb/) to load animated gif,s using stbi__fopen+stbi__start_file+stbi__gif_load_nextapis.
Displaying a texture in dear imgui can be done with the ImGui::Image() or ImDrawList::AddImage() api, make sure to read the FAQ and all comments about ImTextureID.
It is tricky to provide an example within the imgui demo because (1) we don't want to embed an image loader in imgui and (2) we can't assume what's the renderer back-end is. Perhaps there would be room in the future for a separate repository providing more complete demos.


3. How to make the text behind the image button align to the image button?
https://github.com/ocornut/imgui/issues/2064
Before drawing the text, do
ImGui::SetCursorPos({ImGui::GetCursorPos().x, ImGui::GetCursorPos().y + (yourImageHeight - ImGui::GetFont()->FontSize) / 2})

Without C++11 you can 
ImGui::SetCursorPos(ImVec2(ImGui::GetCursorPos().x, ImGui::GetCursorPos().y + (yourImageHeight - ImGui::GetFont()->FontSize) / 2))


4. STD::ASYNC - C++

1. function: load the map file
2. Loading should be independent of the rest of the images (loop load)
3. 
  static std::mutex s_MeshMutex; // global
  // using std::future
  std::vector<std::future<void>> m_Futures  // we use future<void> since out function returns void.

  void func1()
  {
    for (a: mapImages)
    {
      mFutures.push_back( std::async (std::Launch::async, loadImage_async, &vecStaticImages, filepath ) )// loadMesh = function, mapStaticImages = holds loaded images, filepath: where image is being located.
    }
  }


  static void loadImage_async (std::vector<Ref<Mesh>>* vecStaticImages, std::string filepath) // use pointers not reference
  {
    auto mesh = Mesh::Load(filepath);

    std::lock_guard<std::mutex> lock(s_MeshMutex); // will automaticaly unlock at the end of this function
    vecStaticImages->push_back(mesh); // <- problem vector is not guarded. we need to lock the resource. Use mutex
  }


