// io_bench.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include "pch.h"
#include <cstdio>
#include <cctype>
#include <iostream>
#include <fstream>

#include <chrono>
#include <string>

using namespace std;

int main()
{
  const int N = 1024;

  char Buffer[N];
  std::ifstream infs;
  std::ofstream outFile;

  std::string customIniFile = "apt.dat"; 
  std::string customAptDat = "customAptDat.txt"; 


  std::ios_base::sync_with_stdio(false); // v3.0.219.10
  std::cin.tie(nullptr); // v3.0.219.10

  char const* oldLocale = setlocale(LC_ALL, "C"); // disable local parsing/conversion
  
  
  outFile.open(customAptDat.c_str()); // can we create/open the file ?
  if (outFile.fail())
  {
    cout<<((std::string("Fail to create file: ") + customAptDat + "\n"));
    return false;
  }

  infs.rdbuf()->pubsetbuf(Buffer, N);
  infs.open(customIniFile.c_str(), ios::in); // can we read the file
  if (!infs.is_open())
  {
    cout<< ((std::string("[parse Custom ini] Fail to open file: ") + customIniFile + "\n"));
    return false;
  }

  //// read each line and search for string "SCENERY_PACK ", the space in the end is important
  std::string line;
  char c = '\0';

  int readCounter = 0;
  int writeCounter = 0;
  std::string code;
  bool flag_AddLine = true;

  auto start = chrono::steady_clock::now();
  code.clear();
  while (infs.get(c))
  {

    if (c != ' ' && !std::isdigit(c) )
      continue;

    if (c == ' ' || c == '\0' || infs.eof())
    {
      // check code
      if ((code.compare("1300") == 0) || code.compare("1") == 0 || code.compare("100") == 0 || code.compare("16") == 0 || code.compare("17") == 0 || code.compare("101") == 0 || code.compare("102") == 0 || code.compare("56") == 0)
      {
        ++writeCounter;
        std::getline(infs, line); // read rest of line
        //line = code + line;
        outFile << code << line << "\n"; // write to output file
        //std::cout << line << "\n";
      }
      else
        std::getline(infs, line); // skip until end of line

      ++readCounter;
      code.clear();

    }
    else
      code += c;
 

  } // end while


  //while (getline(infs, line) )
  //{
  //  ++readCounter;
  //  int pos = 0;
  //  code.clear();



  //  if (line.empty())
  //    continue;

  //  if (!std::isdigit(line.at(0)))
  //    continue;


  //  //int spacePos = (int)line.find_first_of(' '); // , (size_t)pos); // not using Utils::splitStringToList because of the overhead. we just need first string value
  //  //if (spacePos == std::string::npos)
  //  //  continue;

  //  //code = line.substr(0, spacePos);

  //  if (pos == 0)
  //  {
  //    // read code
  //    for (auto chr : line)
  //    {
  //      if (std::isdigit(chr))
  //        code += chr;
  //      else if (chr == ' ' || chr == '\0' || infs.eof())
  //        break;
  //     
  //      ++pos;
  //    }
  //  }

  //  // check code
  //  if ((code.compare("1300") == 0) || code.compare("1") == 0 || code.compare("100") == 0 || code.compare("16") == 0 || code.compare("17") == 0 || code.compare("101") == 0 || code.compare("102") == 0 || code.compare("56") == 0)
  //  {
  //    ++writeCounter;
  //    std::cout << line << "\n";
  //  }
  //
  //} // end while loop

  auto end = chrono::steady_clock::now();
  auto diff = end - start;
  double duration = chrono::duration <double, milli>(diff).count();
  cout << "Duration: " <<  duration << "ms (" << (duration / 1000) << "sec)\n";
  cout << "Processed: " <<  readCounter << " of rows\n";
  cout << "Wrote: " << writeCounter << " of rows\n";


  //while ((getline(infs, line)) )
  //{


  //}

  if (outFile.is_open())
    outFile.close();

  if (infs.is_open())
    infs.close();



  exit(0);
}

// Run program: Ctrl + F5 or Debug > Start Without Debugging menu
// Debug program: F5 or Debug > Start Debugging menu

// Tips for Getting Started: 
//   1. Use the Solution Explorer window to add/manage files
//   2. Use the Team Explorer window to connect to source control
//   3. Use the Output window to see build output and other messages
//   4. Use the Error List window to view errors
//   5. Go to Project > Add New Item to create new code files, or Project > Add Existing Item to add existing code files to the project
//   6. In the future, to open this project again, go to File > Open > Project and select the .sln file
