#include <iostream>
#include <fstream>
#include <nlohmann/json.hpp>
#include <string>

const static struct _fplndb_json_keys_struct
{
  const std::string Z_KEY_id{ "id" };
  const std::string Z_KEY_popularity{ "popularity" }; // v3.0.253.3
  const std::string Z_KEY_distance{ "distance" };
  const std::string Z_KEY_encodedPolyline{ "encodedPolyline" };
  const std::string Z_KEY_flightNumber{ "flightNumber" };
  const std::string Z_KEY_fromICAO{ "fromICAO" };
  const std::string Z_KEY_fromName{ "fromName" };
  const std::string Z_KEY_maxAltitude{ "maxAltitude" };
  const std::string Z_KEY_notes{ "notes" };
  const std::string Z_KEY_toICAO{ "toICAO" };
  const std::string Z_KEY_toName{ "toName" };
  const std::string Z_KEY_waypoints{ "waypoints" };
} mx_fplndb_json_keys;

typedef struct _ext_internet_fpln_strct
{
  int         internal_id{ -1 };
  int         fpln_unique_id{ -1 }; // holds plan unique id from site
  std::string fromICAO_s{ "" };
  std::string toICAO_s{ "" };
  std::string fromName_s{ "" };
  std::string toName_s{ "" };
  std::string flightNumber_s{ "" };
  double      distnace_d{ 0.0 };

  int         maxAltitude_i{ 0 };
  std::string notes_s{ "" };

  std::string popularity_s{ "" }; // v3.0.253.3
  int         popularity_i{ 0 };  // v3.0.253.3

  int                                     waypoints_i{ 0 };
  std::string                             encode_polyline_s;

  std::string formated_nav_points_s{ "" };                    // holds the list of nav points but in formated string
  std::string formated_nav_points_with_guessed_names_s{ "" }; // v3.0.255.2 holds the list of nav points but in formated string and guessed wp names, not just coordinates


} mx_ext_internet_fpln_strct;

std::string jsonTextOneLine = R"(
[
  {
    "id": 7715626,
    "fromICAO": "LLBG",
    "toICAO": "VABB",
    "fromName": "Ben Gurion",
    "toName": "Mumbai Chhatrapati Shivaji Intl",
    "flightNumber": null,
    "distance": 2215.91252512761,
    "maxAltitude": 35000,
    "waypoints": 51,
    "likes": 0,
    "downloads": 1,
    "popularity": 1710946412,
    "notes": "Basic altitude profile:\n- Ascent Rate: 2500ft/min\n- Ascent Speed: 250kts\n- Cruise Altitude: 35000ft\n- Cruise Speed: 420kts\n- Descent Rate: 1500ft/min\n- Descent Speed: 250kts\n\nOptions:\n- Use NATs: yes\n- Use PACOTS: yes\n- Use low airways: yes\n- Use high airways: yes\n",
    "encodedPolyline": "{|jbE_h|sEvouA{hmJflMcxp@bvNgdt@jpxB__aHjvhAcqiDrtx@gboC~`iA{~sDvhoBog_Jr}g@{{nAzj_D_spHjep@grzArtZwudCrox@gglG~wNkffAjw]_vgCzp_@{hlCfjDs]_qEcdlBcGsf\\kMcsp@n~Lg|c@b}Mohf@~oMo{d@f{H_}XbcUk{cAbiRw}z@jsF_rVfkP_es@j|N_on@~`p@gz~Bn``@gqxAruM_ti@r_S{px@~tR{}~@feNk{o@ngKwfe@~lLcgyAf{MgifBnp]_adAnkXcinArtvA_aiFvq{@s~|Cnk{@spyCbu`@gtyAfueB_}mG~mq@gbkObcdEoa}Nz{pEcdaOnosCge_N~tzEgd|F",
    "createdAt": "2024-03-18T14:53:32.000Z",
    "updatedAt": "2024-03-18T14:53:32.000Z",
    "tags": [
      "generated"
    ],
    "user": null,
    "application": null,
    "cycle": {
      "id": 40,
      "ident": "FPD2106",
      "year": 21,
      "release": 6
    }
  }
]
)";

std::string jsonTextMultiLines = R"(
[
  {
    "id": 7715626,
    "fromICAO": "LLBG",
    "toICAO": "VABB",
    "fromName": "Ben Gurion",
    "toName": "Mumbai Chhatrapati Shivaji Intl",
    "flightNumber": null,
    "distance": 2215.91252512761,
    "maxAltitude": 35000,
    "waypoints": 51,
    "likes": 0,
    "downloads": 1,
    "popularity": 1710946412,
    "notes": "Basic altitude profile:\n- Ascent Rate: 2500ft/min\n- Ascent Speed: 250kts\n- Cruise Altitude: 35000ft\n- Cruise Speed: 420kts\n- Descent Rate: 1500ft/min\n- Descent Speed: 250kts\n\nOptions:\n- Use NATs: yes\n- Use PACOTS: yes\n- Use low airways: yes\n- Use high airways: yes\n",
    "encodedPolyline": "{|jbE_h|sEvouA{hmJflMcxp@bvNgdt@jpxB__aHjvhAcqiDrtx@gboC~`iA{~sDvhoBog_Jr}g@{{nAzj_D_spHjep@grzArtZwudCrox@gglG~wNkffAjw]_vgCzp_@{hlCfjDs]_qEcdlBcGsf\\kMcsp@n~Lg|c@b}Mohf@~oMo{d@f{H_}XbcUk{cAbiRw}z@jsF_rVfkP_es@j|N_on@~`p@gz~Bn``@gqxAruM_ti@r_S{px@~tR{}~@feNk{o@ngKwfe@~lLcgyAf{MgifBnp]_adAnkXcinArtvA_aiFvq{@s~|Cnk{@spyCbu`@gtyAfueB_}mG~mq@gbkObcdEoa}Nz{pEcdaOnosCge_N~tzEgd|F",
    "createdAt": "2024-03-18T14:53:32.000Z",
    "updatedAt": "2024-03-18T14:53:32.000Z",
    "tags": [
      "generated"
    ],
    "user": null,
    "application": null,
    "cycle": {
      "id": 40,
      "ident": "FPD2106",
      "year": 21,
      "release": 6
    }
  },
  {
    "id": 7714307,
    "fromICAO": "LLBG",
    "toICAO": "CYVR",
    "fromName": "Ben Gurion",
    "toName": "Vancouver",
    "flightNumber": null,
    "distance": 5883.3153298077,
    "maxAltitude": 35000,
    "waypoints": 127,
    "likes": 0,
    "downloads": 1,
    "popularity": 1710912202,
    "notes": "Basic altitude profile:\n- Ascent Rate: 2500ft/min\n- Ascent Speed: 250kts\n- Cruise Altitude: 35000ft\n- Cruise Speed: 420kts\n- Descent Rate: 1500ft/min\n- Descent Speed: 250kts\n\nOptions:\n- Use NATs: no\n- Use PACOTS: yes\n- Use low airways: yes\n- Use high airways: yes\n",
    "encodedPolyline": "{|jbE_h|sEc~zFrxmBk|cBvql@g{`C~tu@_tZrjK_wo@~iUs{mEn}aAgjSsXo{UvLwpiCkk@svaFrw`@stgFjja@km]bdCongCrvTw|aIr_cBoaqErmcA_vbCzzi@klbDftv@{exDnw_Aga{Araa@cue@bnMscaBzpd@{amCbym@_vcEn}aA{vlBvrZgpmCvyc@cwd@vhK_zgBnmW{plAfj]obdEvxiA{{qBj{o@cspEbm{As~yBzcw@cdnAjw]_hy@n{_@kenFv}jCs}aCjr`A{ud@biRo{ZzlM{aiEbakBcfuBjhtAgwdGbqgE{p{FjxtBk`lBfwpAoh{Br~wC{m^jve@{bf@nyo@onr@fi|A_r|AzrdB_w[nb_@gm^rjn@wgg@fr|@g`oAnz|B{brIbxlQgbqBfk}EsczB~a_GgfaDnwwSoioDba~J_seK~flW_ibE~|hQ_ibE~flW?~hbE_ibE~flW?~hbE?~hbE_ibE~flW?~hbE_ibE~po]?~hbE?~hbE?~hbE?~hbE?~hbE?~hbE?~hbE?~hbE?~hbE?~hbE?~hbE?~hbE?~hbE?~hbE?~hbE?~hbE?~hbE?~hbE?~hbE?~hbE?~hbE?~hbE?~hbE?~hbE?~x|u@?~hbE?~hbE?~hbE?~hbE?~hbE?~hbE?~hbE?~hbE?~hbE?~hbE?~hbE?~hbE?~hbE?~hbE?~hbE?~hbE?~hbE?~hbE?~hbE?~hbE?~hbE?~hbE~v`@nyzGzwbHndxwAviuJ~b`|@jizS~b`|@~{mZ~po]bk_\\~ybVrgcT~uzKzcqH~ieBv|fDnvs@vj{Hvp|AzekHnqtBnceHnzoA",
    "createdAt": "2024-03-18T05:23:22.000Z",
    "updatedAt": "2024-03-18T05:23:22.000Z",
    "tags": [
      "generated"
    ],
    "user": null,
    "application": null,
    "cycle": {
      "id": 40,
      "ident": "FPD2106",
      "year": 21,
      "release": 6
    }
  }
]
)"; 

// -------------------------------------------------

std::string
getJsonValue(nlohmann::json js, std::string key, std::string outDefaultValue)
{
  bool bFoundKey = false;
  if (!js.is_discarded() && js.contains(key))
  {
    bFoundKey = true;
    if (js[key].is_null())
      return outDefaultValue;

    if (js[key].is_string())
      return js[key].get<std::string>();
  }

  return outDefaultValue;
}

// -------------------------------------------------

template< typename T>
T
getJsonValue(nlohmann::json js, std::string key, nlohmann::detail::value_t inType, T outDefaultValue)
{
  bool bFoundKey = false;
  if (!js.is_discarded() && js.contains(key))
  {
    bFoundKey = true;
    if (js[key].is_null())
      return outDefaultValue;

    //  null,              ///< null value
    //  object,          ///< object (unordered set of name/value pairs)
    //  array,           ///< array (ordered collection of values)
    //  string,          ///< string value
    //  boolean,         ///< boolean value
    //  number_integer,  ///< number value (signed integer)
    //  number_unsigned, ///< number value (unsigned integer)
    //  number_float,    ///< number value (floating-point)
    //  binary,          ///< binary array (ordered collection of bytes)
    //  discarded        ///< discarded by the parser callback function*/

    switch (inType)
    {
      case nlohmann::detail::value_t::null:
      {
        return outDefaultValue;       
      }
      break;
      case nlohmann::detail::value_t::object:
      {
        if (js[key].is_object())
          return js[key];
      }
      break;
      case nlohmann::detail::value_t::array:
      {
        if (js[key].is_array())
          return js[key];
      }
      break;
      case nlohmann::detail::value_t::boolean:
      {
        if (js[key].is_boolean())
          return (T)js[key].get<bool>();
      }
      break;
      case nlohmann::detail::value_t::number_integer:
      case nlohmann::detail::value_t::number_unsigned:
      case nlohmann::detail::value_t::number_float:
      {
        if (js[key].is_number())
          return (T)js[key].get<double>();
      }
      break;

    }

  }

  return outDefaultValue;
}




// -------------------------------------------------

void
testSimpleJsonCases()
{
   std::string cURL_s = R"({"METAR":"KLAX 171953Z 24008KT 10SM FEW090 FEW180 SCT220 BKN270 18/11 A3006 RMK AO2 SLP179 T01780111 $","TAF":"TAF AMD KLAX 171934Z 1720/1824 09006KT P6SM SCT150 FM172030 26012KT P6SM SCT200 FM180300 26006KT P6SM BKN200 FM180800 VRB03KT P6SM BKN200 FM181200 VRB03KT P6SM BKN015 FM181600 VRB03KT P6SM SKC"})";
  // std::string cURL_s = R"({"METAR":})";
  // nlohmann::json js    = nlohmann::json::parse(cURL_s, nullptr, false);
  try
  {
    nlohmann::json js = nlohmann::json::parse(cURL_s, nullptr, true);
    if (js.is_null())
      std::cout << "Json is NULL.\n";
    if (js.empty())
      std::cout << "Json is empty.\n";



    // if (js.contains("METAR"))
    //   std::cout << "METAR:\n" << js["METAR"] << "\n";
  }
  catch (nlohmann::json::parse_error& ex)
  {
    std::cerr << "parse error at byte " << ex.byte << std::endl;
    std::cout << "parse error at byte" << ex.byte << ". Reason: " << ex.what() << '\n';
  }
}

// -------------------------------------------------

void
parseJsonArray(std::string inJsonText)
{
  try
  {
    nlohmann::json jSon = nlohmann::json::parse(inJsonText, nullptr, true);
    // loop over all lines
    for (auto& js : jSon)
    {
      mx_ext_internet_fpln_strct fpln;
      std::cout << "JSON Line:\n" << js.dump() << "\n\n";

      fpln.distnace_d = getJsonValue<double>(js, mx_fplndb_json_keys.Z_KEY_distance, nlohmann::detail::value_t::number_float, 0.0);
      fpln.encode_polyline_s = getJsonValue(js, mx_fplndb_json_keys.Z_KEY_encodedPolyline, "");
      fpln.flightNumber_s    = getJsonValue(js, mx_fplndb_json_keys.Z_KEY_flightNumber, "");
      fpln.fromICAO_s        = getJsonValue(js, mx_fplndb_json_keys.Z_KEY_fromICAO, "");
      fpln.fromName_s        = getJsonValue(js, mx_fplndb_json_keys.Z_KEY_fromName, "");

      std::cout << "Z_KEY_distance:" << fpln.distnace_d << "\n";
      std::cout << "Z_KEY_encodedPolyline:" << fpln.encode_polyline_s << "\n";
      std::cout << "Z_KEY_flightNumber:" << fpln.flightNumber_s << "\n";
      std::cout << "Z_KEY_fromICAO:" << fpln.fromICAO_s << "\n";
      std::cout << "Z_KEY_fromName:" << fpln.fromName_s << "\n";
      std::cout << "\n\n";
    }
  }
  catch (nlohmann::json::parse_error& ex)
  {
    //std::cerr << "parse error at byte " << ex.byte << std::endl;
    std::cout << "parse error at byte" << ex.byte << ". Reason: " << ex.what() << '\n';
  }
}

int main()
{
  testSimpleJsonCases();

  parseJsonArray(jsonTextMultiLines);

  int i = 0; // dummy
}

