#include <array>
#include <cctype>
#include <chrono>
#include <fstream>
#include <iostream>
#include <map>
#include <sstream>
#include <string>
#include <algorithm>

//bool
//is_number(const std::string& str)
//{
//  return !str.empty() && std::all_of(str.begin(), str.end(), ::isdigit);
//}

bool
is_number(const std::string& s)
{
  bool validNumber = true;

  bool minusPlusNotAllowed = false; // as long as we did not found digits, we can use "+/-" signs

  bool foundDecimalDot = false;

  std::string::const_iterator it = s.begin();
  while (it != s.end() && validNumber)
  {
    // number is legal if starts with "+/-" signs
    if (!minusPlusNotAllowed && (((*it) == '+') || ((*it) == '-')))
    {
      minusPlusNotAllowed = true; // only one sign is allowed at the begining
      ++it;
    }
    else if ((*it) == '.' && !foundDecimalDot)
    {
      foundDecimalDot     = true;
      minusPlusNotAllowed = true;
      ++it;
    }
    else if (::isdigit(*it))
    {
      minusPlusNotAllowed = true;
      ++it;
    }
    else
      validNumber = false;
  } // end while

  return !s.empty() && it == s.end();
}

void
parse_max_weight_line(const std::string& line, std::map<int, float>& mapMaxWeight, std::string& v_station_max_number)
{
  std::istringstream iss(line);
  //std::string        tokens[10];
  std::vector<std::string> vec_tokens(10);
  int                tokenCount = 0;

  std::string sTmp;

  while (iss >> sTmp && tokenCount < 10)
  {
    vec_tokens[tokenCount] = sTmp;
    //tokens[tokenCount] = sTmp;
    tokenCount++;
  }

  //std::string lastToken = tokens[tokenCount - 1];
  std::string lastToken = vec_tokens.back();
  if (is_number(lastToken))
  {
    float v_max_station_weight = std::stof(lastToken);

    auto pos = vec_tokens.at(1).find_last_of('/');
    if (pos != std::string::npos)
    {
      std::string v_station_index_str = vec_tokens.at(1).substr(pos + 1);
      if (is_number(v_station_index_str))
      {
        int v_station_index           = std::stoi(v_station_index_str);
        mapMaxWeight[v_station_index] = v_max_station_weight;
      }
      else if (v_station_index_str == "count")
      {
        v_station_max_number = v_station_index_str;
      }
      
    }
  }
}

void
parse_station_name_line(const std::string& line, std::map<int, std::string>& mapStationNames)
{
  std::istringstream iss(line);
  std::string        tokens[10];
  int                tokenCount = 0;

  while (iss >> tokens[tokenCount] && tokenCount < 10)
    tokenCount++;

  auto pos = tokens[1].find_last_of('/');
  if (pos != std::string::npos)
  {
    std::string v_station_index_str = tokens[1].substr(pos + 1);
    if (is_number(v_station_index_str))
    {
      int v_station_index = std::stoi(v_station_index_str);

      std::string v_station_name;
      for (int i = 2; i < tokenCount; ++i)
      {
        if (!v_station_name.empty())
          v_station_name += " ";
        v_station_name += tokens[i];
      }
      mapStationNames[v_station_index] = v_station_name;
    }
  }
}

int
main()
{
  std::string                v_plane_file_name = "Cessna_172SP.acf";
  std::array<std::string, 2> arr_text          = { "P acf/_fixed_max", "P acf/_fixed_name" };

  std::map<int, float>       mapMaxWeight;
  std::map<int, std::string> mapStationNames;
  std::string                v_station_max_number;

  std::ifstream file(v_plane_file_name);
  if (!file)
  {
    std::cerr << "Error: Could not open file " << v_plane_file_name << "\n";
    return 1;
  }

  // Start timing
  auto start = std::chrono::high_resolution_clock::now();

  std::string line;
  char        ch;
  int         char_counter_i = 0;
  while (file.get(ch))
  {
    if (ch == '\n')
    {
      for (const auto& target : arr_text)
      {
        if (line.starts_with(target))
        {
          if (target == "P acf/_fixed_max")
          {
            parse_max_weight_line(line, mapMaxWeight, v_station_max_number);
          }
          else if (target == "P acf/_fixed_name")
          {
            parse_station_name_line(line, mapStationNames);
          }
          break;
        }
      }

      if (ch != '\n')
        std::getline(file, line); // skip to the next line

      line.clear();
      char_counter_i = 0;
    }
    else
    {
      line += ch;
      ++char_counter_i;
    }
  }

  // End timing
  auto end      = std::chrono::high_resolution_clock::now();
  auto duration = std::chrono::duration_cast<std::chrono::milliseconds>(end - start);

  // Print results
  std::cout << "Max Weights:\n";
  for (const auto& [index, weight] : mapMaxWeight)
    std::cout << "Station " << index << ": " << weight << "\n";

  std::cout << "\nStation Names:\n";
  for (const auto& [index, name] : mapStationNames)
    std::cout << "Station " << index << ": " << name << "\n";

  std::cout << "\nExecution Time: " << duration.count() << " ms (" << duration.count() / 1000.0 << " s)\n";

  return 0;
}
