// sqlitec++.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <string>
#include <sqlite3.h> 
#include <map>
#include <unordered_map>
#include <assert.h>


std::map <std::string, std::string> row;
std::unordered_map <int, std::map <std::string, std::string> > reasultTable;

static int callback(void* data, int argc, char** argv, char** azColName) {
  int i;
  fprintf(stderr, "%s: ", (const char*)data);

  for (i = 0; i < argc; i++) {
    printf("%s = %s\n", azColName[i], argv[i] ? argv[i] : "NULL");

    row[azColName[i]] = argv[i] ? argv[i] : "";
  }

  reasultTable[reasultTable.size()] = row;
  printf("\n");
  return 0;
}


void prepare_sqlite_db_tables(sqlite3* inDB_ptr)
{

  assert(inDB_ptr != nullptr && "Database pointer is empty");
  char* error_msg{};
  //if (isInMemory)
  //{
  //  inDB_ptr->set_db_path_and_file(inDB_ptr->get_db_path());
  //  if (inDB_ptr->db_is_open_and_ready)
  //  {
  //    inDB_ptr->close_database();
  //  }

  //  inDB_ptr->open_database(); // we always try to open the database
  //} // end if inMemory type database

  //if (inDB_ptr->db_is_open_and_ready) // status. 0 = success/OK
  {
    // auto commit since we are not in transaction mode
    //inDB_ptr->execute_stmt("drop table if exists xp_airports");
    //inDB_ptr->execute_stmt("drop table if exists xp_ap_metadata");
    //inDB_ptr->execute_stmt("drop table if exists xp_ap_ramps");
    //inDB_ptr->execute_stmt("drop table if exists xp_helipads");
    //inDB_ptr->execute_stmt("drop table if exists xp_loc");
    //inDB_ptr->execute_stmt("drop table if exists xp_rw");
    //inDB_ptr->execute_stmt("drop view if exists airports_vu");

    sqlite3_exec(inDB_ptr, "create table if not exists xp_airports ( icao_id int, icao text, ap_elev integer, ap_name text, ap_type int, is_custom int, ap_lat real, ap_lon real, primary key (icao_id) )", 0, 0, &error_msg);
    sqlite3_exec(inDB_ptr, "create table if not exists xp_ap_metadata ( icao_id int, icao text NOT NULL, key_col text NOT NULL, val_col text NULL ) ", 0, 0, &error_msg);
    sqlite3_exec(inDB_ptr, "create table if not exists xp_ap_ramps ( icao_id int, icao text NOT NULL, ramp_lat real NOT NULL, ramp_lon real NOT NULL, ramp_heading_true NULL, location_type text NULL, for_planes text NULL, ramp_uq_name text NULL)", 0, 0, &error_msg);
    sqlite3_exec(inDB_ptr, "create table if not exists xp_rw ( icao_id int,  icao text NOT NULL, rw_width   real NULL, rw_surf integer NULL, rw_sholder integer NULL, rw_smooth  real NULL, rw_no_1    text NOT NULL, rw_no_1_lat real not null, rw_no_1_lon real not null, rw_no_1_disp_hold real null, rw_no_2    text NOT NULL, rw_no_2_lat real not null, rw_no_2_lon real not null, rw_no_2_disp_hold real null, rw_length_mt integer null )", 0, 0, &error_msg);
    sqlite3_exec(inDB_ptr, "create table if not exists xp_helipads ( icao_id int,  icao text NOT NULL, name text NULL, lat    real NOT NULL, lon    real NOT NULL, length real NULL,  width  real NULL ) ", 0, 0, &error_msg);
    sqlite3_exec(inDB_ptr, "create table if not exists xp_loc ( lat real NOT NULL, lon real NOT NULL, ap_elev_ft integer, frq_mhz integer, max_reception_range integer, loc_bearing real, ident text, icao text, icao_region_code text, loc_rw text, loc_type text ) ", 0, 0, &error_msg);

    // main view to fetch basic data on airports and their ramps
    sqlite3_exec(inDB_ptr,R"(CREATE VIEW airports_vu as WITH helipads_view as (select icao_id, count(1) as helipad_counter from xp_helipads group by icao_id),
     heli_ramps_view as (select icao_id, count(1) as ramp_helis from xp_ap_ramps where for_planes like '%helos%' group by icao_id),
     plane_ramps_view as (select icao_id, count(1) as ramp_planes from xp_ap_ramps where for_planes <> 'helos' group by icao_id),
     props_ramps_view as (select icao_id, count(1) as ramp_planes from xp_ap_ramps where for_planes like '%props%' group by icao_id),
     turboprop_ramps_view as (select icao_id, count(1) as ramp_planes from xp_ap_ramps where for_planes like '%turboprops%' group by icao_id),
     jets_heavy_ramps_view as (select icao_id, count(1) as ramp_planes from xp_ap_ramps where for_planes like '%jets%' or for_planes like '%heavy%' group by icao_id),
     rw_hard_vu as (select icao_id, count(1) as rw_hard from xp_rw where rw_surf in (1,2) group by icao_id),
     rw_dirt_gravel_vu as (select icao_id, count(1) as rw_dirt_n_gravel from xp_rw where rw_surf in (4,5) group by icao_id),
     rw_grass_vu as (select icao_id, count(1) as rw_grass from xp_rw where rw_surf = 3 group by icao_id),     
     rw_water_vu as (select icao_id, count(1) as rw_water from xp_rw where rw_surf = 13 group by icao_id)     
SELECT t1.icao_id,
       t1.icao,
       t1.ap_elev,
       t1.ap_name,
       t1.ap_type,
       t1.is_custom,
       t1.ap_lat,
       t1.ap_lon
       ,IFNULL((select helipad_counter from helipads_view v1 where t1.icao_id = v1.icao_id), 0) as helipads
       ,IFNULL((select ramp_helis from heli_ramps_view v1 where t1.icao_id = v1.icao_id ), 0) as ramp_helos
       ,IFNULL((select ramp_planes from plane_ramps_view v1 where t1.icao_id = v1.icao_id ), 0) as ramp_planes       
       ,IFNULL((select ramp_planes from props_ramps_view v1 where t1.icao_id = v1.icao_id ), 0) as ramp_props       
       ,IFNULL((select ramp_planes from turboprop_ramps_view v1 where t1.icao_id = v1.icao_id ), 0) as ramp_turboprops       
       ,IFNULL((select ramp_planes from jets_heavy_ramps_view v1 where t1.icao_id = v1.icao_id ), 0) as ramp_jet_heavy       
       ,IFNULL((select rw_hard from rw_hard_vu v1 where t1.icao_id = v1.icao_id ), 0) as rw_hard           
       ,IFNULL((select rw_dirt_n_gravel from rw_dirt_gravel_vu v1 where t1.icao_id = v1.icao_id ), 0) as rw_dirt_gravel           
       ,IFNULL((select rw_grass from rw_grass_vu v1 where t1.icao_id = v1.icao_id ), 0) as rw_grass           
       ,IFNULL((select rw_water from rw_water_vu v1 where t1.icao_id = v1.icao_id ), 0) as rw_water           
  FROM xp_airports t1)", 0, 0, &error_msg);



    ////inDB_ptr->execute_stmt("CREATE INDEX icao_id_airport_n1 on xp_airports (icao_id)");
    //inDB_ptr->execute_stmt("CREATE UNIQUE INDEX icao_apName_n1 on xp_airports (icao, ap_name)");
    ////inDB_ptr->execute_stmt("CREATE UNIQUE INDEX uq_icao_lat_lon_n1 on xp_airports (icao, trunc(ap_lat), trunc(ap_lon))");
    //inDB_ptr->execute_stmt("CREATE INDEX ap_type_n2 on xp_airports (ap_type)");
    //inDB_ptr->execute_stmt("CREATE INDEX icao_id_helipad_n1 on xp_helipads (icao_id)");
    //inDB_ptr->execute_stmt("CREATE INDEX icao_id_rw_n1 on xp_rw (icao_id)");
    //inDB_ptr->execute_stmt("CREATE INDEX surf_type_rw_n2 on xp_rw (rw_surf)");

    //// the following indexes are optional
    //inDB_ptr->execute_stmt("CREATE INDEX icao_helipad_n2 on xp_helipads (icao)");
    //inDB_ptr->execute_stmt("CREATE INDEX icao_rw_n2 on xp_rw (icao)");

  }


}




int main()
{
  

  sqlite3* memdb;
  sqlite3* db;
  char* zErrMsg = 0;
  int rc;
  std::string sql;
  const char* data = "Callback function called";

  // Open memory database 
  //const std::string memdb_name = "file::memory:?cache=shared";
  const std::string memdb_name = "d:\\temp\\test_vu.sqlite.db";
  rc = sqlite3_open(memdb_name.c_str(), &memdb);
  if (rc) {
    fprintf(stderr, "Can't open :memory: database: %s\n", sqlite3_errmsg(memdb));
    return(0);
  }
  else {
    fprintf(stderr, "Opened :memory: database successfully\n");
  }

  // debug prepare statment


  prepare_sqlite_db_tables(memdb);

  return 0;

  /* Open database */
  const std::string external_file_db = "d:\\xp11clean\\Resources\\plugins\\missionx\\db\\airports_xp.sqlite.db";
  rc = sqlite3_open(external_file_db.c_str(), &db);

  if (rc) {
    fprintf(stderr, "Can't open database: %s\n", sqlite3_errmsg(db));
    return(0);
  }
  else {
    fprintf(stderr, "Opened database successfully\n");
  }

  /* Create SQL statement */
  //sql = "SELECT * from xp_airports";

  const std::string attach_name = "inmem";

  sql = "ATTACH DATABASE '" + external_file_db + "' as " + attach_name;
  if (sqlite3_exec (memdb, sql.c_str(), 0, 0, &zErrMsg) == SQLITE_OK )
  {

    sql = "create table xp_airports as select * from inmem.xp_airports";
    /* Execute SQL statement */
    rc = sqlite3_exec(memdb, sql.c_str(), callback, (void*)data, &zErrMsg);
    if (rc != SQLITE_OK) {
      fprintf(stderr, "SQL error: %s\n", zErrMsg);
      sqlite3_free(zErrMsg);
    }
    else {
      fprintf(stdout, "Operation done successfully\n");
    }

    sql = "DETACH DATABASE '" + attach_name + "'";
    sqlite3_exec(memdb, sql.c_str(), 0, 0, &zErrMsg);

    // test in memory data
    sql = "delete from xp_airports where icao_id < 100;  select count(1) as counter from xp_airports";
    
    rc = sqlite3_exec(memdb, sql.c_str(), callback, (void*)data, &zErrMsg);
    if (rc != SQLITE_OK) {
      fprintf(stderr, "SQL error: %s\n", zErrMsg);
      sqlite3_free(zErrMsg);
    }
    else {
      fprintf(stdout, "fetch data from :memory: db\n");
    }


  }

  sqlite3_close(db);
  sqlite3_close(memdb);
  return 0;

}

// Run program: Ctrl + F5 or Debug > Start Without Debugging menu
// Debug program: F5 or Debug > Start Debugging menu

// Tips for Getting Started: 
//   1. Use the Solution Explorer window to add/manage files
//   2. Use the Team Explorer window to connect to source control
//   3. Use the Output window to see build output and other messages
//   4. Use the Error List window to view errors
//   5. Go to Project > Add New Item to create new code files, or Project > Add Existing Item to add existing code files to the project
//   6. In the future, to open this project again, go to File > Open > Project and select the .sln file
