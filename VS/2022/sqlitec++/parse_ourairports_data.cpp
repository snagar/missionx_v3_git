// sqlitec++.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include <sstream>
#include <iostream>
#include <fstream>
#include <stdio.h>
#include <stdlib.h>
#include <string>
#include <sqlite3.h> 
#include <map>
#include <unordered_map>
#include <chrono>

std::map <std::string, std::string> row;
std::unordered_map <int, std::map <std::string, std::string> > reasultTable;

// https://ourairports.com/

bool db_is_open_and_ready{ false };


const std::string remove_char_from_string(const std::string token, const char c_to_remove)
{
  if (token.find(c_to_remove) != std::string::npos)
  {
    std::string new_token;
    for (const auto c : token)
    {
      if (c == c_to_remove)
        continue;

      new_token.push_back(c);
    }

    return new_token;
  }

  return token;
}

/// <summary>
/// Split delimete line to use in sql statement.
/// Since we need exact number of field , the function wont skip empty delimete values and will add "empty" value to the vector.
/// At the end of the function we will add empty fields to the vector according to the missing fields - if any. Useful when last value is empty.
/// </summary>
/// <param name="s">The string to be splitted</param>
/// <param name="delimiter">delimate character</param>
/// <param name="numOfFields">How many fields must be in the vector</param>
/// <returns>vector of string values</returns>
std::vector<std::string> mxsplit_db_fields(const std::string& s, char delimiter, const int numOfFields)
{
  int d;
  std::vector<std::string> tokens;
  std::string token;
  std::istringstream tokenStream(s);

  while (std::getline(tokenStream, token, delimiter))
  {
    //if (token.empty()) // should be null
    //{
    //  //continue; 
    //  token = "''";
    //}

    char c0{'\0'}, c1{'\0'};
    if (token.find("\"") != std::string::npos)
    {
      d = 1;
      c0 = (*(token.cbegin() ));
      c1 = (*(token.cend() - 1));

      //if (c0 == '"')
      //  std::cout << "starts with \"";
    }

    if (token.find("64D") != std::string::npos)
    {
      d = 1;
    }

    if (!token.empty() && token.size() > 1 && (*(token.cbegin())) == '"' && (*(token.cend()-1)) != '"' ) // if we have token that starts with '"' but does not end with '"'
    {
      std::string t1;
      while (std::getline(tokenStream, t1, delimiter)) // find next '"'
      {
        token += t1;
        if (!t1.empty() && (*(t1.cend() - 1)) == '"')
        {
          d = 1;
          break; // exit while loop
        }

      }
    }

    // remove all '"'
    token = remove_char_from_string(token, '"');
    //if (token.find('"')!=std::string::npos)
    //{
    //  std::string new_token;
    //  for (const auto c : token)
    //  {
    //    if (c == '"')
    //      continue;
    //
    //    new_token.push_back(c);
    //  }
    //
    //  token = new_token;
    //}


    tokens.push_back(token);
  }

  if (numOfFields > (int)tokens.size())
  {
    for (int i = (int)tokens.size(); i < numOfFields; ++i)
      tokens.push_back("");
  }
  else if (numOfFields < (int)tokens.size() )
  {
    std::cout << "to many fields splitted " << '\n';
  }


  return tokens;
}


bool start_transaction(sqlite3* db)
{
  char* zErr; // error message

  std::string last_err;
  auto rc = sqlite3_exec(db, "BEGIN TRANSACTION", NULL, NULL, &zErr);
  if (rc == SQLITE_OK)
    return true;

  last_err = std::string("[DB] Error while begin transaction: ") + sqlite3_errmsg(db) + "\n";
  std::cout << last_err << '\n';
  return false;
}

bool end_transaction(sqlite3* db)
{
  char* zErr; // error message

  std::string last_err;
  auto rc = sqlite3_exec(db, "END TRANSACTION", NULL, NULL, &zErr);
  if (rc == SQLITE_OK)
    return true;

  last_err = std::string("[DB] Error while begin transaction: ") + sqlite3_errmsg(db) + "\n";
  std::cout << last_err << '\n';
  return false;
}

bool execute_stmt(sqlite3* db, const char* inStmt)
{
  std::string last_err;

  if (!db_is_open_and_ready)
  {    
    return false;
  }

  char* error_msg{};

  auto rc = sqlite3_exec(db, inStmt, 0, 0, &error_msg);
  if (rc != SQLITE_OK) {
    last_err = std::string("[DBASE - ERROR] STMT Error: ") + sqlite3_errmsg(db) + "\n" + inStmt + "\n";
    std::cout << last_err << '\n';
    return false;
  }

  return true;
}



///////////////////////////////////////
///////////////////////////////////////
///////////////////////////////////////
///////////////////////////////////////
///////////////////////////////////////

int main()
{
  double duration = 0.0;
  unsigned int lineCounter = 0; // count the lines written to file
  auto start = std::chrono::steady_clock::now();

  const std::map<int, std::string> map_order_of_fields = { {1, "id"}, {2,"ident"},{3, "type"},{4, "name"},{5, "latitude_deg"},{6, "longitude_deg"},{7, "elevation_ft"},{8, "continent"},{9, "iso_country"},{11, "iso_region"},{12, "municipality"},{13, "scheduled_service"},{14, "gps_code"},{15, "iata_code"},{16, "local_code"},{17, "home_link"},{18, "wikipedia_link"},{19, "keywords"} };
  std::map<int, std::string> map_fields_type           = { {1, "int"}, {2,"text"},{3, "text"},{4, "text"},{5, "real"}        ,{6, "real"}         ,{7, "int"}         ,{8, "text"}     ,{9, "text"}       ,{11, "text"}      ,{12, "text"}        ,{13, "text"}             ,{14, "text"}    ,{15, "text"}     ,{16, "text"}      ,{17, "text"}     ,{18, "text"}          ,{19, "text"} };

  auto const lmbda_get_fields = [](std::map<int, std::string> inMap) {
    std::string s;
    bool is_first = true;
    int counter = 0;
    for (auto& m : inMap)
    {
      s += m.second + ","; // ((is_first) ? "" : ",");
      is_first = false;
      counter++;
    }
    if (counter > 0)
      s.pop_back();

    return s;
  };


  auto const lmbda_create_table_from_map = [&]() {
    std::string s;
    bool is_first = true;
    int counter = 0;
    for (const auto& m : map_order_of_fields)
    {
      //s += m.second + " text" + ",";
      s += m.second + " " + map_fields_type[m.first] + ",";
      is_first = false;
      counter++;
    }
    if (counter > 0)
      s.pop_back(); // remove last ","

    return s;
  };

  const std::string insert_header_s = "insert into airport_csv ( " + lmbda_get_fields(map_order_of_fields) + " ) ";
  const std::string airport_csv_stmt = "drop table if exists airport_csv; create table airport_csv ( " + lmbda_create_table_from_map() + " )"; 

#ifndef RELEASE
  std::cout << insert_header_s << '\n' << airport_csv_stmt << '\n';
#endif // !RELEASE


  std::ifstream file_aptDat;


  sqlite3* db;
  char* zErrMsg = 0;
  int rc;
  std::string sql;
  const char* data = "Callback function";

  /* Create SQL statement */
  //sql = "SELECT * from xp_airports";
  std::string db_file = "D:/temp/airports/ourairports.sqlite.db";
  std::string airports_csv = "D:/temp/airports/airports.csv";

  /* Open database */
  rc = sqlite3_open(db_file.c_str(), &db);

  if (rc) {
    fprintf(stderr, "Can't open database: %s\n", sqlite3_errmsg(db));
    return(0);
  }
  else {
    db_is_open_and_ready = true;
    fprintf(stderr, "Opened database successfully\n");

    execute_stmt(db, airport_csv_stmt.c_str());
  }

  std::ios_base::sync_with_stdio(false); // v3.0.219.10
  std::cin.tie(nullptr); // v3.0.219.10
  file_aptDat.open(airports_csv.c_str(), std::ios::in); // can we read the file
  if (!file_aptDat.is_open())
  {
    std::cout << "[parse file] Fail to open file: " << airports_csv << '\n';
    return 0;
  }
  else
  {
    if (start_transaction(db))
    {


      std::string line;
      std::getline(file_aptDat, line);
      line.clear(); // skip first line
      int limiter = 0;
      //while (file_aptDat.get(c) )
      while (std::getline(file_aptDat, line) && limiter > -1)
      {
        //if (c == '\n')
        {
          const std::vector<std::string> vecSplit = mxsplit_db_fields(line, ',', (int)map_order_of_fields.size());
          if (vecSplit.size() >= map_order_of_fields.size())
          {
            std::string values;
            const int size_i = (int)vecSplit.size();
            int counter = 1;
            //for (auto& v : vecSplit)
            for (int i=0; i<(int)map_order_of_fields.size(); ++i)
            {
              const auto v = vecSplit[i];
              values += ((counter > 1) ? "," : "") + ((v.empty())? "null" : std::string("\"") + v + "\""); // insert null if value is empty, and add "," only if we are not in first field
              counter++;
            }

            const std::string sql = insert_header_s + "values ( " + values + ")";
            execute_stmt(db, sql.c_str());
          }
          else
          {
            std::cout << "( " << limiter << " ) Wrong number of fields between vector and map.\n";
          }

//#ifndef RELEASE
//          std::cout << sql << '\n';
//#endif // !RELEASE

          line.clear();
          limiter++;
        }


//#ifndef RELEASE
//        if (limiter > 1000)
//          limiter *= -1; // exit loop
//#endif // !RELEASE


      }

      std::cout << "Proccessed: " << limiter << " lines.\n\n";
    }

    end_transaction(db);
  }

  sqlite3_close(db);


  auto end = std::chrono::steady_clock::now();
  auto diff = end - start;
  duration = std::chrono::duration <double, std::milli>(diff).count();
  std::cout << "Duration: " << duration << "ms (" << (duration / 1000) << "sec)\n";


  return 0;

}

// Run program: Ctrl + F5 or Debug > Start Without Debugging menu
// Debug program: F5 or Debug > Start Debugging menu
