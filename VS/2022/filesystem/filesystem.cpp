// filesystem.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include <iostream>
#include <fstream>
#include <string>
#include <filesystem>
#include <map>
#include <set>
#include <list>
#include <cctype>
#include <fstream>

namespace fs = std::filesystem;
const static std::string SCENERY_PACK_ = "SCENERY_PACK ";


std::string ltrim(std::string str, const std::string chars = std::string("\t\n\v\f\r "))
{
  // trim leading spaces
  size_t startpos = str.find_first_not_of(chars);//(" \t");
  if (std::string::npos != startpos)
  {
    str = str.substr(startpos);
  }

  return str;
} // ltrim

  /* *************************************************************** */
std::string rtrim(std::string str, const std::string chars = std::string("\t\n\v\f\r "))
{
  str.erase(str.find_last_not_of(chars) + 1);
  return str;
}

/* *************************************************************** */
std::string trim(std::string str, const std::string chars = std::string("\t\n\v\f\r "))
{
  return ltrim(rtrim(str, chars), chars);
}


/* *************************************************************** */

int
countCharsInString(const std::string& inText, const char & inCharToCount)
{
  int counter = 0;
  for (const auto& c : inText)
    counter += (c == inCharToCount) ? 1 : 0;

  return counter;
}

/* *************************************************************** */

std::vector<std::string>
split(const std::string& text, const std::string delimeter = " \t\n\r\f")
{
  std::vector<std::string> vecTokens;
  std::string              token;
  for (const auto &c : text)
  {
    if (delimeter.find(c) != std::string::npos)
    {
      if (false == token.empty()) // token is not empty
      {
        vecTokens.push_back(token);
        token.clear();
      }       
    }
    else 
    {
      token.push_back(c);
    }
  }

  if (false == token.empty())
    vecTokens.push_back(token);

  return vecTokens;
}

/* *************************************************************** */

std::string
replaceStringWithOtherString(std::string inStringToModify, const std::string inStringToReplace, const std::string inNewString, const bool flag_forAllOccurances = false)
{
  //  bool flag_continueSearch = true;

  int pos = (int)(inStringToModify.find(inStringToReplace));
  while (pos != (int)(std::string::npos))
  {
    inStringToModify.replace(pos, inStringToReplace.length(), inNewString);
    pos = (int)(std::string::npos);

    if (flag_forAllOccurances)
      pos = (int)(inStringToModify.find(inStringToReplace));
    else
      break;
  }

  return inStringToModify;
}

/* *************************************************************** */

std::string
stringToLower(std::string strToConvert)
{ // change each element of the string to lower case
  for (size_t i = 0; i < strToConvert.length(); i++)
  {
    strToConvert[i] = tolower(strToConvert[i]);
  }
  return strToConvert; // return the converted string
}


/* *************************************************************** */
/* *************************************************************** */

void check_validity_of_display_object_elements(const std::string& inSavePath)
{
  //std::string file1 = "../../../RescueX_Lib/cars/RTW_04.obj";
  std::string file1 = "USAmbulance.obj";
  
  fs::path pathToShow = inSavePath + file1;
  //fs::path pathToShow = file1;

  std::cout << "exists() = " << fs::exists(pathToShow) << "\n"
            << "root_name() = " << pathToShow.root_name() << "\n"
            << "root_path() = " << pathToShow.root_path() << "\n"
            << "relative_path() = " << pathToShow.relative_path() << "\n"
            << "parent_path() = " << pathToShow.parent_path() << "\n"
            << "filename() = " << pathToShow.filename() << "\n"
            << "stem() = " << pathToShow.stem() << "\n"
            << "extension() = " << pathToShow.extension() << "\n"
            << "isFile() = " << fs::directory_entry(pathToShow).is_regular_file() << "\n"
            << "parent server name: " << pathToShow.parent_path().filename().string() << "\n";



  std::string path = pathToShow.remove_filename().string();
  std::cout << "path: " << path << "\n";

  for (const auto& entry : fs::directory_iterator(path))
  {
    std::cout << entry.path() << std::endl;
    std::cout << entry.path().filename().string() << std::endl;
  }

}


std::list<std::string> gather_all_active_custom_libraries()
{
  const std::string customScenery_folder = "d:\\xp11clean/Custom Scenery/";
  //const std::string customIniFile = customScenery_folder + "scenery_packs.ini"; // mxconst::FOLDER_SEPARATOR + missionx::XPLANE_SCENERY_INI_FILENAME; // 

  std::list<std::string> listActiveCustomFolders;

  fs::path customScenery_fs = customScenery_folder;

  if (fs::exists(customScenery_fs) && fs::directory_entry(customScenery_fs).is_directory())
  {
    fs::path xplane_folder_fs = customScenery_fs.parent_path().parent_path();
    std::ifstream infs;
    // optimizations.
    std::ios_base::sync_with_stdio(false); // 
    std::cin.tie(nullptr); // 
    // open scenery file
    const std::string customIniFile = customScenery_fs.string() + "/" + "scenery_packs.ini"; // mxconst::FOLDER_SEPARATOR + missionx::XPLANE_SCENERY_INI_FILENAME; // 

    infs.open(customIniFile.c_str(), std::ios::in);

    if (infs.is_open())
    {
      std::string line;
      while (getline(infs, line))
      {
        if (line.find(SCENERY_PACK_.c_str()) == 0) // if line starts with "SCENERY_PACK " then should be valid
        {
          const std::string scenery_pack = line.substr(SCENERY_PACK_.length()); // add trim to the string
          //listActiveCustomFolders.emplace_back( xplane_folder_fs.string() + scenery_pack); // holds absolute path
          listActiveCustomFolders.emplace_back(scenery_pack); // holds relative path
        }
        line.clear();
      }

    }
    else
    {
      std::cout << "Fail opening: " << customIniFile << "\n";
    }

    if (infs.is_open())
      infs.close();
  }

  return listActiveCustomFolders;
}

// when loading a mission with 3D Object, check its existance as physical file based on "library" file (which makes it virtual)
std::string check_virtual_file_existance(const std::string inFile, const std::string inXPlanePath)
{
  const std::string POSTFIX_OBJ_FILE = ".obj";
  const size_t postfix_length = std::string(POSTFIX_OBJ_FILE).length();
  const std::string_view export_sv = "EXPORT ";
  const std::string_view LibraryFileName = "library.txt"; // located at the root folder of each custom scenery folder
  const std::map<std::string, int> mapLibTypes = { {"EXPORT ", 2},{"EXPORT_RATIO ", 3},{"EXPORT_EXTEND ", 2},{"EXPORT_EXCLUDE ", 2},{"EXPORT_BACKUP ", 2} };
  std::string result("");

  auto sceneryList = gather_all_active_custom_libraries();
  // loop over all scenery folders and search their Library file content.
  for (const auto& f : sceneryList)
  {
    fs::path lib_file_fs = inXPlanePath + f + "/" + LibraryFileName.data();
    if (fs::directory_entry(lib_file_fs).is_regular_file())
    {
      std::ifstream infs;
      infs.open(lib_file_fs, std::ios::in);
      if (infs.is_open())
      {
        bool flag_foundVirtualFile = false;
        std::string line;
        while (getline(infs, line) && !flag_foundVirtualFile)
        {
          // check line is valid, has export in it
          for (const auto& [key, value] : mapLibTypes)
          {
            if (line.find(key) == std::string::npos)
              continue;

            // get value number and try to split correctly.
            auto offset = key.length();
            if (value > 2) // determind offset since virtual + path are the last two strings
            {
              auto v = value;
              while (v > 2 && offset != std::string::npos)
              {
                offset = line.find(" ", offset);
                v--;
              }
            }

            if (offset != std::string::npos) // now we should only have 2 ".obj" strings. One virtual and one physical.
            {
              std::string virtual_s, physical_s;
              auto end_virtual_string = line.find(POSTFIX_OBJ_FILE, offset);
              if (end_virtual_string != std::string::npos)
              {
                virtual_s = trim(line.substr(offset, end_virtual_string - offset + postfix_length));
                if (virtual_s.compare(inFile) == 0)
                {
                  offset = end_virtual_string + postfix_length;
                  auto end_phys_string_i = line.find(POSTFIX_OBJ_FILE, offset);
                  if (end_phys_string_i != std::string::npos)
                    physical_s = trim(line.substr(offset, end_phys_string_i - offset + postfix_length));

                  if (physical_s.empty())
                    return physical_s;
                  else
                  { // check validity of file existence
                    std::filesystem::path real_file_location_fs = inXPlanePath + f + physical_s;
                    if (fs::exists(real_file_location_fs) && fs::directory_entry(real_file_location_fs).is_regular_file())
                    {
                      result = real_file_location_fs.string();
                      flag_foundVirtualFile = true;
                    }
                    else
                      result.clear();
                  }
                }

              }

            }           

            line.clear();
            break;
          } // end loop over map

        } // while loop over infs lines

        infs.close();
      }
    }
  }

  return result;
}

void inject_file_into_file()
{
  std::string path_s = "d:\\xp11clean\\Custom Scenery\\missionx\\liecht\\";
  std::string template_s = "template.xml";
  std::string find_s = "%obj_people%";
  std::string replace_file_s = "winter_people.txt";
  std::string output_file_debug_S = "tmp3981234.xml";

  fs::path template_file = path_s + template_s;
  if (fs::exists(template_file) && fs::is_regular_file(template_file))
  {
    std::ifstream infs_xml;
    std::ios_base::sync_with_stdio(false); // 
    std::cin.tie(nullptr); // 

    infs_xml.open(template_file, std::ios::in);

    if (infs_xml.is_open())
    {
      std::string line;
      std::string xml_file_content_s;
      while (getline(infs_xml, line)) { xml_file_content_s.append(line).append("\n"); }; // read all file
      const std::string original_xml_file_content_s = xml_file_content_s;

      infs_xml.close(); // free XML file

      // loop over all <find_replace> nodes, read the files and replace with the xml_file
      std::ifstream infs_txt;
      fs::path txt_file = path_s + replace_file_s;
      if (fs::exists(txt_file) && fs::is_regular_file(txt_file))
      {

        infs_txt.open(txt_file, std::ios::in);
        if (infs_txt.is_open())
        {
          std::string line_txt;
          std::string txt_file_content_s;
          while (getline(infs_txt, line_txt)) { txt_file_content_s.append(line_txt).append("\n"); }; // read all file

          infs_txt.close();

          xml_file_content_s = replaceStringWithOtherString(xml_file_content_s, find_s, txt_file_content_s, true);
          std::cout << "new content: \n" << xml_file_content_s << "\n=================\n\n";
        }


      }
      //else
      //  continue;


      // end loop over all <find_replace>


      // Write to new template working file ?
      // after finishing the loop check if xml_file_content_s different than original_xml_file_content_s
      if (xml_file_content_s.compare(original_xml_file_content_s) != 0)
      {
        // write to tmp file
        std::fstream ob;
        fs::path output_file = path_s + output_file_debug_S;
        if (fs::exists(output_file) && fs::is_regular_file(output_file))
        {
          if (fs::remove(output_file))
          {
            std::cout << "File: " << output_file.string() << ", deleted.";
          }
          else
          {
            std::cout << "File: " << output_file.string() << ", failed to be removed.";
            // abort the running random engine
          }
        }

        if (!fs::exists(output_file))
        {
          ob.open(output_file, std::ios::out);
          if (ob.is_open())
          {
            ob << xml_file_content_s << "\n";
            ob.close();
          }

        }


      }

    }
    else
    {
      std::cout << "Fail opening: " << template_file.string() << "\n";
    }


  }
  else
  {
    std::cout << "Template file: " << template_file.string() << " does not exists or not regular file.\n";
  }



}


std::set<std::string>
search_datarefs_in_acf_file(fs::path inFile)
{
  const size_t          min_cahrs = 8;
  std::ifstream         file_aptDat;
  std::set<std::string> setDatarefs;

  std::ios_base::sync_with_stdio(false); 
  std::cin.tie(nullptr);                 

    file_aptDat.open(inFile.string(), std::ios::in); // read the file
  if (file_aptDat.is_open())
  {
    std::string line;
    std::string line_s;

    // std::getline(file_toRead, line);
    while (getline(file_aptDat, line))
    {
      line_s.clear();

      for (auto c : line)
      {
        line_s += (char)std::tolower(c);
      }

      const size_t strLength = (line_s.length()); //      ? line_s.length() - 1 : line_s.length();
      if (min_cahrs < strLength)
      {
        auto datarefPos_i = line_s.find("dataref");
        auto pos_first_space = line_s.find(" ", datarefPos_i);
        if (datarefPos_i != line_s.npos && datarefPos_i < (strLength - 1) && pos_first_space != std::string::npos)
        {
          const std::string dataref_s = trim(line.substr(pos_first_space, 256)); // only copy the last 256 chars
          if (dataref_s.find("sim/") == 0)                                       // do not store default dataref names
            continue;

          std::cout << "dataref_s: " << dataref_s << "\n";
          setDatarefs.emplace(dataref_s);
          
        }
      } // end if string length is longer than 8 characters
    }   // end loop over file lines


    if (file_aptDat.bad())
      perror("error while reading file");

  }
  else // fail to open file
  {
    // Log::logAttention((std::string("[Fail parse aptdat] Fail to open file: ") + relative_apt_dat_path).c_str(), true);
    std::cout << "Fail to open the file: " << inFile.string() << "\n";

  }
  return setDatarefs;
}

std::set<std::string>
search_datarefs_in_obj_file(fs::path inFile)
{
  const char            dlmterToCount  = '/';
  const size_t          min_dref_chars = 8;
  const size_t          min_line_chars = 25;
  std::ifstream         file_toRead;
  std::set<std::string> setDatarefs;

  std::ios_base::sync_with_stdio(false); 
  std::cin.tie(nullptr);                 

    file_toRead.open(inFile.string(), std::ios::in); // read the file
  if (file_toRead.is_open())
  {
    std::string line;
    std::string line_s;

    // std::getline(file_toRead, line);
    while (getline(file_toRead, line))
    {
      line_s.clear();

      line = trim(line);
      if (line.length() < min_line_chars)
        continue;

      line_s = stringToLower(line);

      if (line_s.find("anim_") != 0)
        continue;


      std::vector<std::string> tokens = split(line);

      if (tokens.empty())
        continue;

      const auto lastVal_s = tokens.back();

      if (lastVal_s.find("CMND=") == 0) // represent command flag in aerobask for example
        continue;

      if (countCharsInString(lastVal_s,  '/') > 0)
      {
        setDatarefs.insert(lastVal_s);
      }


    }   // end loop over file lines


    if (file_toRead.bad())
      perror("error while reading file");

  }
  else // fail to open file
  {
    // Log::logAttention((std::string("[Fail parse aptdat] Fail to open file: ") + relative_apt_dat_path).c_str(), true);
    std::cout << "Fail to open the file: " << inFile.string() << "\n";

  }
  return setDatarefs;
}

// XPLMGetNthAircraftModel(index, outFileName, outPath); // we will only return the file name
std::set<std::string>
gather_aircraft_dataref(fs::path inAircraftPath, std::string acf_file)
{
  std::set<std::string> drefKeySet;

  if (fs::is_directory(inAircraftPath))
  {
    // search acf_file for DATAREF string
    fs::path acf = inAircraftPath / acf_file;

    // print directory
    //std::cout << "Parent Path: " << acf.parent_path().filename().string() << "\n";

    if (fs::is_regular_file(acf))
    {
      //drefKeySet = search_datarefs_in_acf_file(acf);
    }

    // recursivally find all "obj" files
    


  }

  for (auto file :  fs::recursive_directory_iterator(  inAircraftPath ) )
  {
    const auto ext =  stringToLower( fs::path(file).extension().string()); // debug
    if (fs::is_regular_file(file) && ext.compare(".obj") == 0)
    {
      std::set<std::string> drefKeySetFromObjFile = search_datarefs_in_obj_file(file);
      //drefKeySet.insert(drefKeySetFromObjFile);
      drefKeySet.insert(drefKeySetFromObjFile.cbegin(), drefKeySetFromObjFile.cend());
    }

  
  }


  return drefKeySet;
}

int main()
{
  ///////////// INJECT FILE CONTENT into OTHER File

  //inject_file_into_file();



  //////////// CHECK VIRTUAL FILE ///////////////////////////
  //const std::string_view file_virtual_s = "R2_Library/letiste/doplnky/sudy_CB_letiste.obj";
  //const std::string real_path_s = check_virtual_file_existance(file_virtual_s.data(), "d:\\xp11clean/");
  //if (real_path_s.empty())
  //{
  //  std::cout << "Failed to find virtual file: " << file_virtual_s << "\n";
  //}
  //else
  //{
  //  std::cout << "Found virtual file: " << file_virtual_s << " in: " << real_path_s << "\n";
  //}
  ///////// END VIRTUAL FILE TEST /////////////////
  
  //check_validity_of_display_object_elements("d:\\xp11clean/Custom Scenery/missionx/random/briefer/");

  fs::path acfPathAndFile = "f:\\X-Plane11\\Aircraft\\Aerobask\\Phenom 300";
  std::string acf_file    = "phenom300.acf";
  gather_aircraft_dataref(acfPathAndFile, acf_file);


    //std::cout << "Hello World!\n";
}

// Run program: Ctrl + F5 or Debug > Start Without Debugging menu
// Debug program: F5 or Debug > Start Debugging menu

// Tips for Getting Started: 
//   1. Use the Solution Explorer window to add/manage files
//   2. Use the Team Explorer window to connect to source control
//   3. Use the Output window to see build output and other messages
//   4. Use the Error List window to view errors
//   5. Go to Project > Add New Item to create new code files, or Project > Add Existing Item to add existing code files to the project
//   6. In the future, to open this project again, go to File > Open > Project and select the .sln file
