#!/usr/bin/perl
#
# https://www.ogimet.com/metars.phtml.en
# http://aviationwxchartsarchive.com/product/metar
#
# Example:
#   perl conveMetarToXPlaneFormat.pl --source example01.txt --target example.rwx --tmpl "TIMESTAMP # ICAO DATE DATA" --le =
###############################
use strict;
use warnings;
use 5.010;
use POSIX qw(strftime);
use Getopt::Long qw(GetOptions);
Getopt::Long::Configure qw(gnu_getopt);
use Data::Dumper;

############### Function ####################


sub ltrim { my $s = shift; $s =~ s/^\s+//;       return $s };
sub rtrim { my $s = shift; $s =~ s/\s+$//;       return $s };
sub  trim { my $s = shift; $s =~ s/^\s+|\s+$//g; return $s };

sub display_help {
   say "command format: $0 {--source|--s} NAME {--target|-t} NAME {--template|--tmpl} FORMAT [--line_end EndOfLine | --le] [--user_date|--udt] [--help|--h|--?]";
   say "\n>> syntax >>";
   say "{} = mandatory";
   say "\"|\" = alias to same command";
   say "[] = optional parameter";
   say "<< syntax <<";
};
############### END FUNCTIONS ###############

### 12aug2019 read external parameters
my $source_file;
my $sh; # source handler
my $target_file;
my $th; # target handler
my $template;
my $line_ends = "";
my $user_date_time;
my $help;
my $need_to_find_metar_line_end = 0;
## 01-sep-2019
my $time_from_metar_line = "";

#print ("===> Reading options\n");

GetOptions(
  'source|s=s' => \$source_file,
  'target|t=s' => \$target_file,
  'template|tmpl=s' => \$template,
  'line_end|le=s' => \$line_ends,
  'user_date_time|udt=s' => \$user_date_time,
  'help|h|?' => \$help
) or die "Usage: $0 --source NAME --target NAME --template FORMAT --line_end EndOfLine --user_date OptionalDate \n";

if ( $help && not ( $help eq "" ) )
{
  display_help();
  #say "command format: $0 -source NAME --target NAME --template FORMAT [--line_end EndOfLine] [--user_date] ";
  exit 0;
}


if ($source_file) {
  say "source file: $source_file";
}

if ($target_file) {
  say "target file: $target_file ";
}

if ($template) {
  say "template: $template";
}

if ($line_ends) {
  say "special end of line character: $line_ends";
}

if ($user_date_time) {
  say "override day and time with: $user_date_time";
}

# Validate external values
if (not defined $source_file)
{
 display_help();
 die "\nNo source file defined"; 
}

if (not defined $target_file)
{
 display_help();
 die "\nNo target file defined"; 
}

if (not defined $template)
{
 display_help();
 die "\nNo Template was defined.\nExample \"TIMESTAMP # ICAO DATE DATA\" will be translate to:\n TIMESTAMP, ignore second string. 3rd string is ICAO. 4th string is DATE. 5th string and on is data\nThe timestamp should be 12 characters in length\n"; 
}


## Internal values
my $DELIMETER=" ";
my @metar_split; # holds each file line splited into words
my @template_split; # holds the template words

@template_split = split($DELIMETER, $template);

print ("Valid METAR line needs to have [ ".@template_split." ] directives: $template\n" );

### Date hadnling
my $datestring = gmtime();

print ("GMT Fallback date time: ");
my $fallback_date = strftime "%Y/%m/%d", gmtime();
my $fallback_time = strftime "%H:%m", gmtime();
my $fallback_date_time = $fallback_date." ".$fallback_time;
print($fallback_date_time."\n\n");

#if (not defined $user_date_time)
#{
# $user_date_time = $fallback_date_time;
#}

## end date

open($sh, '<', $source_file)
    or die "Could not open file '$source_file' $!";
  
open($th, '>', $target_file)
  or die "Could not open file '$target_file' $!";
  
 #my $regex = qr/=$/sp;
 #my $regex = qr/${line_ends}$/sp;
 #print "Regex: $regex \n";

 # line end handling
 my $new_row = "";
 my $le_length = -1 ;

 if ( $line_ends eq "")
 {
   $line_ends = "";
   $le_length = -1; ## No known line end character.

   print "line_ends was empty.";
 }
 else
 {
  $le_length = length $line_ends;
 }
  

 
 my $counterLines = 0;
 my $processed_lines = 1;
## Loop over file
  while (my $row = <$sh>) {
  print "Process line $processed_lines \n";
  $processed_lines += 1;

    chomp $row;
    $row = trim $row;

#    $row =~ s/[\n\r\t]+//g;
    $row =~ s/[\n\t]+//g;  ## I think this removes \n and \t special chars in line

    my $el_pos = -1;
    
    if ( not ($line_ends eq ""))# read line until the end of line character, but do not include it. Example: end of line is: "=". Line includes: {some words}=  will return {some words}
    {
      $el_pos = index ($row, $line_ends, - $le_length );
    }
    
    my $len = length $row;

    $counterLines = $counterLines + 1;

    print "[debug] end_line pos: $el_pos. row: $row\n"; # debug

    @metar_split = split ($DELIMETER, $row); # split METAR line to words using the space delimeter
    
   
    my $val = "";
    my $templateValue;
    my $templateCounter = scalar @template_split;
    my $counterTemplate = 0;
    my $counterMetarWords = 0;
    my $metar_split_size = scalar @metar_split;
    
    my $boolSkipLine = 0;

    ## check if line starts with "#"
    if ( $row =~ m/^#/  ) ## if starts with comment symbol "#"
    {
      print "Skipping row with value: $row\n"; # debug
      $boolSkipLine = 1;
      next;
    }
    
    if ( defined $row && ( $row eq '' || $row =~ /^ *$/ || $row =~ /^\s*$/ ) ) ## if empty
    {
      print "Skipping empty row $row\n";
      $boolSkipLine = 1;
      next;
    }
    

    if ( $need_to_find_metar_line_end == 1 ) ## in some cases a metar line can have few lines. We concatenate thes lines to form one line to split and analyze
    {
      print "[debug] Continue search for metar file special end of line: [ $line_ends ]\n";
      print "[debug] ${row} \n";      

      if ( $el_pos > -1 ) ## if we found line end then we can continue
      {
        $need_to_find_metar_line_end = 0;
        $boolSkipLine = 0;

        print "[debug] Line ends with: $line_ends. Need to flush to file\n";
      }
      else  # if we did not found line end then we need to flag the line as multi line and we need to concatenate
      {
        $new_row = $new_row." ".$row;
        print "[debug] Did not find special line end $line_ends . Will read another line\n";
        $need_to_find_metar_line_end = 1;
        next;
      }

    }
    else ## if we finished building the metar line, we need to test it against a template line
    {
      ## Use the template rules given in the command line and analyze the validity of the current metar line
      foreach my $i (0..$#template_split)
      {
        my $word = $template_split[$i];
        my $lenTemplateWord = length $word;
      
      
        if ($boolSkipLine == 1)
        {
          next;
        }
        
        print "Template Word is: [$word], \n"; #, length: $lenTemplateWord.";
        if ( $word =~ m/^#/  ) ## if template word is "#" then skip
        {
        
          $val = shift @metar_split;
          $counterMetarWords = $counterMetarWords + 1;
         
          print "Skip word: ".$val."\n";
                    
        }
        elsif ($word eq "ICAO") # check ICAO
        {
          $val = shift @metar_split;
          $counterMetarWords = $counterMetarWords + 1;
        
          if ( defined $val )
          {
            my $len = 0;
            $len = length($val);                  
            if ( defined $len && $len  > 2 )
            {
              print "Length icao: $len, ";
              print "processing ICAO: $val, ";
              $new_row .=" " . $val;
            }          
            print ("Process ICAO $val \n");          
          }
          else
          {
            print ("No ICAO set found, skip line...");
            $boolSkipLine = 1;
          }
        
        }
        elsif ($word eq "TIMESTAMP") ## check timestamp in correct length
        {
          $val = shift @metar_split;
          $counterMetarWords = $counterMetarWords + 1;
        
          if ( defined $val )
          {
            my $len = length $val;
            if ( $len == 12)
            {
              my $year = substr $val, 0,4;
              my $month= substr $val, 4,2;
              my $day=   substr $val, 6,2;
              my $hour = substr $val, 8,2;
              my $minute = substr $val, 10;

              $user_date_time = "$year/$month/$day $hour:$minute";

              print ("Process Timestamp $val\n");      
            }
            else
            {
              print "Timestamp length format is wrong.\n";
              $boolSkipLine = 1;
            }
              
          }
          else
          {
            $boolSkipLine = 1;          
          }

        }
        elsif ($word eq "DATE") ## check date is in correct length
        {
          $val = shift @metar_split;
          $counterMetarWords = $counterMetarWords + 1;
          $time_from_metar_line = ""; # reset value 

          if ( defined $val )
          {
            my $len = length $val;
            if ( $len == 7)
            {
              $new_row .=" " . $val;

              $time_from_metar_line = (substr $val, 2,2).":".(substr $val, 4,2);
              print ("[debug] Extracted time from metar line: $time_from_metar_line \n");
              print ("Process DATE $val\n");      
            }
            else
            {
              print "Date length format is wrong.\n";
              $boolSkipLine = 1;
            }
              
          }
          else
          {
            $boolSkipLine = 1;          
          }

        }
        elsif ($word eq "DATA") ## data represents the rest of the METAR line that we should just copy as is. In some cases the DATA encompass few lines, therefore we use the flags need_to_find_metar_line_end to flag them, so next iteration we will continue with data line read.
        {
          my $mWord = "";          
          foreach my $w (0..$#metar_split) # we should start from 0 since we shift out all the time. 
          {
            $mWord = $metar_split[$w];
            #print "$mWord - "; ## debug
            $new_row .=" ".${mWord};
            #print "$new_row\n"; ## debug

          }
          print "Processed DATA: $new_row\n";

          # check if new row ends with line_end
          if (defined $line_ends )
          {
            if ( $line_ends eq '' || $line_ends eq "" || $line_ends =~ /^ *$/ || $line_ends =~ /^\s*$/ )
            {
             print "[debug] No need for special line end in template. Can continue to next line. \n";
              $need_to_find_metar_line_end = 0;
            }
            else
            {
              print "[debug] Checking if new line ends with the line_ends symbol: '$line_ends'\n";

              #if ( $new_row =~ /$regex/ )
              if (  $el_pos > -1 )
              {
                $need_to_find_metar_line_end = 0;
                $boolSkipLine = 0;
                print "Ends with special line end.\n";

              }
              else
              {
                $need_to_find_metar_line_end = 1;
              }


            }
          }

        }
        else
        {
          print "do something else...\n";
        }
    
    
        #print "New Row: $new_row\n"; ## DEBUG
      }
        
    } # end loop over each word and "$need_to_find_metar_line_end == 1"

   ## Write to output file
   print "before writing to file. skip line value: $boolSkipLine,  need_to_find_metar_line_end value: $need_to_find_metar_line_end and row: $new_row \n";

   if ( $need_to_find_metar_line_end == 0 )
   {
     if ($boolSkipLine == 1)
     {
       $new_row = "";
       print "skipping row: $row\n";
     }
     else
     {
      ## make sure to remove the "end line" in cases of Single METAR line or Multi line.
      if ($counterLines == 1) # handle when only 1 metar row found
      {
        if ( not $line_ends eq "" )
        {
          $el_pos = index ($new_row, $line_ends, - $le_length );
          if ( $el_pos > -1 && ( not $line_ends eq "" ) )  
          {
            $new_row = substr $new_row, 0, $el_pos; # concat new line with old line 
          }      
        }
      }
      else   # handle when metar line combined from multi rows
      {
        if ( not $line_ends eq "" ) 
        {
          $el_pos = index ($row, $line_ends, - $le_length );
          if ( $el_pos > -1 )
          {
            $new_row = $new_row." ".(substr $row, 0, $el_pos); # concat new line with old line 

          }        
        }
      }

      print "adding: $new_row\n";
    
      if (defined $th )
      {
      ## print date + time in one line
        if ( defined $user_date_time && $user_date_time ne "" ) # || $time_from_metar_line eq '' || $time_from_metar_line eq "" || $time_from_metar_line =~ /^ *$/ || $time_from_metar_line =~ /^\s*$/ )
        {
          print $th trim($user_date_time) . "\n";
        }
        elsif (defined $time_from_metar_line) 
        {
          $user_date_time = $fallback_date." ".$time_from_metar_line;
          print $th trim($user_date_time) . "\n";
        }
        else 
        {
          $user_date_time = $fallback_date_time;
          print $th trim($user_date_time) . "\n";        
        }
          
        # print the metar line
        print $th trim($new_row) . "\n";
        print $th "\n";
      }
     }
    
     print "======== \n"; # debug
     $new_row=""; # reset row so it won't be cummulative
     $counterLines = 0;
   }
    
  } # end loop over source lines



 if (defined $th )
 {
   close ($th);  
 }
  
  
 close ($sh);
