#include "asprintf.h" /* NOTE: this has to be placed *before* '#include <stdio.h>' */

#include <stdio.h>
#include <stdlib.h>

int main(void)
{
    char *str = NULL;
    int len = asprintf(&str, "The answer to %s is %d", "life, the universe and everything", 42);
    if (str != NULL) {
        printf("String: %s\n", str);
        printf("Length: %d\n", len);
        free(str);
    }
    return 0;
}