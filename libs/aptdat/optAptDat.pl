use strict;
use warnings;
 
my $filename = "apt.dat";
open(my $fh, '<:encoding(UTF-8)', $filename)
  or die "Could not open file '$filename' $!";

my $outfile = 'apt.txt';
open(my $of, '>', $outfile)
  or die "Could not open file '$outfile' $!";

my $counter=1;
my $continue=1; ## boolean value
my $line="";

while ( (my $line=<$fh>) && ($continue == 1) ) {
  #select * from global_name;
  #$line=$fh;
  chomp $line;  ## removes eol
  #print "$line";
  
  my $string_len =  length( $line );
  if ($string_len < 5){
    next;
  }
    
  ## find index of first space
  my $index_of_space = index $line, ' ';
  my $code=substr($line,0,$index_of_space);
  
  #print "Code:[$code]\n";
  if ($code eq "1" || $code eq "16" || $code eq "17" || $code eq "1300" || $code eq "100" || $code eq "101" || $code eq "102" || $code eq "56" )
  {
    print $of $line."\n";
  }
  
  $counter=$counter+1;
  #if ($counter > 10000)
  #{
  #  $continue = 0;
  #  #exit(0);
  #}
    
}

close($outfile);
close ($fh);

print "done, proccessed: $counter lines.\n";
