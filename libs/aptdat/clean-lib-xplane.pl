#!/usr/bin/perl
use strict;
use warnings;
 
 
##
## You should use the following format:
## $ perl {file_name}.pl {in file name to format} {library folder name}
## Example:
## $ perl clean-lib-xplane.pl r2lib.txt R2_Library
##
## It is important to use case sensitive file names
##  
##

my $filename = "$ARGV[0]";
my $lib_name = "$ARGV[1]";

open(my $fh, '<:encoding(UTF-8)', $filename)
  or die "Could not open file '$filename' $!";

my $outfile = "${filename}.cleaned";
open(my $of, '>', $outfile)
  or die "Could not open file '$outfile' $!";

my $counter=1;
my $continue=1; ## boolean value
my $line="";

while ( (my $line=<$fh>) && ($continue == 1) ) {
  #select * from global_name;
  #$line=$fh;
  chomp $line;  ## removes eol
  #print "$line";

  # find last position of space  
  my $char = ' ';
  my $last_index_of_space = rindex($line, $char);
  
  if ($last_index_of_space > -1)
  {
    my $cleaned_line=substr($line, ($last_index_of_space + 1));
    chomp $cleaned_line;
    $cleaned_line =~ s/\x0d{0,1}\x0a{0,1}\Z//s; ## remove end line on Unix, Win & OSX style OSes  # https://www.perlmonks.org/?node_id=504626
    
    my $formated_line='<obj3d  name="'.$lib_name.'" file_name="../../../'.$lib_name.'/'.$cleaned_line.'" />';
    
    print $of $formated_line."\n";
  }

  $counter=$counter+1;
    
}

close($outfile);
close ($fh);

print "done, proccessed: $counter lines.\n";
