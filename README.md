### W.I.P ###


# Mission-X V3 - By Saar #
Copyright (C) 2023 Saar Nagar. All rights reserved.

### Mission-X v3 programmer setup ###

To setup Mission-X you need one of the following tools:

- For Windows: [Visual Studio 2017 community addition (should be free)](https://www.visualstudio.com/downloads/)
- For Linux: [Eclipse CDT](http://www.eclipse.org/). I strongly suggest to download the new eclipse installer and install using it (works well on Linux and Windows).
- For OSX: use X-Code 10.x (just download from Apple Store). If you have newer X-Code try to build against lower SDK. Unfortunately the new UI library I'm using fail to render itself in OSX, although in DEBUG I do see it has the expected configurations. Hope to solve this.

### How do I get set up? ###

First: Download the latest repository build.

For each tool there is a project file. Open the file and all dependencies and configuration should be self contained in the project folder.

Example:

- Visual Studio project resides in: "VS/2017/missionx.sln"
- Eclipse project: This is a little tricky since I did not want to place project in the default folder. Therfore the project was placed in: "eclipse/missionx" (project files are hidden in Linux). You will have to import the project.


There are two build types **debug x64** and **release x64**.

Debug build will support "print" command in MY-BASIC scripts. This is relevent to debug scripts and should also be used by Mission designers. Unfortunately this command affects FPS.

Release build disables the "print" command in MY-BASIC scripts. This is to max FPS.

### Contribution guidelines ###

To be able to contribute you should:

1. Go Over the code (just follow the Flight Loop).
2. Read the comments or any material related to this plugin developemnt.
3. Send me questions and I'll try to assist.
4. **Any** code you add, should adhere to the following rule:

 * Do not write cryptic code or non readable one. Remember, when you write it, it is understandable and make sense to you. But in few months you might scratch your head and won't understand what you did. Now think on the other programmer that read your code for the first time.
 * Any code you write that is "cryptic" you must add an explanation and an alternative code in comments to describe what you did.
 * Use C++ x11 to x17 code standards, newer C++ standards like C++ x20/21 will probably not cross compile between OSes. If you still need this, you must provide the alternative code for all other OSes compilers.
 * Try not to create instances of Objects using "new".
 * When sending an Object to a function, use "&name" instead of "*name" when ever it is possible.
5. Add comments to explain what you want to achieve, and try to add a version that will reflect when you did this change. Example: // v3.0.180 

It is somewhat hard to just drop in and try to understand what the programmer thought, or what were their intentions. This is why I will try every now and then add some comments on the flow of the plugin and maybe add more explanations that will suite programers.

Here is a high level of the plugins flight loop


### W.I.P ###
